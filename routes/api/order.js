const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../../models/User');
const e = require('express');

router.post('/', async (req, res) => {
  let elem = {};
  User.getOrderbyPersid(req.body.persid, (err, order) => {
    if (err) {
      res.json({ success: false, msg: 'Failed to add order' });
    } else {
      if (order.length == 0) {
        let resources = req.body.resources;
        let rationRun = [
          {
            pax: req.body.order[0].pax,
            muslim: req.body.order[0].muslim,
            nonMuslim: req.body.order[0].nonMuslim,
            breakfast: req.body.breakfast,
            lunch: req.body.lunch,
            dinner: req.body.dinner,
            vegetarianIndian: req.body.order[0].vegetarianIndian,
            vegetarianChinese: req.body.order[0].vegetarianChinese,
            extraInfo: req.body.order[0].extraInfo,
          },
        ];
        req.body.order.rationRun = rationRun;

        let order = new User.Order({
          persid: req.body.persid,
          orders: req.body.order,

          unit: req.body.unit,
          hub: req.body.hub,
        });
        if (order.check == undefined || order.check == false)
          order.orders[0].resources = resources;
        User.postOrder(order, (err, order) => {
          if (err) {
            res.json({ success: false, msg: 'Failed to add order' });
          } else {
            res.json({ success: true, msg: 'Added Order' });
          }
        });
      } else {
        if (req.body.orderId == undefined) {
          let rationRun = [
            {
              pax: req.body.order[0].pax,
              muslim: req.body.order[0].muslim,
              nonMuslim: req.body.order[0].nonMuslim,
              breakfast: req.body.breakfast,
              lunch: req.body.lunch,
              dinner: req.body.dinner,
              vegetarianIndian: req.body.order[0].vegetarianIndian,
              vegetarianChinese: req.body.order[0].vegetarianChinese,
              extraInfo: req.body.order[0].extraInfo,
            },
          ];
          console.log('ration run: ' + JSON.stringify(rationRun));
          let resources = req.body.resources;
          order[0].orders.push({
            endDate: req.body.order[0].endDate,
            location: req.body.order[0].location,
            activity: req.body.order[0].activity,
            startDate: req.body.order[0].startDate,
            check: req.body.order[0].check,
            rationRun: rationRun,
            types: req.body.order[0].types,
            poc: req.body.order[0].poc,
            processing: req.body.order[0].processing,
            resources: resources,
            extraInfo: req.body.order[0].extraInfo,
          });
        } else {
          if (req.body.indentDriver == true || req.body.indentDriver == false) {
            //When Driver is being indented
            req.body.order = [req.body.order]; //req.body.order has to be converted to an array as we use order[0] below
            req.body.resources = req.body.order[0].resources;
          }
          order[0].orders.forEach((element) => {
            console.log(req.body.orderId);
            if (element._id == req.body.orderId) {
              console.log('harlooo');
              if (
                (req.body.remove == false || req.body.remove == undefined) &&
                req.body.status != true &&
                req.body.status != false //req.body.status is checked becaue we only need to update one line and not each. This is also to prevent errors if the req.body.order[0] is not in a correct format and all we had to do was update the status.
              ) {
                if (req.body.order[0].rationRun[0] != undefined) {
                  console.log('helooo');
                  for (
                    let i = 0;
                    i < req.body.order[0].rationRun[0].resources.length;
                    i++
                  ) {
                    let bool = true;
                    for (
                      let x = 0;
                      x < element.rationRun[0].resources.length;
                      x++
                    ) {
                      console.log('HERLLLOOO');
                      console.log(
                        new Date(
                          req.body.order[0].rationRun[0].resources[i].date
                        ).getDate()
                      );
                      if (
                        new Date(
                          element.rationRun[0].resources[x].date
                        ).getDate() ==
                          new Date(
                            req.body.order[0].rationRun[0].resources[i].date
                          ).getDate() &&
                        element.rationRun[0].resources[x].typeName ==
                          req.body.order[0].rationRun[0].resources[i].typeName
                      ) {
                        element.rationRun[0].resources[x] =
                          req.body.order[0].rationRun[0].resources[i];
                        element.rationRun[0].resources[x].token = [];
                        bool = false;
                      }
                    }
                    console.log(bool);
                    if (bool == true)
                      element.rationRun[0].resources.push(
                        req.body.order[0].rationRun[0].resources[i]
                      );
                  }

                  if (req.body.breakfast != undefined) {
                    element.rationRun[0].breakfast = req.body.breakfast;
                    element.rationRun[0].lunch = req.body.lunch;
                    element.rationRun[0].dinner = req.body.dinner;
                    element.rationRun[0].muslim = req.body.order[0].muslim;
                    element.rationRun[0].nonMuslim =
                      req.body.order[0].nonMuslim;
                    element.rationRun[0].vegetarianChinese =
                      req.body.order[0].vegetarianChinese;
                    element.rationRun[0].vegetarianIndian =
                      req.body.order[0].vegetarianIndian;
                    element.rationRun[0].extraInfo =
                      req.body.order[0].extraInfo;
                    element.rationRun[0].pax = req.body.order[0].pax;
                  }
                  console.log('Why Am I not here' + element.rationRun[0]);
                }
                element.endDate = req.body.order[0].endDate;
                element.location = req.body.order[0].location;
                element.activity = req.body.order[0].activity;
                element.startDate = req.body.order[0].startDate;
                element.check = req.body.order[0].check;
                element.types = req.body.order[0].types;
                element.poc = req.body.order[0].poc;
                element.resources = req.body.resources;
                element.extraInfo = req.body.order[0].extraInfo;
              }
              if (req.body.status == true) {
                element.status = req.body.status;
                element.processing = 'Approved';
                element.indentApprover = req.body.indentApprover;
              }
              if (
                req.body.status != undefined &&
                req.body.status != true &&
                req.body.status != false
              )
                if (
                  req.body.status.localeCompare(
                    'Processing (pending approval)'
                  ) == 0
                ) {
                  element.status = false;
                  element.processing = 'Processing (pending approval)';
                  element.indentApprover = req.body.indentApprover;
                  element.reason = '';
                }
              if (req.body.status == false) {
                element.status = req.body.status;
                element.processing = 'Rejected';
                element.indentApprover = req.body.indentApprover;
                element.reason = req.body.reason;
              } else {
              }
            }
          });
        }
        if (req.body.remove == true) {
          User.removeOrder(req.body.persid, req.body.orderId, (err, order) => {
            if (err) {
              res.json({ success: false, msg: 'Failed to remove order' });
            } else {
              res.json({ success: true, msg: 'Removed Order' });
            }
          });
        }

        if (req.body.remove == false || req.body.remove == undefined) {
          console.log('inside updating order');
          User.updateOrder(order, (err, order) => {
            if (err) {
              console.log(err);
              res.json({ success: false, msg: 'Failed to add order' });
            } else {
              console.log('Here');
              res.json({ success: true, msg: 'Added order' });
            }
          });
        }
      }
    }
  });
});

module.exports = router;
