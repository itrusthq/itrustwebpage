const express = require('express');
const router = express.Router();
const User = require('../../models/User');
const { google } = require('googleapis');
var FCM = require('fcm-node');
var serverKey = require('./service-account.json'); //put the generated private key path here
const { response } = require('express');

var fcm = new FCM(serverKey);
// @route GET api/Post
router.get('/', async (req, res) => {
  User.getOrderbyPersid(req.query.ID, (err, order) => {
    if (err) {
      res.json({ success: false, msg: 'Failed to get order' });
    } else {
      res.json({ sucess: true, orders: order });
    }
  });
});

//Getting all the orders from the node for the indents page/ITMS
router.get('/all', async (req, res) => {
  User.getOrdersbyUnit(req.query.hub, (err, orders) => {
    if (err) {
      res.json({ success: false, msg: 'Failed to get all Orders' });
    } else {
      res.json({ success: true, orders: orders });
    }
  });
});

router.post('/notif', async (req, res) => {
  //if response sharedResources has a remark then we do a fcm.send() ok so
  let resources = req.body.resources;

  let remarks = [];
  let resourcesArr = [];
  for (let x = 0; x < resources.length; x++) {
    let bool = true;
    if (resources[x].remarks !== undefined && resources[x].remarks !== '') {
      for (let i = 0; i < resources[x].token.length; i++) {
        if (req.body.token === resources[x].token[i]) bool = false;
      }
      if (bool == true) {
        remarks.push(resources[x].remarks);
        resourcesArr.push(resources._id);
      }
    }
  }
  function getAccessToken() {
    return new Promise(function (resolve, reject) {
      var key = require('./service-account.json');
      var jwtClient = new google.auth.JWT(
        key.client_email,
        null,
        key.private_key,
        'https://www.googleapis.com/auth/firebase.messaging',
        null
      );
      jwtClient.authorize(async function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
        console.log(tokens.access_token);
        let string = req.body.token.toString();
        let val = '';
        let val1 = '';
        let val2 = '';
        User.postRemarksUpdate(remarks[0], req.body.token, (err, callback) => {
          if (err) {
            console.log(err);
          } else {
            let order1 = '';
            callback.orders.forEach((order) => {
              order.rationRun.forEach((rationRun) => {
                rationRun.resources.forEach((resources) => {
                  if (
                    resources.remarks !== undefined &&
                    resources.remarks === remarks[0]
                  ) {
                    resources.token.push(req.body.token);
                    val = order.activity;
                    console.log(val);
                    order1 = callback;
                    val1 = resources.date.toString();
                    val2 = resources.typeName;
                  }
                });
              });
            });
            User.updateOrder([callback], (err, order) => {
              if (err) {
                console.log(err);
                res.json({ success: false, msg: 'Failed to add order' });
              } else {
                console.log('Here');
                res.json({ success: true, msg: 'Added order' });
              }
            });
          }
          var message = {
            //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: string,

            data: {
              //you can send only notification or only data(or include both)
              my_key: remarks[0],
              my_another_key: val,
              my_third_key: val1,
              my_last_key: val2,
            },
          };

          try {
            fcm.send(message, function (err, response) {
              if (err) {
                console.log(err);
              } else {
                console.log('Successfully sent with response: ', response);
              }
            });
          } catch (err) {
            console.log(err);
          }
        });
      });
    });
  }
  if (remarks.length > 0) {
    let tk = getAccessToken();
  } else {
    res.json({ msg: 'no remarks' });
  }
});
module.exports = router;
