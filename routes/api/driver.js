const express = require('express');
const User = require('../../models/User');
const router = express.Router();
const config = require('config');
var firebase = require('firebase/app');

// @route GET api/driver
router.get('/driver', async (req, res) => {
  try {
    User.getDrivers((err, drivers) => {
      if (err) res.json({ success: false, msg: 'Failed to find user' });
      else res.json({ success: true, drivers: drivers });
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

router.patch('/', async (req, res) => {
  //I want to use patch instead of post even tho both work as post is meant for adding data as a child document...

  const name = req.body.params.driverAssigned; //If it doesn't work we may need to change it so that it
  const orderId = req.body.params.orderId;
  const resourceId = req.body.params.resourceId;
  const startDate = req.body.params.startDate;
  const endDate = req.body.params.endDate;
  try {
    User.postDriverbyId(
      name,
      orderId,
      resourceId,
      startDate,
      endDate,
      (err, drivers) => {
        if (err) res.json({ success: false, msg: 'Failed to find user' });
        else {
          res.json({ success: true, drivers: drivers });
          firebase
            .database()
            .ref('drivers/' + name)
            .push({
              orderId: orderId,
              resourceId: resourceId,
              startDate: startDate,
              endDate: endDate,
            })
            .catch((err) => {});
        }
      }
    );
  } catch (err) {
    res.json({ success: 'false', msg: err.message });
  }
});

router.patch('/erase', async (req, res) => {
  //I want to use patch instead of post even tho both work as post is meant for adding data as a child document...

  const name = req.body.params.driverAssigned; //If it doesn't work we may need to change it so that it
  const orderId = req.body.params.orderId;
  const resourceId = req.body.params.resourceId;
  const startDate = req.body.params.startDate;
  const endDate = req.body.params.endDate;
  try {
    User.removeDriverByOrderId(
      name,
      orderId,
      resourceId,
      startDate,
      endDate,
      (err, drivers) => {
        if (err) res.json({ success: false, msg: 'Failed to find user' });
        else {
          res.json({ success: true, drivers: drivers });
          firebase
            .database()
            .ref('drivers/' + name + '/' + orderId)
            .set({ orderId: null });
        }
      }
    );
  } catch (err) {
    res.json({ success: 'false', msg: err.message });
  }
});

//Updating using persid
router.patch('/persid', async (req, res) => {
  //I want to use patch instead of post even tho both work as post is meant for adding data as a child document...
  const name = req.body.params.driver;
  const persid = req.body.params.persid; //If it doesn't work we may need to change it so that it
  const adhocId = req.body.params.adhocId;
  const status = req.body.params.status;
  try {
    User.postDriverbyNameAdhoc(name, adhocId, status, (err, drivers) => {
      if (err) res.json({ success: false, msg: 'Failed to find user' });
      else {
        res.json({ success: true, drivers: drivers });
      }
    });
  } catch (err) {
    res.json({ success: 'false', msg: err.message });
  }
});

module.exports = router;
