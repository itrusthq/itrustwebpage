const express = require('express');
const User = require('../../models/User');
const router = express.Router();
const config = require('config');

// @route GET api/driver
router.get('/', async (req, res) => {
  try {
    User.getVehicles((err, vehicles) => {
      if (err) res.json({ success: false, msg: 'Failed to find user' });
      else res.json({ success: true, vehicles: vehicles });
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

router.patch('/', async (req, res) => {
  //I want to use patch instead of post even tho both work as post is meant for adding data as a child document...

  const name = req.body.params.vehicleAssigned; //If it doesn't work we may need to change it so that it
  const orderId = req.body.params.orderId;
  const resourceId = req.body.params.resourceId;
  const startDate = req.body.params.startDate;
  const endDate = req.body.params.endDate;
  try {
    User.postVehiclebyId(
      name,
      orderId,
      resourceId,
      startDate,
      endDate,
      (err, vehicles) => {
        if (err) res.json({ success: false, msg: 'Failed to find user' });
        else res.json({ success: true, vehicles: vehicles });
      }
    );
  } catch (err) {
    res.json({ success: 'false', msg: err.message });
  }
});

router.patch('/erase', async (req, res) => {
  //I want to use patch instead of post even tho both work as post is meant for adding data as a child document...

  const name = req.body.params.vehicleAssigned; //If it doesn't work we may need to change it so that it
  const orderId = req.body.params.orderId;
  const resourceId = req.body.params.resourceId;
  const startDate = req.body.params.startDate;
  const endDate = req.body.params.endDate;
  try {
    User.removeVehicleById(
      name,
      orderId,
      resourceId,
      startDate,
      endDate,
      (err, vehicles) => {
        if (err) res.json({ success: false, msg: 'Failed to find user' });
        else res.json({ success: true, vehicles: vehicles });
      }
    );
  } catch (err) {
    res.json({ success: 'false', msg: err.message });
  }
});

module.exports = router;
