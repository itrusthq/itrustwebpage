const express = require('express');
const User = require('../../models/User');
const router = express.Router();
const config = require('config');
const { Client, Status } = require('@googlemaps/google-maps-services-js');
const axios = require('axios');
const firebase = require('firebase');
const { google } = require('googleapis');
/* var FCM = require('fcm-node');

var serverKey = require('./service-account.json'); //put the generated private key path here

var fcm = new FCM(serverKey); */
router.post('/', async (req, res) => {
  let adhoc = new User.adhoc({
    location: req.body.location,
    destination: req.body.destination,
    vehicle: req.body.vehicle,
    quantity: req.body.quantity,
    reason: req.body.reason,
    sharedResource: req.body.sharedResource,
  });
  User.postAdhoc(adhoc, (err, callback) => {
    if (err) console.log({ success: false, msg: 'Failed to add Adhoc order' });
    else res.json({ success: true, msg: 'Added Adhoc Order' });
  });
});

router.patch('/', async (req, res) => {
  let id = req.body.params.id;
  let status = req.body.params.status;
  let persid = req.body.params.persid;
  let name = req.body.params.name;
  User.patchAdhoc(id, status, persid, name, (err, callback) => {
    if (err) console.log({ success: false, msg: err });
    else res.json({ success: true, msg: 'Updated Adhoc order' });
  });
});

router.get('/', async (req, res) => {
  User.getAdhoc((err, adhoc) => {
    let distance = 0;

    if (req.query.location != undefined) {
      async function getAll() {
        let location1 = req.query.location.replace(/\s/g, '');
        let destination = req.query.destination.replace(/\s/g, '');
        let driverLat = req.query.latitude;
        let driverLong = req.query.longitude;
        destination = destination.replace('#', '');
        let res0 = await axios.get(
          'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
            driverLat.toString() +
            ',' +
            driverLong.toString() +
            '&key=AIzaSyDga0vz02MHkeyK2QbMbHdMCRY9px56Is4'
        );

        let res1 = await axios.get(
          'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=' +
            res0.data.results[0].formatted_address.toString() +
            '&destinations=' +
            location1.toString() +
            '&key=AIzaSyDga0vz02MHkeyK2QbMbHdMCRY9px56Is4'
        );
        let location;

        distance = res1.data.rows[0].elements[0].distance.value;
        let time = res1.data.rows[0].elements[0].duration.value;
        res.json({ success: true, adhoc: adhoc, distance, time });
      }
      getAll();
    }
    if (err) console.log({ success: false, msg: 'Failed to add Adhoc order' });
    else if (req.query.location === undefined) {
      res.json({ success: true, adhoc: adhoc, distance });
    }
  });
});
router.post('/adhoc', async (req, res) => {
  /* function getAccessToken() {
    return new Promise(function (resolve, reject) {
      var key = require('./service-account.json');
      var jwtClient = new google.auth.JWT(
        key.client_email,
        null,
        key.private_key,
        'https://www.googleapis.com/auth/firebase.messaging',
        null
      );
      jwtClient.authorize(async function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
        console.log(tokens.access_token);
        let string = req.body.token.toString();

        var message = {
          //this may vary according to the message type (single recipient, multicast, topic, et cetera)
          to: string,

          data: {
            //you can send only notification or only data(or include both)
            my_key: 'my value',
            my_another_key: 'my another value',
          },
        };

        fcm.send(message, function (err, response) {
          if (err) {
            console.log('Something has gone wrong!');
          } else {
            console.log('Successfully sent with response: ', response);
          }
        }); */
  /*          const config = {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + tokens.access_token,
          },
          params: {
            message: {token: string,
              notification: {
                title: 'FCM Message',
                body: 'This is a message from FCM',
              },
            },
            
          }
        };

        console.log(string);
        let body = {
          message: {
            notification: {
              title: 'FCM Message',
              body: 'This is a message from FCM',
            },
          },
          token: string,
        };
        let data = {
          message: {
            notification: {
              title: 'FCM Message',
              body: 'This is a message from FCM',
            },
            webpush: {
              headers: {
                Urgency: 'high',
              },
              notification: {
                body: 'This is a message from FCM to web',
                requireInteraction: 'true',
              },
            },
          },
          token: string,
        };

        try {
          let res1 = await axios.post(
            'https://fcm.googleapis.com/v1/projects/orderapp-89c82/messages:send',
            data,
            config
          );
        } catch (err) {
          console.log(err.message);
        }  */
  /*  });
    });
  }
  let tk = getAccessToken();
  console.log(tk);
  let token =
    'AAAACf8xHj8:APA91bErhUBGPW2ZLEx6C3VkkacgXudQAYM63PNYdGwXBj60--QVl13EPHqnTe0MBFt0H286V3xVk19aaPU2NVRAWUxmdgLebk4cHKxkDTYmU1HlKt_xwfrunig2Zac9VfczGQfzFjRt'; */
});
module.exports = router;
