const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');
const User = require('../../models/User');

// @route GET api/Auth
router.get('/', async (req, res) => {
  try {
    var decoded = jwt.verify(req.query.id, config.get('jwtSecret'));

    User.getUserbyId(decoded.user.id, (err, user) => {
      if (err) res.json({ success: false, msg: 'Failed to find user' });
      else {
        User.getOrderbyPersid(user.persid, (err, order) => {
          if (err) res.json({ success: false, msg: 'Failed to find orders' });
          else res.json({ success: true, orders: order });
        });
      }
    });
  } catch (err) {
    res.json({ success: false });
  }
});

router.get('/user', async (req, res) => {
  try {
    let query = JSON.parse(req.query.id);
    var decoded = jwt.verify(query, config.get('jwtSecret'));

    User.getUserbyId(decoded.user.id, (err, user) => {
      console.log(user);
      if (err) res.json({ success: false, msg: 'Failed to find user' });
      else res.json({ success: true, user: user });
    });
  } catch (err) {
    res.json({ success: false });
  }
});

router.post('/', async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { persid, password } = req.body;
  User.getUserbyPersid(persid, (err, user) => {
    if (err) return res.json({ success: false, msg: err });
    if (!user) {
      return res.json({ success: false, msg: 'User not found' });
    }

    if (password.localeCompare(user.password) == 0) {
      const payload = {
        user: {
          id: user.id,
          authorized: user.authorized,
        },
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err;
          res.json({ success: true, user: { token, user } });
        }
      );
    } else {
      return res.json({ success: false, msg: 'Wrong password' });
    }
  });
});

module.exports = router;
