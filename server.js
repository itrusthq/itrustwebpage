// server.js

const express = require('express');
const connectDB = require('./config/db');
const path = require('path');
var app = express();
var cors = require('cors');
app.use(cors());
// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require('firebase/app');

// Add the Firebase products that you want to use
require('firebase/auth');
require('firebase/firestore');
require('firebase/database');

// Connect Database
connectDB();

const firebaseConfig = {
  apiKey: 'AIzaSyCkbfLsnJjrvQkkV0DctV86n4H52ToeWwQ',
  authDomain: 'orderapp-89c82.firebaseapp.com',
  databaseURL: 'https://orderapp-89c82.firebaseio.com',
  projectId: 'orderapp-89c82',
  storageBucket: 'orderapp-89c82.appspot.com',
  messagingSenderId: '42936114751',
  appId: '1:42936114751:web:60d6d9d5a90b45f030a28a',
};

var firebaseProject = firebase.initializeApp(firebaseConfig);
var defaultFirestore = firebaseProject.firestore();
var database = firebase.database();
/* 
defaultFirestore
  .collection('orders')
  .get()
  .then((snapshot) => {
    snapshot.forEach((doc) => {
      console.log(doc.id, '=>', doc.data());
    });
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  }); */
firebase
  .database()
  .ref('orders/' + 23)
  .set({
    username: 'name1',
    email: 'email1',
    profile_picture: 'imageUrl',
  })
  .catch((err) => {
    console.log('error', err);
  });

/* console.log(firebaseProject.name);
 */
//Init Middleware
app.use(express.json({ extended: false }));

//Define Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/post', require('./routes/api/post'));
app.use('/api/order', require('./routes/api/order'));
app.use('/api/driver', require('./routes/api/driver'));
app.use('/api/vehicle', require('./routes/api/vehicle'));
app.use('/api/adhoc', require('./routes/api/adhoc'));
app.use(express.static('views/build'));

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'views', 'build', 'index.html'));
});

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
