import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Dashboard from './components/dashboard/Dashboard';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import Alert from './components/Alert';
import newOrder from './components/newOrder/newOrder';
import store from './store';
import processing from './components/processing/proccssing';
import history from './components/history/history';
import itmsdashboard from './components/ITMSdashboard/itmsdashbord';
import indent from './components/indents/indent';
import resources from './components/resources/resources';
import PrivateRoute from '../src/components/routing/PrivateRoute';
import * as serviceWorker from './serviceWorker';
import { PrivateRoute2 } from '../src/components/routing/PrivateRoute';
import adhoc from '../src/components/adhoc/adhoc';
import driver from '../src/components/driver/driver';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Alert />
      <Route exact path='/' component={App}></Route>
      <Switch>
        <PrivateRoute
          exact
          path='/dashboard'
          component={Dashboard}
        ></PrivateRoute>
        <PrivateRoute
          exact
          path='/newOrder'
          component={newOrder}
        ></PrivateRoute>
        <PrivateRoute
          exact
          path='/processing'
          component={processing}
        ></PrivateRoute>
        <PrivateRoute exact path='/history' component={history}></PrivateRoute>
        <PrivateRoute2
          exact
          path='/itms'
          component={itmsdashboard}
        ></PrivateRoute2>
        <PrivateRoute2
          exact
          path='/itms/indent'
          component={indent}
        ></PrivateRoute2>
        <PrivateRoute2
          exact
          path='/itms/resources'
          component={resources}
        ></PrivateRoute2>
        <PrivateRoute2
          exact
          path='/itms/adhoc'
          component={adhoc}
        ></PrivateRoute2>{' '}
        <PrivateRoute2
          exact
          path='/itms/driver'
          component={driver}
        ></PrivateRoute2>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
//initalizeFirebase();
if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('./firebase-messaging-sw.js')
    .then(function (registration) {
      console.log('Registration successful, scope is:', registration.scope);
    })
    .catch(function (err) {
      console.log('Service worker registration failed, error:', err);
    });
}
