import * as firebase from 'firebase/app';
import 'firebase/messaging';
const initializedFirebaseApp = firebase.initializeApp({
  // Project Settings => Add Firebase to your web app

  apiKey: 'AIzaSyCkbfLsnJjrvQkkV0DctV86n4H52ToeWwQ',
  authDomain: 'orderapp-89c82.firebaseapp.com',
  databaseURL: 'https://orderapp-89c82.firebaseio.com',
  projectId: 'orderapp-89c82',
  storageBucket: 'orderapp-89c82.appspot.com',
  messagingSenderId: '42936114751',
  appId: '1:42936114751:web:60d6d9d5a90b45f030a28a',

  messagingSenderId: '42936114751',
});
let messaging = 'not supported';
if (firebase.messaging.isSupported()) {
  messaging = initializedFirebaseApp.messaging();
  messaging.usePublicVapidKey(
    // Project Settings => Cloud Messaging => Web Push certificates
    'BPJGBcvaEb1Zt_DLHjY4gCh0FYziCcPxa1QcSBn442fRjyn5X5QyIUhUT4fGu6gLDtFbZYTzn9Yye663tbeAJvY'
  );
} else {
  console.log('Firebase messaging is not supported');
}
export { messaging };
