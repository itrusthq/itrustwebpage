import React from 'react';

function nextPath1(props, path, orderId) {
  console.log('hello');
  props.history.push({ pathname: path, state: { orderId: orderId } });
}

export function resubmitIndent(props, path, orderId, resubmit) {
  console.log(resubmit);
  props.history.push({
    pathname: path,
    state: { orderId: orderId, resubmit: resubmit },
  });
}

export function nextPath(props, path) {
  props.history.push({ pathname: path });
}

export default nextPath1;
