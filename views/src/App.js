import React, { Fragment, useState, useEffect } from 'react';
import './App.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
//Redux
import { connect } from 'react-redux';
import { setAlert } from './actions/alert';
import PropTypes from 'prop-types';
//
import addNotification from 'react-push-notification';

import { setData } from './actions/dataorder';
//PrivateRoute
import Auth from '../src/components/auth/auth';
import Alert from '../src/components/Alert';
import auth1 from '../src/components/routing/PrivateRoute';
import { config } from 'react-transition-group';
let data = '';
const App = (props) => {
  const [formData, setFormData] = useState({
    persid: '',
    password: '',
    authorized: false,
  });

  const { persid, password } = formData;

  data = formData;
  document.title = 'ITRUST';
  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    const newUser = {
      persid,
      password,
    };

    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const body = JSON.stringify(newUser);

      const res = await axios.post('/api/auth', body, config);
      console.log(res.data);
      if (res.data.success === true) {
        props.setAlert('User has logged in', 'success');
        props.setData(data);
        console.log(res.data.user.user.authorized);
        console.log(res.data.user.token);
        localStorage.setItem('token', JSON.stringify(res.data.user.token));
        console.log('SET');
        Auth();
        setFormData({ authorized: true });
        if (res.data.user.user.authorized == true) {
          props.history.push('/itms', '');
        } else props.history.push('/dashboard');
      }
      if (res.data.success == false) {
        props.setAlert('Invalid Login Credentials', 'danger');
        console.log(res.data.user);
      }
    } catch (err) {
      console.log(err.data);
    }
    document.title = 'ITRUST';
  };
  useEffect(() => {
    document.title = 'ITRUST';
  });
  App.propTypes = {
    setAlert: PropTypes.func.isRequired,
    setData: PropTypes.func.isRequired,
  };
  return (
    <div id='bg'>
      <title>ITRUST</title>
      <div className='login'>
        <Form className='formCSS' onSubmit={(e) => onSubmit(e)}>
          <div className='illustration'>
            <img
              className='image'
              src={require('../src/components/dashboard/images/logo3.jpg')}
            />
          </div>
          <Alert></Alert>

          <Form.Group className='test' controlId='exampleForm.ControlInput1'>
            <Form.Control
              type='number'
              placeholder='PERSID'
              name='persid'
              value={persid}
              onChange={(e) => onChange(e)}
              required
            ></Form.Control>
          </Form.Group>
          <Form.Group controlId='formBasicPassword'>
            <Form.Control
              type='Password'
              placeholder='Password'
              name='password'
              value={password}
              onChange={(e) => onChange(e)}
              required
            ></Form.Control>
          </Form.Group>
          <Button
            variant='primary'
            type='submit'
            className='btn btn-primary btn-lg btn-block'
            value='Login'
            style={{ backgroundColor: '#3D74DB' }}
          >
            Login
          </Button>
        </Form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  console.log(state);
  return state;
};
export default connect(mapStateToProps, { setAlert, setData })(App);
