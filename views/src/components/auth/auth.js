import React from 'react';
import { TextareaAutosize } from '@material-ui/core';
import axios from 'axios';

async function Auth() {
  let login = false;
  let data = localStorage.getItem('token');
  console.log(data);
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  let res = await axios.get(
    '/api/auth/user',
    {
      params: {
        id: data,
      },
    },
    config
  );
  return res.data.success;
}

export default Auth;
