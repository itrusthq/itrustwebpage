import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route } from 'react-router-dom';
import SideNav, {
  Toggle,
  Nav,
  NavItem,
  NavIcon,
  NavText,
} from '@trendmicro/react-sidenav';
import './react-sidenav.css';
import 'react-bootstrap-icons';
import { GearWide } from 'react-bootstrap-icons';

class SideMenu extends Component {
  render() {
    return (
      <div>
        <Route
          render={({ location, history }) => (
            <React.Fragment>
              <SideNav
                onSelect={(selected) => {
                  const to = '/' + selected;
                  if (to == '/') localStorage.clear();
                  if (location.pathname !== to) {
                    history.push(to);
                  }
                }}
              >
                <SideNav.Toggle />
                <SideNav.Nav defaultSelected='Home'>
                  <NavItem eventKey='dashboard'>
                    <NavIcon>
                      <svg
                        className='bi bi-house-door-fill'
                        width='2em'
                        height='2em'
                        viewBox='0 1.5 16.5 16'
                        fill='currentColor'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path d='M6.5 10.995V14.5a.5.5 0 01-.5.5H2a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z' />
                        <path
                          fillRule='evenodd'
                          d='M13 2.5V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z'
                          clipRule='evenodd'
                        />
                      </svg>
                    </NavIcon>
                    <NavText>Dashboard</NavText>
                  </NavItem>

                  <NavItem eventKey='newOrder'>
                    <NavIcon>
                      <svg
                        className='bi bi-plus-square-fill'
                        width='2em'
                        height='2em'
                        viewBox='0 0 16.5 18'
                        fill='currentColor'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          fillRule='evenodd'
                          d='M2 0a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2H2zm6.5 4a.5.5 0 00-1 0v3.5H4a.5.5 0 000 1h3.5V12a.5.5 0 001 0V8.5H12a.5.5 0 000-1H8.5V4z'
                          clipRule='evenodd'
                        />
                      </svg>
                    </NavIcon>
                    <NavText>New Order</NavText>
                  </NavItem>

                  <NavItem eventKey='processing'>
                    <NavIcon>
                      <svg
                        className='bi bi-gear-fill'
                        width='2em'
                        height='2em'
                        viewBox='0 0 16 16'
                        fill='currentColor'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          fillRule='evenodd'
                          d='M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 01-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 01.872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 012.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 012.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 01.872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 01-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 01-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 100-5.86 2.929 2.929 0 000 5.858z'
                          clipRule='evenodd'
                        />
                      </svg>
                    </NavIcon>
                    <NavText>Processing</NavText>
                  </NavItem>
                  <NavItem eventKey='history'>
                    <NavIcon>
                      <svg
                        className='bi bi-clock-history'
                        width='2em'
                        height='2em'
                        viewBox='0 0 16 16'
                        fill='currentColor'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          fillRule='evenodd'
                          d='M8.515 1.019A7 7 0 008 1V0a8 8 0 01.589.022l-.074.997zm2.004.45a7.003 7.003 0 00-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 00-.439-.27l.493-.87a8.025 8.025 0 01.979.654l-.615.789a6.996 6.996 0 00-.418-.302zm1.834 1.79a6.99 6.99 0 00-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 00-.214-.468l.893-.45a7.976 7.976 0 01.45 1.088l-.95.313a7.023 7.023 0 00-.179-.483zm.53 2.507a6.991 6.991 0 00-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 01-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 01-.401.432l-.707-.707z'
                          clipRule='evenodd'
                        />
                        <path
                          fillRule='evenodd'
                          d='M8 1a7 7 0 104.95 11.95l.707.707A8.001 8.001 0 118 0v1z'
                          clipRule='evenodd'
                        />
                        <path
                          fillRule='evenodd'
                          d='M7.5 3a.5.5 0 01.5.5v5.21l3.248 1.856a.5.5 0 01-.496.868l-3.5-2A.5.5 0 017 9V3.5a.5.5 0 01.5-.5z'
                          clipRule='evenodd'
                        />
                      </svg>
                    </NavIcon>
                    <NavText>History</NavText>
                  </NavItem>
                  <NavItem eventKey=''>
                    <NavIcon>
                      <svg
                        className='bi bi-box-arrow-in-left'
                        width='2em'
                        height='2em'
                        viewBox='0 1.5 16 16'
                        fill='currentColor'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          fillRule='evenodd'
                          d='M7.854 11.354a.5.5 0 000-.708L5.207 8l2.647-2.646a.5.5 0 10-.708-.708l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708 0z'
                          clipRule='evenodd'
                        />
                        <path
                          fillRule='evenodd'
                          d='M15 8a.5.5 0 00-.5-.5h-9a.5.5 0 000 1h9A.5.5 0 0015 8z'
                          clipRule='evenodd'
                        />
                        <path
                          fillRule='evenodd'
                          d='M2.5 14.5A1.5 1.5 0 011 13V3a1.5 1.5 0 011.5-1.5h8A1.5 1.5 0 0112 3v1.5a.5.5 0 01-1 0V3a.5.5 0 00-.5-.5h-8A.5.5 0 002 3v10a.5.5 0 00.5.5h8a.5.5 0 00.5-.5v-1.5a.5.5 0 011 0V13a1.5 1.5 0 01-1.5 1.5h-8z'
                          clipRule='evenodd'
                        />
                      </svg>
                    </NavIcon>
                    <NavText>Log Out</NavText>
                  </NavItem>
                </SideNav.Nav>
              </SideNav>
            </React.Fragment>
          )}
        />
      </div>
    );
  }
}

export default SideMenu;
