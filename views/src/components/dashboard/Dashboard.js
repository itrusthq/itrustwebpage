import React, { Fragment, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { render } from 'react-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import './Dashboard.css';
import image from './images/img.jpg';
import { Link } from 'react-router-dom';
import newOrder from '../newOrder/newOrder';
import store from '../../store';
import SideMenu from '../sidemenu/sidemenu';
class Dashboard extends React.Component {
  //Stateful Class
  //To get access to any states and props
  constructor(props) {
    super(props);
    console.log(this.props.user);
    this.state = {};
    console.log(store.getState());
  }
  nextPath(path) {
    this.props.history.push(path);
  }
  ifExists = () => {
    console.log(this.state);
    if (this.state.authorised == true) {
      return (
        <Row className='Row'>
          <Col md={{ span: 4, offset: 5 }}>
            <Button onClick={() => this.nextPath('/itms')} className='btn-lg'>
              New Order
            </Button>
          </Col>
        </Row>
      );
    }
  };

  componentDidMount() {
    document.title = 'ITRUST';
  }

  render() {
    return (
      <div
        className='overlayFix'
        style={{
          color: 'black',
          backgroundColor: 'rgb(235, 231, 221)',
        }}
      >
        {' '}
        <SideMenu></SideMenu>
        <Container>
          <Row></Row>
          <Col className='col-xs-1 text-center'>
            <h1 className='h1Class'>Dashboard</h1>
          </Col>
          <Row className='Row'>
            <Col className='col-xs-1 text-center'>
              <Button
                onClick={() => this.nextPath('/newOrder')}
                className='btn-lg'
                style={{ backgroundColor: '#3D74DB', width: 150 }}
              >
                New Order
              </Button>
            </Col>
          </Row>

          <Row className='Row'>
            <Col className='col-xs-1 text-center'>
              <Button
                onClick={() => this.nextPath('/processing')}
                className='btn-lg'
                style={{ backgroundColor: '#3D74DB', width: 150 }}
              >
                Processing
              </Button>
            </Col>
          </Row>
          <Row className='Row'>
            <Col className='col-xs-1 text-center'>
              <Button
                onClick={() => this.nextPath('/history')}
                className='btn-lg'
                style={{ backgroundColor: '#3D74DB', width: 150 }}
              >
                History
              </Button>
            </Col>
          </Row>
          {this.ifExists()}
        </Container>
      </div>
    );
  }
}

export default Dashboard;
