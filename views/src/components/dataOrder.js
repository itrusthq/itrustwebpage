import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'; //For calling an action or getting a state

const data = ({ data }) =>
  data !== null &&
  data.length > 0 &&
  data.map(data => (
    <div key={data.id} className={`alert alert-${data.dataType}`}>
      {data}
    </div>
  ));

data.protoTypes = {
  data: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  data: state.data
});
export default connect(mapStateToProps)(data);
