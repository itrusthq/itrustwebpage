import React, { Component } from 'react';
import { TopNav } from '../topnav/topnav';
import axios from 'axios';
import { Button, Col, Container, Row, Card, Collapse } from 'react-bootstrap';
import Fade from 'react-reveal/Fade'; // Importing Zoom effect
import MaterialTable from 'material-table';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import nextPath from '../../nextPath';
import { Form } from 'react-bootstrap';
import image from '../dashboard/images/img.jpg';
import { connect } from 'react-redux';
import { setAlert } from '../../actions/alert';
import PropTypes from 'prop-types';
import moment from 'moment';
// import Modal from 'react-bootstrap/Modal';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DriveEtaIcon from '@material-ui/icons/DriveEta';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
class indent extends React.Component {
  table = [];
  detailPanel = [];
  constructor(props) {
    super(props);
    let arrOpen = new Array(20);
    arrOpen.fill(true, 0, 20);
    this.state = {
      arrOpen: arrOpen,
      resources: {},
      order: {},
      show: false,
      driver: [{ orderId: [''] }],
      orderAll: [],
      vehicle: [],
      show: false,
      showDriver: false,
      update: '',
      reason: '',
      detailPanelData: '',
    };
    this.googleInput = React.createRef();
    let data = localStorage.getItem('token');
    //console.log(data);
    this.state.data = data;
    this.getIndents(data);
    this.getDrivers();
  }

  componentDidMount() {
    document.title = 'Indent';
  }

  async getDrivers() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get('/api/driver/driver', config);
    //console.log(res);
    this.setState({ driver: res.data.drivers });
    this.getVehicles();
  }

  getVehicles = async () => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    let res = await axios.get('/api/vehicle', config);

    console.log(res.data);
    this.setState({
      vehicle: res.data.vehicles,
    });
    console.log(this.state);
  };

  async getIndents(data) {
    //console.log(data);
    //console.log(data.token);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth/user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    //console.log(res.data);
    let res1 = await axios.get(
      '/api/post/all',

      {
        params: {
          hub: res.data.user.hub,
        },
      }
    );
    //console.log(res1);
    this.setState({
      //I need to change this for multiple orders so that it is res1.data.orders instead of res1.data.orders[0].orders. This is actually going to create quite a big problem as we will have to deal with orders[x].persid and orders[x].order[i] because there will be a lot more data and each persid will have their own order. If there is more than one hub than we need to check for that too before we add it to the indents. This going to be quite the overhual.
      order: res1.data.orders[0].orders,
      orderAll: res1.data.orders, //Keep it like this for now because its simpler. Remember this isn't one order and rather the entire history of orders. orders[x].orders is one indent made.
      persid: res1.data.orders[0].persid,
      unit: res1.data.orders[0].unit,
      hub: res1.data.orders[0].hub,
    });
    //console.log(this.state);
  }
  changeOpen = (x) => {
    let arrOpen = [...this.state.arrOpen];
    arrOpen[x] = !arrOpen[x];
    console.log(arrOpen[x]);
    this.setState({ arrOpen });
  };

  render() {
    //Done updating rowData for approveIndent and rejectIndent
    let newTable = [];
    if (window.screen.width < 700) {
      this.createTable();
      console.log(this.table);
      newTable = (
        <div>
          <h1>Indents</h1>{' '}
          {this.table.map((item, index) => (
            <div
              style={{
                paddingLeft: 10,
                paddingTop: 10,
                paddingRight: 10,
              }}
            >
              <Fade left>
                <Card
                  bg='light'
                  key={index}
                  style={{}}
                  text='dark'
                  className='mb-2'
                  style={{ boxShadow: '2px 3px grey' }}
                >
                  <Card.Header
                    style={{
                      backgroundColor: 'rgba(0,0,0,0.14)',
                      fontWeight: 'bold',
                    }}
                  >
                    {item.orderId}{' '}
                    <Button
                      style={{ float: 'right' }}
                      onClick={() => this.changeOpen(index)}
                    >
                      {this.state.arrOpen[index] ? 'Close' : 'Open'}
                    </Button>
                  </Card.Header>
                  <Collapse in={this.state.arrOpen[index]}>
                    <Card.Body style={{ backgroundColor: 'rgba(0,0,0,.07)' }}>
                      <div>
                        <table>
                          <thead style={{ maxWidth: 100 }}>
                            <tr>
                              <th
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                Activity
                              </th>
                              <th
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                Type
                              </th>
                              <th
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                Status
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                {item.activity}
                              </td>
                              <td
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                {item.types}
                              </td>
                              <td
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                {item.processing}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <hr></hr>
                        <table>
                          <thead style={{ maxWidth: 100 }}>
                            <tr>
                              <th
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                POC
                              </th>
                              <th
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                Location
                              </th>
                              <th
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                Unit
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                {item.poc}
                              </td>
                              <td
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                {item.location}
                              </td>
                              <td
                                style={{
                                  width: 90,
                                  padding: 2,
                                }}
                              >
                                {item.unit}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <Card.Text>
                        <li>Start Date: {item.startDate}</li>
                        <li>End Date: {item.endDate}</li>

                        {item.extraInfo.localeCompare('') === 0 ? null : (
                          <li> Remarks: {item.extraInfo} </li>
                        )}

                        {item.admin === undefined ? null : (
                          <li>Admin: {item.admin}</li>
                        )}

                        {item.reason === undefined ||
                        item.reason.localeCompare('') === 0 ? null : (
                          <li> Reason For Rejection: {item.reason}</li>
                        )}
                      </Card.Text>
                    </Card.Body>
                  </Collapse>
                  <Collapse in={this.state.arrOpen[index]}>
                    <Card.Footer
                      style={{ backgroundColor: 'rgba(0,0,0,0.14)' }}
                    >
                      <Button
                        onClick={(event) =>
                          this.approveIndent(item, item.orderId)
                        }
                      >
                        Approve Indent
                      </Button>{' '}
                      <Button
                        onClick={(event) =>
                          this.setState({
                            show: true,
                            rowData: item,
                            orderId: item.orderId,
                          })
                        }
                      >
                        Reject Indent
                      </Button>{' '}
                      <hr></hr>
                      <Button
                        onClick={(event) =>
                          this.setState({
                            showDriver: true,
                            detailPanelData: item,
                          })
                        }
                      >
                        Approve Driver
                      </Button>{' '}
                      <Button
                        onClick={(event) =>
                          this.setState({
                            showVehicle: true,
                            detailPanelData: item,
                          })
                        }
                        variant='secondary'
                      >
                        Approve Vehicle
                      </Button>
                    </Card.Footer>
                  </Collapse>
                </Card>
              </Fade>
              <hr />
              <this.rejectModal />
              <this.approveDriverModal />
              <this.approveVehicleModal />
            </div>
          ))}
        </div>
      );
    }
    return (
      <div
        className='overlay'
        style={{
          overflowY: 'scroll',
          background: 'rgb(207, 220, 227, 1.0)',
        }}
      >
        <TopNav />
        <br></br>
        <Container fluid>
          <Row>
            <Col className='col-md-4'>
              <Button
                style={{ width: 170 }}
                onClick={() => {
                  nextPath(this.props, '/newOrder');
                }}
              >
                Create a new indent
              </Button>
            </Col>
            <Col className='col-md-4 ml-auto'>
              <Button
                style={{ width: 150 }}
                onClick={() => {
                  nextPath(this.props, '/processing');
                }}
              >
                Processing Page
              </Button>
            </Col>
          </Row>
        </Container>
        <br></br>
        <link
          rel='stylesheet'
          href='https://fonts.googleapis.com/icon?family=Material+Icons'
        ></link>
        {this.state.allData} <table />
        <div style={{ maxWidth: '100%' }}>
          {window.screen.width < 700 ? (
            newTable
          ) : (
            /*  <MaterialTable
              style={{
                backgroundColor: 'rgb(179, 196, 220)',
                borderTop: '4px solid rgb(179, 196, 220)',
              }}
              columns={[
                {
                  title: 'Requester',
                  field: 'persid',
                  fontSize: 8,
                },
                { title: 'Activity', field: 'activity' },
                { title: 'Order ID', field: 'orderId', fontSize: 8 },
                { title: 'Processing', field: 'processing' },
              ]}
              data={this.createTable()}
              title='Indents'
              options={{
                header: true,
                searchFieldStyle: {
                  maxWidth: 200,
                },

                headerStyle: {
                  backgroundColor: 'lightgrey',
                  width: 30,
                  padding: 5,
                  color: 'black',
                  fontSize: 12,
                  borderTop: '1px solid black',
                },
                cellStyle: {
                  width: 30,
                  fontSize: 12,
                  padding: 10,
                },

                //add the option to approve/reject an indent and it should probably open up an dialog box
                rowStyle: {
                  backgroundColor: 'rgb(179, 196, 200)',
                  color: 'black',
                  width: 40,
                  maxWidth: 50,
                  fontSize: 12,
                  padding: 5,
                },

                actionsColumnIndex: 0,
                addRowPosition: 'last',
              }}
              actions={[
                {
                  icon: CheckIcon,
                  tooltip: 'Approve Indent',

                  onClick: (event, rowData) =>
                    this.approveIndent(rowData, rowData.orderId), //need to pass in rowData.persid as well
                },
                {
                  icon: ClearIcon,
                  tooltip: 'Reject Indent',
                  onClick: (event, rowData) =>
                    this.setState({
                      show: true,
                      rowData: rowData,
                      orderId: rowData.orderId,
                    }),
                },
                ,
              ]}
              detailPanel={[
                {
                  render: (rowData, drivers) => {
                    return (
                      <MaterialTable
                        options={{ actionsColumnIndex: -1 }}
                        columns={[
                          { title: 'Driver assigned', field: 'driverAssigned' },

                          {
                            title: 'Vehicle assigned',
                            field: 'vehicleAssigned',
                          },
                          {
                            title: 'Assign Driver',
                            field: 'driverName',
                            render: (rowData) => (
                              <Form.Control
                                defaultValue={this.state.driver[0].name}
                                onChange={(e) =>
                                  this.onDriverSelected(
                                    e.target.value,
                                    e.target,
                                    rowData
                                  )
                                }
                                as='select'
                              >
                                {this.createDriverSelect(rowData)}
                              </Form.Control>
                            ),
                          },
                          {
                            title: 'Assign Vehicle',
                            field: 'vehicleName',
                            render: (rowData) => (
                              <Form.Control
                                onChange={(e) =>
                                  this.onVehicleSelected(
                                    e.target.value,
                                    e.target,
                                    rowData
                                  )
                                }
                                as='select'
                              >
                                {this.createVehicleSelect(rowData)}
                              </Form.Control>
                            ),
                          },
                        ]}
                        actions={[
                          {
                            icon: PersonAddIcon,

                            tooltip: 'Approve Driver',

                            onClick: (event, rowData) =>
                              this.approveDriver(rowData),
                          },
                          {
                            icon: PersonAddDisabledIcon,
                            tooltip: 'Remove Driver',
                            onClick: (event, rowData) =>
                              this.removeDriver(rowData),
                          },
                          {
                            icon: DriveEtaIcon,
                            tooltip: 'Approve Vehicle',
                            onClick: (event, rowData) =>
                              this.approveVehicle(rowData),
                          },
                          {
                            icon: RemoveCircleIcon,
                            tooltip: 'Remove Vehicle',
                            onClick: (event, rowData) =>
                              this.removeVehicle(rowData),
                          },
                        ]}
                        data={this.createDetailPanel(rowData)}
                        title='Additional Information'
                      />
                    );
                  },
                },
              ]}
            /> */
            <div style={{ maxWidth: '100%' }}>
              <MaterialTable
                style={{
                  backgroundColor: 'rgb(179, 196, 220)',
                  borderTop: '4px solid rgb(179, 196, 220)',
                }}
                columns={[
                  { title: 'Requester', field: 'persid', type: 'numeric' },
                  { title: 'Order ID', field: 'orderId' },
                  { title: 'Unit', field: 'unit' },

                  { title: 'Activity', field: 'activity' },

                  { title: 'Type', field: 'types' },
                  { title: 'POC', field: 'poc' },
                  { title: 'Location', field: 'location' },
                  {
                    title: 'Start Date',
                    field: 'startDate',
                    type: 'datetime',
                  },
                  { title: 'End Date', field: 'endDate', type: 'datetime' },
                  { title: 'Extrainfo', field: 'extraInfo' },
                  { title: 'Processing', field: 'processing' },
                  { title: 'Admin', field: 'admin' },
                  { title: 'Reason', field: 'reason' },
                ]}
                data={this.createTable()}
                title='Indents'
                options={{
                  header: true,
                  headerStyle: {
                    backgroundColor: 'lightgrey',

                    color: 'black',
                    borderTop: '1px solid black',
                  },
                  //add the option to approve/reject an indent and it should probably open up an dialog box
                  rowStyle: {
                    backgroundColor: 'rgb(179, 196, 200)',
                    color: 'black',
                  },

                  actionsColumnIndex: 0,
                  addRowPosition: 'last',
                }}
                actions={[
                  {
                    icon: CheckIcon,
                    tooltip: 'Approve Indent',

                    onClick: (event, rowData) =>
                      this.approveIndent(rowData, rowData.orderId), //need to pass in rowData.persid as well
                  },
                  {
                    icon: ClearIcon,
                    tooltip: 'Reject Indent',
                    onClick: (event, rowData) =>
                      this.setState({
                        show: true,
                        rowData: rowData,
                        orderId: rowData.orderId,
                      }),
                  },
                  ,
                ]}
                detailPanel={[
                  {
                    render: (rowData, drivers) => {
                      return (
                        <MaterialTable
                          columns={[
                            { title: 'Vehicle', field: 'vehicle' },
                            { title: 'Quantity', field: 'quantity' },
                            { title: 'Driver', field: 'driver' },
                            {
                              title: 'Driver Approved',
                              field: 'approve',
                              type: 'boolean',
                            },

                            {
                              title: 'Driver assigned',
                              field: 'driverAssigned',
                            },
                            {
                              title: 'Vehicle Approved',
                              field: 'approveVehicle',
                              type: 'boolean',
                            },
                            {
                              title: 'Vehicle assigned',
                              field: 'vehicleAssigned',
                            },
                            {
                              title: 'Assign Driver',
                              field: 'driverName',
                              render: (rowData) => (
                                <Form.Control
                                  defaultValue={this.state.driver[0].name}
                                  onChange={(e) =>
                                    this.onDriverSelected(
                                      e.target.value,
                                      e.target,
                                      rowData
                                    )
                                  }
                                  as='select'
                                >
                                  {this.createDriverSelect(rowData)}
                                </Form.Control>
                              ),
                            },
                            {
                              title: 'Assign Vehicle',
                              field: 'vehicleName',
                              render: (rowData) => (
                                <Form.Control
                                  onChange={(e) =>
                                    this.onVehicleSelected(
                                      e.target.value,
                                      e.target,
                                      rowData
                                    )
                                  }
                                  as='select'
                                >
                                  {this.createVehicleSelect(rowData)}
                                </Form.Control>
                              ),
                            },
                          ]}
                          actions={[
                            {
                              icon: PersonAddIcon,
                              tooltip: 'Approve Driver',
                              onClick: (event, rowData) =>
                                this.approveDriver(rowData),
                            },
                            {
                              icon: PersonAddDisabledIcon,
                              tooltip: 'Remove Driver',
                              onClick: (event, rowData) =>
                                this.removeDriver(rowData),
                            },
                            {
                              icon: DriveEtaIcon,
                              tooltip: 'Approve Vehicle',
                              onClick: (event, rowData) =>
                                this.approveVehicle(rowData),
                            },
                            {
                              icon: RemoveCircleIcon,
                              tooltip: 'Remove Vehicle',
                              onClick: (event, rowData) =>
                                this.removeVehicle(rowData),
                            },
                          ]}
                          data={this.createDetailPanel(rowData)}
                          title='Additional Information'
                        />
                      );
                    },
                  },
                ]}
              />
            </div>
          )}
        </div>
        <this.rejectModal />
        <br />
        {window.screen.width < 700 ? (
          <MaterialTable
            style={{
              backgroundColor: 'rgb(179, 196, 220)',
              borderTop: '4px solid rgb(179, 196, 220)',
              marginBottom: 50,
            }}
            columns={[
              { title: 'Requester', field: 'persid' },
              { title: 'Order ID', field: 'orderId' },
              { title: 'Activity', field: 'activity' },
              { title: 'Processing', field: 'processing' },
            ]}
            data={this.createTable1()}
            title='History'
            options={{
              header: true,
              searchFieldStyle: {
                maxWidth: 200,
              },

              headerStyle: {
                backgroundColor: 'lightgrey',
                width: 30,
                padding: 5,
                color: 'black',
                fontSize: 12,
                borderTop: '1px solid black',
              },
              cellStyle: {
                width: 30,
                fontSize: 10,
                padding: 5,
                marginLeft: -20,
              },

              //add the option to approve/reject an indent and it should probably open up an dialog box
              rowStyle: {
                backgroundColor: 'rgb(179, 196, 200)',
                color: 'black',
                width: 40,
                maxWidth: 50,
                fontSize: 10,
                padding: 5,
              },
              actionsCellStyle: {
                padding: 5,
                marginLeft: -5,
                maxWidth: 10,
              },

              actionsColumnIndex: 0,
              addRowPosition: 'last',
            }}
            detailPanel={[
              {
                render: (rowData, drivers) => {
                  return (
                    <MaterialTable
                      options={{ search: false }}
                      columns={[
                        { title: 'Vehicle', field: 'vehicle' },

                        { title: 'Driver assigned', field: 'driverAssigned' },

                        { title: 'Vehicle assigned', field: 'vehicleAssigned' },
                      ]}
                      data={this.createDetailPanel(rowData)}
                      title='Details'
                    />
                  );
                },
              },
            ]}
          />
        ) : (
          <MaterialTable
            style={{
              backgroundColor: 'rgb(179, 196, 220)',
              borderTop: '4px solid rgb(179, 196, 220)',
              marginBottom: 50,
            }}
            columns={[
              { title: 'Requester', field: 'persid' },
              { title: 'Order ID', field: 'orderId' },
              { title: 'Unit', field: 'unit' },
              { title: 'Activity', field: 'activity' },
              { title: 'Type', field: 'types' },
              { title: 'POC', field: 'poc' },
              { title: 'Location', field: 'location' },
              {
                title: 'Start Date',
                field: 'startDate',
                type: 'datetime',
              },
              { title: 'End Date', field: 'endDate', type: 'datetime' },
              { title: 'Extrainfo', field: 'extraInfo' },
              { title: 'Processing', field: 'processing' },
              { title: 'Admin', field: 'admin' },
              { title: 'Reason', field: 'reason' },
            ]}
            data={this.createTable1()}
            title='Past Indents'
            options={{
              header: true,
              headerStyle: {
                backgroundColor: 'lightgrey',
                color: 'black',
                fontSize: 20,
                borderTop: '1px solid black',
              },
              //add the option to approve/reject an indent and it should probably open up an dialog box
              rowStyle: {
                backgroundColor: 'rgb(179, 196, 200)',
                color: 'black',
              },

              tableLayout: 'auto',
              actionsColumnIndex: 0,
              addRowPosition: 'last',
            }}
            detailPanel={[
              {
                render: (rowData, drivers) => {
                  return (
                    <MaterialTable
                      columns={[
                        { title: 'Vehicle', field: 'vehicle' },
                        { title: 'Quantity', field: 'quantity' },
                        { title: 'Driver', field: 'driver' },
                        {
                          title: 'Driver Approved',
                          field: 'approve',
                          type: 'boolean',
                        },
                        { title: 'Driver assigned', field: 'driverAssigned' },
                        {
                          title: 'Vehicle Approved',
                          field: 'approveVehicle',
                          type: 'boolean',
                        },

                        { title: 'Vehicle assigned', field: 'vehicleAssigned' },
                      ]}
                      data={this.createDetailPanel(rowData)}
                      title='Additional Information'
                    />
                  );
                },
              },
            ]}
          />
        )}
      </div>
    );
  }
  //Remove a driver after assinging him to a detail
  removeDriver = async (rowData) => {
    if (rowData.driver == 0) {
      console.log(rowData);
    } else {
      rowData.approve = false;
      //I need to check if the driver is indented with the same time
      console.log('remove Data' + JSON.stringify(rowData));
      try {
        let approveState = this.state;
        console.log(approveState);
        if (
          approveState.order[rowData.orderNum].resources[rowData.resourceNum]
            .approve[rowData.approveNum] == false
        ) {
          console.log(approveState);
          return;
        }
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        };
        if (rowData.drivers == undefined)
          rowData.drivers = this.state.driver[0].name;
        console.log('hereitgoes');
        console.log(rowData);
        let resRemove = await axios.patch(
          '/api/driver/erase',
          {
            params: {
              driverAssigned: rowData.driverAssigned,
              orderId: rowData.orderId,
              resourceId: rowData.resourceId,
              startDate: rowData.startDate,
              endDate: rowData.endDate,
            },
          },
          config
        );

        //console.log(rowData.order);
        approveState.orderId = rowData.orderId;
        approveState.persid = rowData.persid;
        approveState.order = rowData.order; //This is what sets the order to approveState.order rather than having to using orderNum
        approveState.hub = rowData.hub;
        approveState.status = 'Processing (pending approval)';
        approveState.unit = rowData.unit;
        //console.log('approveState: ' + JSON.stringify(approveState.order));
        //console.log(approveState.order.resources[rowData.resourceNum]);
        approveState.order.resources[rowData.resourceNum].approve[ //orderNum and resourceNum are not orderId and resourceId they are the index of the order and resource which need to be changed
          rowData.approveNum
        ] = false; //This rather large line of code choose the correct approve to change back from true to false
        approveState.indentDriver = true; //This indent driver tell the post function that we actually want to replace the order's portion whith approveState since we are not sending only the updated order and rather the orders

        //Code below: It should be able to assign the driver to approveState. We also need to remove the driver from the list. The select method should also show the correct driver. It could also change it completely to like a text or something. Cons: they can't change the driver.

        approveState.order.resources[rowData.resourceNum].driverAssigned[
          rowData.approveNum
        ] = ''; //Assigning the rowData driver

        let res = await axios.post('/api/order', approveState, config);
        // consoleconsoleconsole.log(JSON.stringify(approveState));
        //console.log('posted');

        this.setState({ driverApprove: false });
        this.getIndents(this.state.data);
        this.getDrivers();
      } catch (err) {
        //console.log(err);
      }
    }
    //console.log(rowData.drivers);
  };
  //Approve a driver after assigning him to a detail
  approveDriver = async (rowData) => {
    console.log(rowData);
    console.log(rowData.order);
    console.log(this.state);
    if (rowData.driver == 0) {
      alert('Driver does not need to be assigned');
      return;
    }
    if (
      rowData.order.resources[rowData.resourceNum].driverAssigned[
        rowData.approveNum
      ].localeCompare('') != 0
    ) {
      alert(
        'Please remove the current assigned driver before assigning another driver'
      );
      return;
    }
    if (rowData.drivers == undefined || rowData.drivers == '') {
      alert('Please choose a driver');
      return;
    } else {
      //If the driver was chosen from the list instead of just the default being used.
      rowData.approve = true;
      //I need to check if the driver is indented with the same time
      console.log(this.state.driver);
      for (let x = 0; x < this.state.driver.length; x++) {
        console.log(this.state.driver[x]);
        if (this.state.driver[x].name.localeCompare(rowData.drivers) == 0) {
          console.log(this.state.driver[x]);
          console.log('HRLLLO');
          for (let i = 0; i < this.state.driver[x].startDate.length; i++) {
            console.log(this.state.driver[x].startDate[i]);
            if (
              moment(new Date(this.state.driver[x].startDate[i])).isBetween(
                new Date(rowData.startDate),
                new Date(rowData.endDate),
                null,
                '[]'
              ) ||
              moment(new Date(this.state.driver[x].endDate[i])).isBetween(
                new Date(rowData.startDate),
                new Date(rowData.endDate),
                null,
                '[]'
              ) ||
              moment(new Date(this.state.driver[x].endDate[i])).isBetween(
                new Date(rowData.endDate),
                new Date(rowData.startDate),

                null,
                '[]'
              ) ||
              moment(new Date(this.state.driver[x].startDate[i])).isBetween(
                new Date(rowData.endDate),
                new Date(rowData.startDate),

                null,
                '[]'
              )
            ) {
              alert('Cannot have the same driver for two days');
              return;
            }
          }
        }
      }
      try {
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        };
        let approveState = this.state;
        //console.log('hereitgoes');

        //console.log(rowData.order);
        approveState.orderId = rowData.orderId;
        approveState.persid = rowData.persid;
        approveState.order = rowData.order;
        approveState.hub = rowData.hub;
        approveState.status = 'Processing (pending approval)';
        approveState.unit = rowData.unit;
        //console.log('approveState: ' + JSON.stringify(approveState.order));
        //console.log(approveState.order.resources[rowData.resourceNum]);
        approveState.order.resources[rowData.resourceNum].approve[ //orderNum and resourceNum are not orderId and resourceId they are the index of the order and resource which need to be changed
          rowData.approveNum
        ] = true; //This rather large line of code choose the correct approve to change from false to true
        approveState.indentDriver = true; //This indent driver tell the post function that we actually want to replace the order's portion whith approveState since we are not sending only the updated order and rather the orders

        //Code below: It should be able to assign the driver to approveState. We also need to remove the driver from the list. The select method should also show the correct driver. It could also change it completely to like a text or something. Cons: they can't change the driver.

        approveState.order.resources[rowData.resourceNum].driverAssigned[
          rowData.approveNum
        ] = rowData.drivers; //Assigning the rowData driver

        //Need to also remove the driver from the list
        //One way of doing this is to post on the driver database that he is assigned to the orderId and if orderId is not undefined it is not added to the list and therefore cannot be selected. We could also add in the resource ID so we know exactly which is being talked about.
        //In order itself we need to assign the driver too therefore do that first

        //console.log('here I am ');
        //console.log(rowData.drivers);
        let res1 = axios.patch(
          '/api/driver',
          {
            params: {
              driverAssigned: rowData.drivers,
              orderId: rowData.orderId,
              resourceId: rowData.resourceId,
              startDate: rowData.startDate,
              endDate: rowData.endDate,
            },
          },
          config
        );

        let res = axios.post('/api/order', approveState, config);
        // consoleconsoleconsole.log(JSON.stringify(approveState));
        //console.log('posted');
        this.setState({ driverApprove: true });
        this.getIndents(this.state.data);
        this.getDrivers();
      } catch (err) {
        //console.log(err);
      }
    }
    //console.log(rowData.drivers);
  };

  //Approve a vehicle after assigning it to a detail
  approveVehicle = async (rowData) => {
    console.log(rowData);

    console.log(this.state);
    if (rowData.quantity == 0) {
      alert('Vehicle quantity cannot be zero please resubmit indent');
      return;
    }
    if (
      rowData.order.resources[rowData.resourceNum].vehicleAssigned[
        rowData.approveNum
      ].localeCompare('') != 0
    ) {
      alert(
        'Please remove the current assigned vehicle before assigning another vehicle'
      );
      return;
    }
    if (rowData.vehicles == undefined || rowData.vehicles == '') {
      alert('Please choose a vehicle');
      return;
    } else {
      //If the driver was chosen from the list instead of just the default being used.
      rowData.approve = true;
      //I need to check if the driver is indented with the same time
      console.log(this.state.vehicle);
      for (let x = 0; x < this.state.vehicle.length; x++) {
        console.log(this.state.vehicle[x]);
        if (
          this.state.vehicle[x].vehiclenum.localeCompare(rowData.vehicles) == 0
        ) {
          console.log('HRLLLO');
          for (let i = 0; i < this.state.vehicle[x].startDate.length; i++) {
            console.log(this.state.vehicle[x].startDate[i]);
            if (
              moment(new Date(this.state.vehicle[x].startDate[i])).isBetween(
                new Date(rowData.startDate),
                new Date(rowData.endDate),
                null,
                '[]'
              ) ||
              moment(new Date(this.state.vehicle[x].endDate[i])).isBetween(
                new Date(rowData.startDate),
                new Date(rowData.endDate),
                null,
                '[]'
              ) ||
              moment(new Date(this.state.vehicle[x].endDate[i])).isBetween(
                new Date(rowData.endDate),
                new Date(rowData.startDate),

                null,
                '[]'
              ) ||
              moment(new Date(this.state.vehicle[x].startDate[i])).isBetween(
                new Date(rowData.endDate),
                new Date(rowData.startDate),

                null,
                '[]'
              )
            ) {
              alert('Cannot use a vehicle which is already being used');
              return;
            }
          }
        }
      }
      try {
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        };
        let approveState = this.state;
        //console.log('hereitgoes');

        //console.log(rowData.order);
        approveState.orderId = rowData.orderId;
        approveState.persid = rowData.persid;
        approveState.order = rowData.order;
        approveState.hub = rowData.hub;
        approveState.status = 'Processing (pending approval)';
        approveState.unit = rowData.unit;
        //console.log('approveState: ' + JSON.stringify(approveState.order));
        //console.log(approveState.order.resources[rowData.resourceNum]);
        approveState.order.resources[rowData.resourceNum].vehicleApprove[ //orderNum and resourceNum are not orderId and resourceId they are the index of the order and resource which need to be changed
          rowData.approveNum
        ] = true; //This rather large line of code choose the correct approve to change from false to true
        approveState.indentDriver = true; //This indent driver tell the post function that we actually want to replace the order's portion whith approveState since we are not sending only the updated order and rather the orders

        //Code below: It should be able to assign the driver to approveState. We also need to remove the driver from the list. The select method should also show the correct driver. It could also change it completely to like a text or something. Cons: they can't change the driver.

        approveState.order.resources[rowData.resourceNum].vehicleAssigned[
          rowData.approveNum
        ] = rowData.vehicles; //Assigning the rowData driver

        //Need to also remove the driver from the list
        //One way of doing this is to post on the driver database that he is assigned to the orderId and if orderId is not undefined it is not added to the list and therefore cannot be selected. We could also add in the resource ID so we know exactly which is being talked about.
        //In order itself we need to assign the driver too therefore do that first

        //console.log('here I am ');
        //console.log(rowData.drivers);
        let res1 = axios.patch(
          '/api/vehicle',
          {
            params: {
              vehicleAssigned: rowData.vehicles,
              orderId: rowData.orderId,
              resourceId: rowData.resourceId,
              startDate: rowData.startDate,
              endDate: rowData.endDate,
            },
          },
          config
        );

        let res = axios.post('/api/order', approveState, config);
        // consoleconsoleconsole.log(JSON.stringify(approveState));
        //console.log('posted');
        this.setState({ driverApprove: true });
        this.getIndents(this.state.data);
        this.getDrivers();
      } catch (err) {
        //console.log(err);
      }
    }
    //console.log(rowData.drivers);
  };

  removeVehicle = async (rowData) => {
    if (rowData.vehicle == 0) {
      console.log(rowData);
    } else {
      rowData.approveVehicle = false;
      //I need to check if the driver is indented with the same time
      console.log('remove Data' + JSON.stringify(rowData));
      try {
        let approveState = this.state;
        console.log(approveState);
        if (
          approveState.order[rowData.orderNum].resources[rowData.resourceNum]
            .vehicleApprove[rowData.approveNum] == false
        ) {
          console.log(approveState);
          return;
        }
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        };
        if (rowData.vehicles == undefined)
          rowData.vehicles = this.state.vehicle[0].vehiclenum;
        console.log('hereitgoes');
        console.log(rowData);
        let resRemove = await axios.patch(
          '/api/vehicle/erase',
          {
            params: {
              vehicleAssigned: rowData.vehicleAssigned,
              orderId: rowData.orderId,
              resourceId: rowData.resourceId,
              startDate: rowData.startDate,
              endDate: rowData.endDate,
            },
          },
          config
        );

        //console.log(rowData.order);
        approveState.orderId = rowData.orderId;
        approveState.persid = rowData.persid;
        approveState.order = rowData.order; //This is what sets the order to approveState.order rather than having to using orderNum
        approveState.hub = rowData.hub;
        approveState.status = 'Processing (pending approval)';
        approveState.unit = rowData.unit;
        //console.log('approveState: ' + JSON.stringify(approveState.order));
        //console.log(approveState.order.resources[rowData.resourceNum]);
        approveState.order.resources[rowData.resourceNum].vehicleApprove[ //orderNum and resourceNum are not orderId and resourceId they are the index of the order and resource which need to be changed
          rowData.approveNum
        ] = false; //This rather large line of code choose the correct approve to change back from true to false
        approveState.indentDriver = true; //This indent driver tell the post function that we actually want to replace the order's portion whith approveState since we are not sending only the updated order and rather the orders

        //Code below: It should be able to assign the driver to approveState. We also need to remove the driver from the list. The select method should also show the correct driver. It could also change it completely to like a text or something. Cons: they can't change the driver.

        approveState.order.resources[rowData.resourceNum].vehicleAssigned[
          rowData.approveNum
        ] = ''; //Assigning the rowData driver

        let res = await axios.post('/api/order', approveState, config);
        // consoleconsoleconsole.log(JSON.stringify(approveState));
        //console.log('posted');
        this.setState({ driverApprove: false });
        this.getIndents(this.state.data);
        this.getDrivers();
      } catch (err) {
        //console.log(err);
      }
    }
    //console.log(rowData.drivers);
  };
  //Change when a driver has been selected
  onDriverSelected = (e, eiD, rowData) => {
    rowData['drivers'] = e;
    console.log(rowData);
  };

  //Creating the select tags REACT STYLE
  createDriverSelect = (rowData) => {
    let drivers = [];
    for (let i = 0; i < this.state.driver.length; i++) {
      if (this.state.driver[i].unit.localeCompare(rowData.unit) == 0) {
        //Checking to see if the driver is from that unit
        if (this.state.driver[i].skillsets.includes(rowData.vehicle[0])) {
          if (rowData['drivers'] == undefined) {
            rowData['drivers'] = this.state.driver[i].name;
          }
          drivers.push(
            <option key={i} value={this.state.driver[i].name}>
              {this.state.driver[i].name}
            </option>
          );
        }
      }
    }
    //console.log(drivers);
    return drivers;
  };

  //Change when a vehicle has been selected
  onVehicleSelected = (e, eiD, rowData) => {
    rowData['vehicles'] = e;
    console.log(rowData);
  };

  //Creating the select tags REACT STYLE
  createVehicleSelect = (rowData) => {
    let vehicles = [];
    let startIndex = 0;
    for (let a = 0; a < this.state.vehicle.length; a++) {
      if (this.state.vehicle[a].unit.localeCompare(rowData.unit) == 0) {
        if (this.state.vehicle[a].name.localeCompare(rowData.vehicle[0]) == 0) {
          if (rowData['vehicles'] == undefined) {
            rowData['vehicles'] = this.state.vehicle[a].vehiclenum;
          }
          vehicles.push(
            <option key={'x' + a} value={this.state.vehicle[a].vehiclenum}>
              {this.state.vehicle[a].vehiclenum}
            </option>
          );
        }
      }
    }
    //console.log(drivers);
    return vehicles;
  };

  //Creating the main table
  createTable = () => {
    //Done updating orderAll
    this.table = [];
    let table = []; //I need to add another for loop which is this.state.orderAlllength
    //console.log('helloooo');
    for (let x = 0; x < this.state.orderAll.length; x++) {
      let orderOne = this.state.orderAll[x]; //this is actually one order (One PERSON which contains all its orders)
      let persid = orderOne.persid;
      //console.log(orderOne);
      for (let i = 0; i < orderOne.orders.length; i++) {
        //^ This will change to  this.state.orderAll[x].orders.length
        let order = orderOne.orders[i];
        //console.log(order);
        //if(this.state.order[i].status == undefined){
        //console.log(orderOne.hub); //All of the following will change as well.
        if (new Date(order.startDate) > new Date())
          this.table.push({
            //I need to push in persid for the order because I need to send it in approveIndent as it requires the order's persid and not just the itms person's persid
            persid: persid,
            orderId: order._id,
            poc: order.poc,
            location: order.location,
            reason: order.reason,
            admin: order.indentApprover,
            extraInfo: order.extraInfo,
            resources: order.resources, //I need this for approveIndent
            orderOne: orderOne, //I am going to try and use this for create Detail Panel
            activity: order.activity,
            orderNum: i,
            types: order.types,
            hub: orderOne.hub,
            unit: orderOne.unit,
            order: order, //this is for Approve state as a preacautionary method so that it doesn't messup all the data of the indentwhen it updates. it could be order instead of orderAll.orders[i] but I doubt it.
            startDate: new Date(order.startDate).toLocaleString('en-GB', {
              timeZone: 'Asia/Singapore',
            }),
            endDate: new Date(order.endDate).toLocaleString('en-GB', {
              timeZone: 'Asia/Singapore',
            }),
            processing: order.processing,
          });
        //}
      }
    }
    console.log(this.table);
    return this.table;
  };

  //Creating the main table past indent
  createTable1 = () => {
    //Done updating orderAll
    let table = [];
    for (let x = 0; x < this.state.orderAll.length; x++) {
      let orderOne = this.state.orderAll[x]; //this is one order
      let persid = orderOne.persid;
      for (let i = 0; i < orderOne.orders.length; i++) {
        //if(this.state.order[i].status == undefined){
        let order = orderOne.orders[i];
        //console.log('order' + JSON.stringify(this.state.orderAll[x]));
        if (new Date(order.startDate) < new Date()) {
          // Need to update this part
          //console.log('hello');
          if (order.processing.localeCompare('Approved') == 0)
            order.processing = 'Fulfilled';
          if (
            order.processing.localeCompare('Rejected') == 0 ||
            order.processing.localeCompare('Processing (pending approval)') == 0
          )
            order.processing = 'Unfulfilled';
          table.push({
            persid: persid,
            hub: orderOne.hub,
            unit: orderOne.unit,
            orderId: order._id,
            poc: order.poc,
            location: order.location,
            activity: order.activity,
            admin: order.indentApprover,
            reason: order.reason,
            extraInfo: order.extraInfo,
            orderNum: i,
            orderOne: orderOne, //I am going to try and use this for create Detail Panel
            types: order.types,
            order: order,
            startDate: new Date(order.startDate).toLocaleString('en-GB', {
              timeZone: 'Asia/Singapore',
            }),
            endDate: new Date(order.endDate).toLocaleString('en-GB', {
              timeZone: 'Asia/Singapore',
            }),
            processing: order.processing,
          });
          for (let x = 0; x < this.state.driver.length; x++) {
            //console.log(JSON.stringify(this.state.driver[x].orderId));
            //console.log(JSON.stringify(order._id));gjhgjhgjhgjhgjhgjhgjhg
            for (let i = 0; i < this.state.driver[x].orderId.length; i++) {
              if (this.state.driver[x].orderId[i] != null) {
                if (
                  this.state.driver[x].orderId[i].localeCompare(order._id) == 0
                ) {
                  const config = {
                    headers: {
                      'Content-Type': 'application/json',
                    },
                  };
                  let res1 = axios.patch(
                    //This is to erase the orderID if it is in the past
                    '/api/driver/erase',
                    {
                      params: {
                        driverAssigned: this.state.driver[x].name,
                        orderId: this.state.driver[x].orderId[i],
                        resourceId: this.state.driver[x].resourceId[i],
                        startDate: this.state.driver[x].startDate[i],
                        endDate: this.state.driver[x].endDate[i],
                      },
                    },
                    config
                  );
                }
              }
            }
          }
          for (let x = 0; x < this.state.vehicle.length; x++) {
            //console.log(JSON.stringify(this.state.driver[x].orderId));
            //console.log(JSON.stringify(order._id));
            for (let i = 0; i < this.state.vehicle[x].orderId.length; i++) {
              if (
                this.state.vehicle[x].orderId[i].localeCompare(order._id) == 0
              ) {
                const config = {
                  headers: {
                    'Content-Type': 'application/json',
                  },
                };
                console.log(this.state.vehicle[x].orderId[i]);
                console.log(this.state.vehicle[x].vehiclenum);
                let res1 = axios.patch(
                  //This is to erase the orderID if it is in the past
                  '/api/vehicle/erase',
                  {
                    params: {
                      vehicleAssigned: this.state.vehicle[x].vehiclenum,
                      orderId: this.state.vehicle[x].orderId[i],
                      resourceId: this.state.vehicle[x].resourceId[i],
                      startDate: this.state.vehicle[x].startDate[i],
                      endDate: this.state.vehicle[x].endDate[i],
                    },
                  },
                  config
                );
              }
            }
          }
        }
      }

      //}
    }
    //console.log(table);
    return table;
  };
  //Creating the Detail Panel which has all the secondary information from resources
  createDetailPanel = (rowData) => {
    //I think I am done updating all of this.
    console.log(rowData);
    this.detailPanel = [];
    let table = []; //There will be another for loop just like the one added in createTable and createTable1()
    //console.log('hellloooooo');
    let orderOne = rowData.orderOne; //this is one order
    let persid = orderOne.persid;

    //All of the following will have to change as well
    //console.log(orderOne);
    let order = rowData.order;
    // console.log('order is ' + order);
    for (let x = 0; order.resources.length > x; x++) {
      let driver = 0;
      let quantity = 0;
      let quantityDiff = 0;
      //console.log(order.resources[x].driver[0]);
      if (order.resources[x].driver[0] === true) {
        driver = order.resources[x].quantity;
      } else if (typeof order.resources[x].driver[0] === 'string') {
        //console.log('here I AMAMMAMA');
        driver = parseInt(order.resources[x].driver[0]);
        console.log(order.resources[x]);
        quantity = parseInt(order.resources[x].quantity[0]); //Vehicle Quantity
        quantityDiff = quantity - driver; //The assumption being made here is that quantity of vehicles is always greater than driver
        console.log(quantityDiff);
        //console.log(driver);
        if (driver > 1) {
          for (let b = 0; b < driver; b++) {
            this.detailPanel.push({
              persid: persid,
              hub: orderOne.hub,
              unit: orderOne.unit,
              vehicle: order.resources[x].vehicle, //I highly doubt we will need to add rowData in this but in the event that we need to. It shouldn't be that difficult.
              quantity: 1,
              driver: 1,
              driverAssignedArray: order.resources[x].driverAssigned, //We need to push the driverAssigned which exists in order becuase we need to prevent duplicates from forming. I doubt I am using this.
              vehicleAssigned: order.resources[x].vehicleAssigned[b],
              approveVehicle: order.resources[x].vehicleApprove[b],
              order: order,
              approve: order.resources[x].approve[b], //Need to have the array of approve
              approveNum: b, //The index of approve because I need to know which approve to change
              resourceNum: x, //The index of resources because I need to know which approve to change,
              orderNum: rowData.orderNum, //The orderNum is necessary because we need to know which order is going to be changed. We cannot grab from orderId
              driverSum: order.resources[x].driverSum, //Adding the driverSum to every table because I am not sure where I am going to have to use it.
              driverAssigned: order.resources[x].driverAssigned[b], //Need to know which driver has been assigned
              orderId: order._id, // I need the orderId for approving the indent.,
              resourceId: order.resources[x]._id,
              startDate: order.startDate,
              endDate: order.endDate,
            });
          }
          for (let b = 0; b < quantityDiff; b++) {
            this.detailPanel.push({
              persid: persid,
              hub: orderOne.hub,
              unit: orderOne.unit,
              vehicle: order.resources[x].vehicle, //I highly doubt we will need to add rowData in this but in the event that we need to. It shouldn't be that difficult.
              quantity: 1,
              driver: 0, //zero drivers as its unnecessary
              vehicleAssigned: order.resources[x].vehicleAssigned[driver + b], //In this case we start from driver length because we will have covered all before it above
              approveVehicle: order.resources[x].vehicleApprove[driver + b],
              order: order,
              approve: order.resources[x].approve[b], //Need to have the array of approve
              approveNum: b, //The index of approve because I need to know which approve to change
              resourceNum: x, //The index of resources because I need to know which approve to change,
              orderNum: rowData.orderNum, //The orderNum is necessary because we need to know which order is going to be changed. We cannot grab from orderId
              driverSum: order.resources[x].driverSum, //Adding the driverSum to every table because I am not sure where I am going to have to use it.
              orderId: order._id, // I need the orderId for approving the indent.,
              resourceId: order.resources[x]._id,
              startDate: order.startDate,
              endDate: order.endDate,
            });
          }
        }
      }

      if (driver <= 1) {
        //console.log(orderOne);
        this.detailPanel.push({
          persid: persid,
          hub: orderOne.hub,
          unit: orderOne.unit,
          vehicle: order.resources[x].vehicle,
          quantity: 1,
          driver: driver,
          order: order,
          vehicleAssigned: order.resources[x].vehicleAssigned[0],
          approveVehicle: order.resources[x].vehicleApprove[0],
          driverAssignedArray: order.resources[x].driverAssigned, //We need to push the driverAssigned which exists in order becuase we need to prevent duplicates from forming.
          approveNum: 0, //The index of approve is zero because its an array of length one,
          resourceNum: x, //x unlike approveNum can still change
          orderNum: rowData.orderNum, //The orderNum is necessary because we need to know which order is going to be changed. We cannot grab from orderId
          approve: order.resources[x].approve[0], //Need to have the array of approves which can also actually be displayed and dynamically changed
          driverSum: order.resources[x].driverSum, //The sum of drivers
          driverAssigned: order.resources[x].driverAssigned[0], //Need to know which driver has been assigned
          orderId: order._id,
          resourceId: order.resources[x]._id,
          startDate: order.startDate,
          endDate: order.endDate,
        });
        if (driver != 0) quantityDiff++;
        for (let b = 1; b < quantityDiff; b++) {
          this.detailPanel.push({
            persid: persid,
            hub: orderOne.hub,
            unit: orderOne.unit,
            vehicle: order.resources[x].vehicle, //I highly doubt we will need to add rowData in this but in the event that we need to. It shouldn't be that difficult.
            approveVehicle: order.resources[x].vehicleApprove[b],
            quantity: 1,
            driver: 0,
            vehicleAssigned: order.resources[x].vehicleAssigned[b], //In this case we only assign one vehicle above so we start from index one
            order: order,
            approve: order.resources[x].approve[b + 1], //Need to have the array of approve
            approveNum: b, //The index of approve because I need to know which approve to change
            resourceNum: x, //The index of resources because I need to know which approve to change,
            orderNum: rowData.orderNum, //The orderNum is necessary because we need to know which order is going to be changed. We cannot grab from orderId
            driverSum: order.resources[x].driverSum, //Adding the driverSum to every table because I am not sure where I am going to have to use it.
            orderId: order._id, // I need the orderId for approving the indent.,
            resourceId: order.resources[x]._id,
            startDate: order.startDate,
            endDate: order.endDate,
          });
        }
      }
    }

    //console.log(rowData);
    return this.detailPanel;
  };

  //approve indent
  approveIndent = (rowData, orderId) => {
    try {
      //Need to check wether drivers have been assigned
      let driverApproveBool = false;
      let order = rowData.order; //getting the order for which resources will change
      console.log(order);
      for (let x = 0; x < order.resources.length; x++) {
        console.log('order Line 829' + JSON.stringify(order));
        for (let i = 0; i < order.resources[x].driverAssigned.length; i++) {
          if (order.resources[x].driver[i] == 0) {
          } else {
            if (order.resources[x].driverAssigned[i] == '') {
              alert('Cannot approve indent before drivers have been assigned');
              driverApproveBool = true;
              return;
            }
          }
        }
        console.log(order.resources[x]);
        if (order.resources[x].vehicleAssigned.includes('')) {
          alert('Cannot approve indent before vehicles have been assigned');
          driverApproveBool = true;
          return;
        }
        /* if (
          order.processing.localeCompare('Approved') == 0 ||
          order.processing.localeCompare('Rejected') == 0
        )
          driverApproveBool = true; */
      }
      if (driverApproveBool == false) {
        let approveState = this.state;
        approveState.orderId = orderId;
        approveState.persid = rowData.persid;
        approveState.hub = rowData.hub;
        approveState.unit = rowData.unit;
        approveState.order = rowData.order;
        approveState.status = true;
        approveState.indentApprover = this.state.persid;
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        };
        this.props.setAlert('Invalid Login Credentials', 'success');
        axios.post('/api/order', approveState, config);
        this.setState({ approve: true });
        this.getIndents(this.state.data);
      }
    } catch (err) {}
  };

  approveVehicleModal = (orderId) => {
    const onChangeVehicle = (event) => {
      let prevState = this.state;
      this.setState(() => ({
        update: event,
      }));
    };
    if (typeof this.state.detailPanelData !== 'string') {
      console.log(this.state.detailPanelData);
      let rowData = this.createDetailPanel(this.state.detailPanelData);
      return (
        <div>
          <Dialog
            open={this.state.showVehicle}
            onClose={this.handleCloseVehicle}
            aria-labelledby='form-dialog-title'
          >
            <DialogTitle id='form-dialog-title'>Vehicle</DialogTitle>
            <form onSubmit={this.approveVehicle}>
              <DialogContent>
                {rowData.map((item, index) => (
                  <div>
                    Vehicle needed: {item.vehicle} | Vehicle Assigned:{' '}
                    {item.vehicleAssigned}
                    <Form.Control
                      defaultValue={this.state.driver[0].name}
                      onChange={(e) =>
                        this.onVehicleSelected(e.target.value, e.target, item)
                      }
                      as='select'
                    >
                      {this.createVehicleSelect(item)}
                    </Form.Control>
                    <Button
                      value={item}
                      onClick={() => this.approveVehicle(item)}
                      color='primary'
                    >
                      Approve Vehicle
                    </Button>{' '}
                    <Button
                      value={item}
                      onClick={() => this.removeVehicle(item)}
                      variant='danger'
                    >
                      Remove Vehicle
                    </Button>
                    <hr />
                  </div>
                ))}
                <DialogContentText></DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleCloseVehicle} color='primary'>
                  Close
                </Button>
              </DialogActions>
            </form>
          </Dialog>
        </div>
      );
    } else {
      return <div></div>;
    }
  };

  approveDriverModal = (orderId) => {
    const onChangeDriver = (event) => {
      let prevState = this.state;
      this.setState(() => ({
        update: event,
      }));
    };
    if (typeof this.state.detailPanelData !== 'string') {
      console.log(this.state.detailPanelData);
      let rowData = this.createDetailPanel(this.state.detailPanelData);
      return (
        <div>
          <Dialog
            open={this.state.showDriver}
            onClose={this.handleCloseDriver}
            aria-labelledby='form-dialog-title'
          >
            <DialogTitle id='form-dialog-title'>Driver</DialogTitle>
            <form onSubmit={this.approveDriver}>
              <DialogContent>
                {rowData.map((item, index) => (
                  <div>
                    Vehicle needed: {item.vehicle} | Driver Assigned:{' '}
                    {item.driverAssigned}
                    <Form.Control
                      defaultValue={this.state.driver[0].name}
                      onChange={(e) =>
                        this.onDriverSelected(e.target.value, e.target, item)
                      }
                      as='select'
                    >
                      {this.createDriverSelect(item)}
                    </Form.Control>
                    <Button
                      value={item}
                      onClick={() => this.approveDriver(item)}
                      color='primary'
                    >
                      Approve Driver
                    </Button>{' '}
                    <Button
                      value={item}
                      onClick={() => this.removeDriver(item)}
                      variant='danger'
                    >
                      Remove Driver
                    </Button>{' '}
                    <hr />
                  </div>
                ))}
                <DialogContentText></DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleCloseDriver} color='primary'>
                  Close
                </Button>
              </DialogActions>
            </form>
          </Dialog>
        </div>
      );
    } else {
      return <div></div>;
    }
  };

  rejectModal = (rowData, orderId) => {
    const onChangeReject = (event) => {
      let prevState = this.state;
      this.setState(() => ({
        update: event,
      }));
    };
    return (
      <div>
        <Dialog
          open={this.state.show}
          onClose={this.handleClose}
          aria-labelledby='form-dialog-title'
        >
          <DialogTitle id='form-dialog-title'>Reject Indent</DialogTitle>
          <form onSubmit={this.rejectIndent}>
            <DialogContent>
              <DialogContentText>
                Please provide a reason for rejecting the indent:
              </DialogContentText>

              <TextField
                autoFocus
                margin='dense'
                id='name'
                label='Reason'
                type='text'
                fullWidth
                name='title'
                inputRef={this.googleInput}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color='primary'>
                Cancel
              </Button>
              <Button onClick={this.rejectIndent} color='primary'>
                Reject
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  };

  handleClose = () => {
    this.setState({ show: false });
  };
  handleCloseDriver = () => {
    this.setState({ showDriver: false });
  };
  handleCloseVehicle = () => {
    this.setState({ showVehicle: false });
  };
  //reject indent
  rejectIndent = (rowData, orderId) => {
    //Done updating orderAll
    let driverApproveBool = false;
    console.log(this.googleInput.current.value);
    if (
      this.googleInput.current.value == undefined ||
      this.googleInput.current.value.localeCompare('') == 0
    ) {
      alert('Please provide a reason for rejecting the indent');
      return;
    }
    let order = this.state.rowData.order; //getting the order for which resources will change
    for (let x = 0; x < order.resources.length; x++) {
      console.log('order Line 829' + JSON.stringify(order));
      for (let i = 0; i < order.resources[x].driverAssigned.length; i++) {
        if (order.resources[x].driver[i] == 0) {
        } else {
          if (order.resources[x].driverAssigned[i].localeCompare('') != 0) {
            alert('Cannot reject indent before drivers have been removed');
            driverApproveBool = true;
            return;
          }
        }
      }
      console.log();
      for (let i = 0; i < order.resources[x].vehicleAssigned.length; i++) {
        if (order.resources[x].vehicleAssigned[i].localeCompare('') != 0) {
          alert('Cannot reject indent before vehicles have been removed');
          driverApproveBool = true;
          return;
        }
      }
      /* if (
        order.processing.localeCompare('Approved') == 0 ||
        order.processing.localeCompare('Rejected') == 0
      )
        driverApproveBool = true; */
    }
    if (driverApproveBool == false) {
      try {
        let approveState = this.state;
        console.log(approveState);
        approveState.orderId = this.state.rowData.orderId;
        approveState.persid = this.state.rowData.persid;
        approveState.hub = this.state.rowData.hub;
        approveState.unit = this.state.rowData.unit;
        approveState.order = this.state.rowData.order;
        approveState.status = false;
        approveState.reason = this.googleInput.current.value;
        approveState.indentApprover = this.state.persid;
        const config = {
          headers: {
            'Content-Type': 'application/json',
          },
        };
        console.log(approveState);
        axios.post('/api/order', approveState, config);
        this.setState({ approve: false });
        this.getIndents(this.state.data);
      } catch (err) {}
    }
  };
}
const mapStateToProps = (state) => {
  console.log(state);
  return state;
};
export default connect(mapStateToProps, { setAlert })(indent);
