import React from 'react';
import axios from 'axios';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import moment from 'moment';
import {
  Col,
  Row,
  Form,
  Accordion,
  Collapse,
  Container,
} from 'react-bootstrap';
import { TopNav } from '../topnav/topnav';
/* import { PlusSquare } from 'react-bootstrap-icons';
 */ const { compose, withProps, lifecycle } = require('recompose');
const { withScriptjs } = require('react-google-maps');
const {
  StandaloneSearchBox,
} = require('react-google-maps/lib/components/places/StandaloneSearchBox');

let obj = {
  location: '',
  reason: '',
  destination: '',
  vehicle: '5TON',
  quantity: 0,
  reason: '',
  submitted: false,
};
const PlacesWithStandaloneSearchBox = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyDga0vz02MHkeyK2QbMbHdMCRY9px56Is4&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
  }),
  lifecycle({
    componentWillMount() {
      const refs = {};
      this.setState({
        places: [],

        onSearchBoxMounted: (ref) => {
          refs.searchBox = ref;
          const bounds = new window.google.maps.LatLng(1.0, 131.0);
          console.log(bounds);
          this.setState({
            bounds: bounds,
          });
        },
        onPlacesChanged: () => {
          const places = refs.searchBox.getPlaces();
          console.log(refs.searchBox.props.name);
          let name = refs.searchBox.props.name;
          console.log(refs);
          let address = places[0].formatted_address;
          console.log(places[0].formatted_address);
          this.setState({
            places,
            [name]: address,
          });
          obj[name] = address;
          console.log(this.state);
        },
        onChange: (e) => {
          console.log('hello');
          let prevState = this.state;
          prevState[e.target.name] = e.target.value;
          this.setState({ prevState });
        },
      });
    },
  }),

  withScriptjs
)((props) => (
  <div data-standalone-searchbox=''>
    <StandaloneSearchBox
      ref={props.onSearchBoxMounted}
      bounds={
        new window.google.maps.LatLngBounds(
          new window.google.maps.LatLng(1.288015, 103.776704),
          new window.google.maps.LatLng(1.415075, 103.910044)
        )
      }
      defaultBounds={
        new window.google.maps.LatLngBounds(
          new window.google.maps.LatLng(1.288015, 103.776704),
          new window.google.maps.LatLng(1.415075, 103.910044)
        )
      }
      onPlacesChanged={props.onPlacesChanged}
      name={props.name}
      onChange={(e) => {
        this.onChange(e);
      }}
    >
      <input
        type='text'
        placeholder={props.name}
        style={{
          border: `1px solid transparent`,
          width: `240px`,
          borderRadius: `3px`,
          boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
          fontSize: `14px`,
          outline: `none`,
          textOverflow: `ellipses`,
        }}
      />
    </StandaloneSearchBox>
    <ol>
      {props.places.map(
        ({ place_id, formatted_address, geometry: { location } }) => (
          <li key={place_id}>
            {formatted_address}
            {' at '}({location.lat()}, {location.lng()})
          </li>
        )
      )}
    </ol>
  </div>
));

class driver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      driver: '',
      vehicle: '',
      adhoc: '',
      arrAdhoc: [],
      persid: '',
      name: '',
      submitted: 'false',
      typeName: 'breakfast',
      drivers: [{ name: '' }],
      remarks: '',
      resources: [
        {
          date: moment(),
          rationId: 0,
          typeName: '',
          remarks: '',
          arr: [{ driver: 0, vehicle: '' }],
        },
      ],
      arrOpen: [true],
      driverSelected: '',
    };
    let MapWithADirectionsRenderer = '';
    let distance = '';
    this.getDistance = this.getDistance.bind(this);
  }
  componentDidMount() {
    this.getPersid();
    this.getSharedResource();
  }
  onChange(e) {
    let name = e.target.name;
    let value = e.target.value;

    let prevState = this.state;
    prevState[name] = value;
    obj[name] = value;
    this.setState({
      destination: obj.destination,
      location: obj.location,
      prevState,
    });
  }
  handleChange(e) {
    let state = this.state;
    state[e.target.name] = e.target.value;
    this.setState({ state });
  }
  async onSubmit(e, sharedResource) {
    try {
      e.preventDefault();
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };
      let arrKeys = Object.keys(obj);
      arrKeys.forEach((element) => {
        if (element.localeCompare('') === 0) {
          alert('Please fill in all the values');
          return;
        }
      });
      let body = {
        location: obj.location,
        destination: obj.destination,
        vehicle: obj.vehicle,
        quantity: obj.quantity,
        reason: obj.reason,
        sharedResource: sharedResource,
      };
      if (obj['submitted'] === false) {
        obj['submitted'] = true;
        await axios.post('/api/adhoc', body, config);

        this.setState(() => ({
          location: '',
          destination: '',
          quantity: '',
          vehicle: '5TON',
          reason: '',
          submitted: true,
          sharedResource: sharedResource,
        }));
        window.location.reload(false);

        console.log(this.state.submitted);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async getPersid() {
    let data = localStorage.getItem('token');
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth//user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    console.log(res.data);
    let persid = res.data.user['persid'];
    console.log(persid);
    this.setState({
      persid: persid,
      unit: res.data.user['unit'],
      hub: res.data.user['hub'],
      name: res.data.user['name'],
      authorized: res.data.user.authorized,
    });
    this.getDrivers();
  }
  async getDrivers() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get('/api/driver/driver', config);
    //console.log(res);
    for (let x = 0; x < res.data.drivers.length; x++) {
      if (res.data.drivers[x].persid == this.state.persid) {
        this.setState({ driver: res.data.drivers[x] });
      }
    }
    this.setState({
      drivers: res.data.drivers,
      driverSelected: res.data.drivers[0].name,
    });
    this.getIndents();
  }
  async getIndents() {
    console.log('hello');
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    let res = await axios.get('/api/adhoc', config);
    console.log(res);
    let bool = false;
    /*  if (
      this.state.driver.persid == this.state.persid &&
      this.state.driver.status != undefined
    ) {
      if (this.state.driver.status.localeCompare('Accept') === 0) {
        for (let i = 0; i < res.data.adhoc.length; i++) {
          if (
            res.data.adhoc[i]._id.localeCompare(this.state.driver.adhocId) === 0
          ) {
            this.setState({ adhoc: res.data.adhoc[i] });
            bool = true;
          }
        }
      }
    } */
    if (bool === false) this.setState({ adhoc: res.data.adhoc });
    console.log(this.state);
    this.getDistance();
  }

  async getDistance() {
    let res = '';
    console.log(this.state);

    /*    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    }; */
    let arrAdhoc = [];
    let state = this.state;

    //async function success(pos) {
    //var crd = pos.coords;
    let len = state.adhoc.length;
    if (len === undefined) state.adhoc = [state.adhoc];
    for (let x = 0; x < state.adhoc.length; x++) {
      //crd = pos.coords;
      let location = state.adhoc[x].location;
      let destination = state.adhoc[x].destination;
      try {
        console.log("i'm tracking you!");
        //let driverLat = crd.latitude;
        //let driverLong = crd.longitude;
        const config = {
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
          params: {
            //location: location,
            //destination: destination,
            //latitude: driverLat,
            //longitude: driverLong,
          },
        };
        let res = await axios.get('/api/adhoc', config);
        let distance = res.data.distance;
        let time = 100;
        if (time < 3000) {
          state.adhoc[x]['time'] = res.data.time;
          if (state.adhoc[x].status == undefined) arrAdhoc.push(state.adhoc[x]);
          else {
            if (
              state.adhoc[x].status.localeCompare('Accept') === 0 &&
              state.adhoc[x].persid == state.persid
            ) {
              arrAdhoc.push(state.adhoc[x]);
            }
          }
        }
      } catch (err) {
        console.log(err);
      }
    }

    this.setState({ arrAdhoc });
    //}

    /*  function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    } */

    /*  navigator.geolocation.getCurrentPosition(
      (success = success.bind(this)), //This allows me to access setState inside success which would otherwise be undefined
      error,
      options
    ); */
    console.log(res);

    console.log(res);
  }
  onSubmit(e, status) {
    let id = e.target.value;
    let driver = this.state.driverSelected;
    console.log('here is the driver' + JSON.toString(driver));
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
      params: {
        persid: this.state.persid,
        driver: driver,
        adhocId: id,
        status: status,
      }, //I will need to create a new patch function which filters by persid instead
    };
    console.log('Accepted Adhoc indent');
    //I have to post to the adhoc indent and the driver the order id
    let name = this.state.name;
    console.log('Cheking if name works: ' + driver);
    axios.patch('/api/driver/persid', config);
    const configAdhoc = {
      headers: {
        'Content-Type': 'application/json',
      },
      params: {
        id: id,
        status: status,
        name: driver,
        persid: this.state.persid,
      }, //I will need to create a new patch function which filters by persid instead
    };
    //I also need to make an update to the  adhoc indent
    axios.patch('/api/adhoc', configAdhoc);

    this.setState({ submitted: true });

    this.getDrivers();
  }

  async getSharedResource() {
    let data = localStorage.getItem('token');
    data = JSON.parse(data);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth',
      {
        params: {
          id: data,
        },
      },
      config
    );
    let allSharedResource = [];
    let resources = [];
    for (let i = 0; i < res.data.orders[0].orders.length; i++) {
      if (res.data.orders[0].orders[i].check === true) {
        if (
          moment(new Date()).isBefore(
            new Date(res.data.orders[0].orders[i].endDate),
            null,
            '[]'
          )
        ) {
          allSharedResource.push(res.data.orders[0].orders[i]);
        } else {
        }
      }
    }

    let type = this.state.typeName.toLowerCase();
    let filterd = [];
    let state = [];
    for (let i = 0; i < allSharedResource.length; i++) {
      //If I am doing anything with checking wether endDate has passed, I have to do it here cuz rationId depends on i, so I cannot change length. I could try changing i to rationId. Probably should lol.
      let rationRun = allSharedResource[i].rationRun[0];
      if (rationRun !== undefined)
        for (let i = 0; i < rationRun.resources.length; i++) {
          resources.push(rationRun.resources[i]);
        }
    }
    this.setState({
      persid: res.data.orders[0].persid,
      order: res.data.orders[0].orders,
      resources: resources,
    });
    let arrOpen = new Array(20);
    arrOpen.fill(true, 0, 20);
    this.setState({ arrOpen: arrOpen });
  }
  handleChangeName = (e, date, i, typeName) => {
    e.preventDefault();

    let resources = [...this.state.resources];
    for (let x = 0; x < resources.length; x++) {
      console.log(resources[x].date);
      if (resources[x].date.date() === date.date()) {
        if (resources[x].rationId === i && resources[x].typeName === typeName) {
          console.log(resources[x].arr);
          resources[x]['remarks'] = e.target.value;
        }
      }

      this.setState({ resources });
      console.log(this.state.resources);
    }
  };
  changeOpen = (x) => {
    let arrOpen = [...this.state.arrOpen];
    arrOpen[x] = !arrOpen[x];
    console.log(arrOpen[x]);
    this.setState({ arrOpen });
  };
  onSubmitButton(e, date, i, order, typeName) {
    let arr = [];
    let rationRun = this.state.resources;
    for (let x = 0; x < rationRun.length; x++) {
      if (
        rationRun[x].date.date() === date.date() &&
        rationRun[x].rationId === i &&
        rationRun[x].typeName === typeName
      )
        arr.push(rationRun[x]);
    }
    console.log(order);
    for (let x = 0; x < this.state.order.length; x++) {
      if (this.state.order[x].rationRun[0] != undefined)
        if (this.state.order[x].rationRun[0]._id === i) {
          order = this.state.order[x];
        }
    }
    console.log(order);
    order.rationRun[0].resources = arr;
    order.orderId = order._id;
    console.log(arr);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let body = {};
    body.orderId = order._id;
    body.persid = this.state.persid;
    body.order = [order];
    console.log(body);
    let getSharedResource = this.getSharedResource.bind(this);
    axios.post('/api/order', body, config).then(function (res) {
      getSharedResource();
    });
  } /* rationRun: []         resources: [
    { date: moment(), rationId: 0, arr: [{ driver: 0, vehicle: '' }] },
  ], */

  render() {
    let accepted = '';
    let filtered = [];
    let filteredStart = [];
    console.log(this.state.driver.status);
    let itemNotStarted = [];
    let itemStarted = [];
    let itemCompleted = [];
    let sharedResource = '';
    let bool = true; //Change to false after testing <---important

    if (this.state.order != undefined) {
      for (let i = 0; i < this.state.order.length; i++) {
        if (this.state.order[i].check === true) {
          if (
            moment(new Date()).isBetween(
              new Date(this.state.order[i].startDate),
              new Date(this.state.order[i].endDate),
              null,
              '[]'
            )
          ) {
            console.log('you may pass');
            bool = true;
            sharedResource = this.state.order[i].activity;
          } else {
            console.log('you are here early');
          }
        }
      }
    }
    if (bool == false) {
      return (
        <div>
          <h2>No Shared Resource is active right now</h2>
        </div>
      );
    }

    this.state.arrAdhoc.filter(function (item) {
      console.log(item.sharedResource);
      if (item.sharedResource === undefined) {
      } else if (item.sharedResource.localeCompare(sharedResource) === 0) {
        if (item.status === undefined) itemNotStarted.push(item);
        else if (item.status.localeCompare('Accept') == 0)
          itemStarted.push(item);
        else if (item.status.localeCompare('Completed') === 0)
          itemCompleted.push(item);
      }
    });
    let rationRun = {};
    let allSharedResource = [];
    let resources1 = [];
    let fullOrder = {};
    let cardArr = [];
    let resources = this.state.resources;
    console.log(this.state.resources);
    let bigAccordion = [];
    if (this.state.order != undefined) {
      for (let i = 0; i < this.state.order.length; i++) {
        if (this.state.order[i].check === true) {
          if (
            moment(new Date()).isBefore(
              new Date(this.state.order[i].endDate),
              null,
              '[]'
            )
          ) {
            console.log('you may pass');
            bool = true;
            sharedResource = this.state.order[i].activity;
            rationRun = this.state.order[i].rationRun[0];
            fullOrder = this.state.order[i];
            allSharedResource.push(this.state.order[i]);
          } else {
            console.log('you are here early');
          }
        }
      }

      let type = this.state.typeName.toLowerCase();
      let filterd = [];
      let state = [];
      for (let i = 0; i < allSharedResource.length; i++) {
        //If I am doing anything with checking wether endDate has passed, I have to do it here cuz rationId depends on i, so I cannot change length. I could try changing i to rationId. Probably should lol.
        let rationRun = allSharedResource[i].rationRun[0];
        cardArr = [];
        if (rationRun !== undefined)
          for (let x = 0; x < rationRun[type] - 1; x++) {
            if (x == 0) {
            }

            console.log(x);
            let filterd = [];

            rationRun = allSharedResource[i].rationRun[0];
            fullOrder = allSharedResource[i];
            console.log(fullOrder);
            let xa = 0;
            if (
              type === 'breakfast' &&
              moment(fullOrder.startDate).hour() > 6
            ) {
              xa = x + 1;
            } else if (
              type === 'lunch' &&
              moment(fullOrder.startDate).hour() > 12
            ) {
              xa = x + 1;
            } else {
              xa = x;
            }
            let date = moment(new Date(fullOrder.startDate))
              .add(xa, 'days')
              .format('dddd, MMMM Do YYYY ');
            let open = true;
            let momentDate = moment(new Date(fullOrder.startDate)).add(
              xa,
              'days'
            );

            cardArr.push(
              <div style={{ margin: 10 }}>
                <Card>
                  <Card.Header
                    style={{
                      backgroundColor: 'grey',
                      color: 'white',
                      fontSize: 20,
                    }}
                  >
                    {date}{' '}
                    <Button
                      style={{ float: 'right' }}
                      onClick={() => this.changeOpen(x + i)}
                    >
                      {this.state.arrOpen[x + i] ? 'Close' : 'Open'}
                    </Button>{' '}
                    {open}
                  </Card.Header>
                  <Collapse in={this.state.arrOpen[x + i]}>
                    <Card.Body>
                      <Card.Title>
                        {this.state.typeName}, Pax: {rationRun.pax}
                      </Card.Title>
                      {
                        ((filterd = []),
                        (state = this.state),
                        console.log(resources),
                        resources.filter(function (item) {
                          item.date = moment(new Date(item.date));
                          console.log(state.resources);

                          if (
                            item.date.date() === momentDate.date() &&
                            item.date.month() === momentDate.month() &&
                            item.date.year() === momentDate.year() &&
                            item.rationId === rationRun._id &&
                            item.typeName === type
                          ) {
                            filterd.push(item);
                            console.log(filtered);
                          }
                        }))
                      }
                      {
                        (console.log(filterd[0]),
                        filterd[0] === undefined || filterd.length === 0
                          ? null
                          : filterd[0].arr.map((val, idx) => {
                              let resourceId = `resources-${idx}`,
                                vehicleId = `vehicle-${idx}`,
                                driverId = `driver-${idx}`,
                                quantityId = `quantity-${idx}`;
                              return (
                                <div key={idx}>
                                  <table>
                                    <thead>
                                      <tr
                                        style={{
                                          padding: 5,
                                          width: 50,
                                          border: '1px solid black',
                                        }}
                                      >
                                        <th
                                          style={{
                                            padding: 5,
                                            width: 100,

                                            border: '1px solid black',
                                          }}
                                        >
                                          Driver:
                                        </th>
                                        <th
                                          style={{
                                            padding: 5,
                                            width: 100,

                                            border: '1px solid black',
                                          }}
                                        >
                                          Vehicle:{' '}
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr
                                        style={{
                                          padding: 5,
                                          width: 50,

                                          border: '1px solid black',
                                        }}
                                      >
                                        <td
                                          style={{
                                            padding: 5,
                                            width: 100,
                                            border: '1px solid black',
                                          }}
                                        >
                                          {val.driver}
                                        </td>
                                        <td
                                          style={{
                                            padding: 5,
                                            width: 100,
                                            border: '1px solid black',
                                          }}
                                        >
                                          {val.vehicle}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <br></br>
                                </div>
                              );
                            }))
                      }{' '}
                      {filterd[0] === undefined
                        ? null
                        : filterd[0].remarks === undefined ||
                          filterd[0].remarks === ''
                        ? null
                        : 'Remarks: ' + filterd[0].remarks}
                      <hr />
                      <input
                        name='remarks'
                        onChange={(e) =>
                          this.handleChangeName(
                            e,
                            momentDate,
                            rationRun._id,
                            type
                          )
                        }
                      />
                      <br />
                      <button
                        onClick={(e) =>
                          this.onSubmitButton(
                            e,
                            momentDate,
                            rationRun._id,
                            fullOrder,
                            type
                          )
                        }
                      >
                        Submit Remarks
                      </button>{' '}
                    </Card.Body>
                  </Collapse>{' '}
                  <Collapse in={this.state.arrOpen[x + i]}>
                    <Card.Footer> </Card.Footer>
                  </Collapse>
                </Card>
              </div>
            );
          }
        bigAccordion.push(
          <div style={{ margin: 15 }}>
            <Accordion defaultActiveKey='0'>
              {' '}
              <Card>
                <Card.Header>
                  {' '}
                  <Accordion.Toggle as={Button} variant='link' eventKey={i}>
                    <Row>
                      <Col>{allSharedResource[i].activity} </Col>
                      <Col>{allSharedResource[i].location} </Col>
                    </Row>{' '}
                    <Row>
                      <Col>
                        {moment(
                          new Date(allSharedResource[i].startDate)
                        ).format('Do MMMM  YYYY, hh:mm:ss a')}{' '}
                      </Col>
                      <Col>
                        {moment(new Date(allSharedResource[i].endDate)).format(
                          'Do MMMM  YYYY, hh:mm:ss a'
                        )}{' '}
                      </Col>
                    </Row>
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey={i}>
                  <Card.Body>{cardArr}</Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </div>
        );
      }
    }

    return (
      <div
        style={{
          background: 'lightgrey',
          position: 'fixed',
          bottom: '0',
          right: '0',
          left: '0',
          top: '0',
          overflowY: 'auto',
        }}
      >
        <div className='overall'>
          <TopNav />

          <h1>Shared Resource</h1>

          <Container>
            <Form.Label style={{ display: 'inline' }}>
              Filter Ration Run:{' '}
            </Form.Label>
            <Form.Control
              as='select'
              style={{ display: 'inline', maxWidth: '80%' }}
              placeholder='Ration'
              name='typeName'
              value={this.state.typeName}
              multiple={false}
              className='vehicle'
              onChange={(e) => this.onChange(e)}
              required
            >
              <option>Breakfast</option>
              <option>Lunch</option>
              <option>Dinner</option>
            </Form.Control>
            {bigAccordion}
            <Form
              style={{
                border: '3px solid',
                borderRadius: 5,
                width: '100%',
                padding: '30px',
                marginTop: 10,
              }}
              onSubmit={(e) => this.onSubmit(e)}
            >
              <Form.Group
                className='input-field'
                controlId='exampleForm.ControlInput31'
              >
                {' '}
                <Form.Label>Start:</Form.Label>
                <PlacesWithStandaloneSearchBox name='location' required />
              </Form.Group>
              <Form.Group
                className='input-field'
                controlId='exampleForm.ControlInput321'
              >
                <Form.Label>Destiantion:</Form.Label>
                <PlacesWithStandaloneSearchBox name='destination' required />
              </Form.Group>
              <Form.Group>
                <Form.Label>Vehicles</Form.Label>
                <Form.Control
                  as='select'
                  placeholder='vehicles'
                  name='vehicle'
                  value={this.state.vehicle}
                  multiple={false}
                  className='vehicle'
                  onChange={(e) => this.onChange(e)}
                  required
                >
                  <option>5TON</option>
                  <option>6TON</option>
                  <option>OUV</option>
                </Form.Control>
              </Form.Group>{' '}
              <Form.Group
                className='input-field'
                controlId='exampleForm.ControlInput3'
              >
                <Form.Control
                  type='number'
                  name='quantity'
                  className='textField'
                  value={this.state.quantity}
                  onChange={(e) => this.onChange(e)}
                  required
                ></Form.Control>
                <Form.Label className='label'>Quantity: </Form.Label>
              </Form.Group>{' '}
              <Form.Group
                className='input-field'
                controlId='exampleForm.ControlInput323'
              >
                <Form.Control
                  type='text'
                  name='reason'
                  className='textField'
                  value={this.state.reason}
                  onChange={(e) => this.onChange(e)}
                  required
                ></Form.Control>
                <Form.Label className='label'>Reason: </Form.Label>
              </Form.Group>
              <Button
                variant='primary'
                type='submit'
                className='btn btn-primary btn-lg btn-block'
                value='Login'
                onClick={(e) => {
                  this.onSubmit(e, sharedResource);
                }}
                style={{ background: '#3d74db', borderStyle: 'none' }}
              >
                Submit
              </Button>
            </Form>{' '}
            <hr></hr>
            <h4>Adhoc indents yet to start</h4>
            {itemNotStarted.map((item, index) => (
              <div>
                {' '}
                <ol
                  key={item.location.toString()}
                  style={{
                    textAlign: 'center',
                    margin: 0,
                    color: 'black',
                    padding: 0,
                    marginBlockStart: 0,
                    paddingInlineStart: 0,
                    marginBlockEnd: 0,
                  }}
                >
                  Start Location: {item.location} | Destination:
                  {item.destination} | Reason: {item.reason} | Vehicle:{' '}
                  {item.vehicle} | Status: {item.status}
                  <br></br>
                </ol>
              </div>
            ))}{' '}
            <hr></hr>
            <h4>Adhoc indents started</h4>
            {itemStarted.map((item, index) => (
              <div>
                {' '}
                <ol
                  key={item.location.toString()}
                  style={{
                    textAlign: 'center',
                    margin: 0,
                    color: 'black',
                    padding: 0,
                    marginBlockStart: 0,
                    paddingInlineStart: 0,
                    marginBlockEnd: 0,
                  }}
                >
                  Start Location: {item.location} | Destination:
                  {item.destination} | Reason: {item.reason} | Vehicle:{' '}
                  {item.vehicle} | Status: {item.status}
                  <br></br>
                </ol>
              </div>
            ))}
            <hr></hr>
            <h4>Adhoc indents completed</h4>
            {itemCompleted.map((item, index) => (
              <div
                style={{
                  border: '2px solid',
                  borderCollapse: true,
                  borderRadius: '5px',
                  padding: '4px',
                  marginBottom: 15,
                }}
              >
                {' '}
                <ol
                  key={item.location.toString()}
                  style={{
                    textAlign: 'center',
                    margin: 0,
                    color: 'black',
                    padding: 0,
                    marginBlockStart: 0,
                    paddingInlineStart: 0,
                    marginBlockEnd: 0,
                    marginBottom: 50,
                  }}
                >
                  Start Location: {item.location} | Destination:{' '}
                  {item.destination} | Reason: {item.reason} | Vehicle:{' '}
                  {item.vehicle} | Status: {item.status}
                  <br></br>
                </ol>
              </div>
            ))}
          </Container>
        </div>
      </div>
    );
  }
  handleSelect = (e) => {
    this.setState({ driverSelected: e.target.value });
    console.log(this.state.driverSelected);
  };
}

export default driver;
