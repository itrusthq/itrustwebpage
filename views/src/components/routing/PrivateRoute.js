import React, { useState, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Auth from '../auth/auth';
import axios from 'axios';
import { set } from 'mongoose';

function PrivateRoute({ component: Component, ...rest }) {
  const [authorized, setAuthorized] = useState(false);
  const [loading, setLoading] = useState(true);
  let data = localStorage.getItem('token');
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const fetchData = async () => {
    let res = await axios.get(
      '/api/auth/user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    setAuthorized(res.data.success);
    setLoading(false);
  };

  useEffect(() => {
    //The reason we use useEffect is simple. It checks to seee if authorized ever changes and if it does it re-renders. It follows a componentDidUpdate() philosphy. It doesn't keep track on redirects, however that is why we need loading. If it did we wouldn't need it.
    fetchData();
  }, []);
  console.log('loading' + loading);
  console.log('authorized' + authorized);
  if (loading) return 'Loading';
  //We check for loading because the async request may not have been completed which means that we don't want to re-render and stop the process...I should have got this sooner. Got so close tho.
  else
    return (
      <Route
        {...rest}
        render={(props) =>
          !authorized ? (
            <Redirect to={{ pathname: '/' }} />
          ) : (
            <Component {...props} />
          )
        }
      />
    );
}

export function PrivateRoute2({ component: Component, ...rest }) {
  console.log('HERERE');
  const [authorized, setAuthorized] = useState(false);
  const [unauthorized, setUnAuthorized] = useState(false);
  const [loading, setLoading] = useState(true);
  let data = localStorage.getItem('token');
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const fetchData = async () => {
    let res = await axios.get(
      '/api/auth/user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    if (res.data.user != undefined) {
      if (res.data.user.authorized == true) {
        setAuthorized(res.data.success);
        setLoading(false);
      } else {
        setUnAuthorized(true);
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    //The reason we use useEffect is simple. It checks to seee if authorized ever changes and if it does it re-renders. It follows a componentDidUpdate() philosphy. It doesn't keep track on redirects, however that is why we need loading. If it did we wouldn't need it.
    fetchData();
  }, []);
  console.log('loading' + loading);
  console.log('authorized' + authorized);
  if (loading) return 'Loading';
  else if (unauthorized == true)
    return (
      <Route
        {...rest}
        render={(props) => <Redirect to={{ pathname: '/dashboard' }} />}
      />
    );
  //We check for loading because the async request may not have been completed which means that we don't want to re-render and stop the process...I should have got this sooner. Got so close tho.
  else
    return (
      <Route
        {...rest}
        render={(props) =>
          !authorized ? (
            <Redirect to={{ pathname: '/' }} />
          ) : (
            <Component {...props} />
          )
        }
      />
    );
}

const PrivateRoute1 = ({ component: Component, ...rest }) => {
  PrivateRoute();
  console.log(PrivateRoute());
  return (
    <Route
      {...rest}
      render={(props) =>
        PrivateRoute.authorized === false ? (
          <Redirect to='/'></Redirect>
        ) : (
          <Component {...props} />
        )
      }
    />
  );
  return 'loading';
};
export default PrivateRoute;
