import React, { Fragment, useState } from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';
import SideMenu from '../sidemenu/sidemenu';
import { Card, Button, Collapse } from 'react-bootstrap';
import Fade from 'react-reveal/Fade';
/*import { render } from 'react-dom';
 import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';
import newOrder from '../newOrder/newOrder'; */
//Just for changes
import store from '../../store';
class history extends React.Component {
  //Stateful Class
  //To get access to any states and props
  table = [];
  constructor(props) {
    super(props);
    console.log(this.props.user);
    let arrOpen = new Array(20);
    arrOpen.fill(true, 0, 20);

    this.state = {
      allData: 'loading',
      arrOpen: arrOpen,
      order: [
        {
          activity: '',
          types: '',
          startDate: new Date(),
          endDate: new Date(),
          location: '',
          poc: '',
        },
      ],
      resources: [{ vehicle: 'car', driver: false, quantity: '2' }],
    };
    console.log(store.getState());
    let data = localStorage.getItem('token');
    data = JSON.parse(data);

    let allData = '';
    if (data != undefined) this.onChange(data);
  }
  async onChange(data) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth',
      {
        params: {
          id: data,
        },
      },
      config
    );
    console.log(res.data);
    this.setState({
      resources: res.data.orders[0].resources,
      allData: res.data.orders[0].persid,
      order: res.data.orders[0].orders,
    });
  }
  componentDidMount() {
    document.title = 'History';
  }
  createTable = () => {
    this.table = [];
    console.log(this.state.order);

    for (let i = 0; i < this.state.order.length; i++) {
      let processing = '';
      if (this.state.order[i].processing != undefined) {
        if (this.state.order[i].processing.localeCompare('Approved') == 0)
          processing = 'Fulfilled';
        if (
          this.state.order[i].processing.localeCompare('Rejected') == 0 ||
          this.state.order[i].processing.localeCompare(
            'Processing (pending approval)'
          ) == 0
        )
          processing = 'Unfulfilled';
      }
      if (new Date(this.state.order[i].startDate) <= new Date())
        this.table.push({
          activity: this.state.order[i].activity,
          types: this.state.order[i].types,
          startDate: new Date(
            this.state.order[i].startDate
          ).toLocaleString('en-GB', { timeZone: 'Asia/Singapore' }),
          endDate: new Date(this.state.order[i].endDate).toLocaleString(
            'en-GB',
            { timeZone: 'Asia/Singapore' }
          ),
          processing: processing,
          orderId: this.state.order[i]._id,
          reason: this.state.order[i].reason,
          remark: this.state.order[i].extraInfo,
          admin: this.state.order[i].indentApprover,
        });
    }
    console.log(this.table);
    return this.table;
  };
  changeOpen = (x) => {
    let arrOpen = [...this.state.arrOpen];
    arrOpen[x] = !arrOpen[x];
    console.log(arrOpen[x]);
    this.setState({ arrOpen });
  };
  render() {
    let createCard = {};
    if (this.state.order != undefined) {
      let table = this.table;
      this.createTable();
      console.log(this.table);
      if (window.screen.width < 700) {
        createCard = (
          <div>
            <h1>History</h1>
            {this.table.map((item, index) => (
              <div>
                <Fade left>
                  <Card
                    style={{
                      backgroundColor: 'whitesmoke',
                      border: '2px black',
                      padding: '5px',
                    }}
                  >
                    <Card.Title>
                      {item.orderId}
                      <Button
                        style={{ float: 'right' }}
                        onClick={() => this.changeOpen(index)}
                      >
                        {this.state.arrOpen[index] ? 'Close' : 'Open'}
                      </Button>
                    </Card.Title>
                    <Collapse in={this.state.arrOpen[index]}>
                      <Card.Body id='Body'>
                        {' '}
                        <div>
                          <table>
                            <thead style={{ maxWidth: 100 }}>
                              <tr>
                                <th
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  Activity
                                </th>
                                <th
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  Type
                                </th>
                                <th
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  Status
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  {item.activity}
                                </td>
                                <td
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  {item.types}
                                </td>
                                <td
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  {item.processing}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <Card.Text>
                          <li>Start: {item.startDate}</li>
                          <li>End: {item.endDate}</li>
                          {item.extraInfo === undefined ||
                          item.extraInfo.localeCompare('') === 0 ? null : (
                            <li>Remark: {item.extraInfo}</li>
                          )}
                          {item.admin === undefined ||
                          item.admin.localeCompare('') === 0 ? null : (
                            <li>Admin: {item.admin} </li>
                          )}
                          {item.reason === undefined ||
                          item.reason.localeCompare('') === 0 ? null : (
                            <li>Rejection reason: {item.reason}</li>
                          )}
                        </Card.Text>
                      </Card.Body>
                    </Collapse>{' '}
                    <Collapse in={this.state.arrOpen[index]}>
                      <Card.Footer></Card.Footer>
                    </Collapse>
                  </Card>{' '}
                </Fade>
                <hr></hr>
              </div>
            ))}
          </div>
        );
      }
      return (
        <div
          style={{
            backgroundColor: 'lightgrey',
            position: 'fixed',
            bottom: '0',
            right: '0',
            left: '0',
            top: '0',
            overflowY: 'auto',
          }}
        >
          <SideMenu></SideMenu>{' '}
          <div className='table'>
            <link
              rel='stylesheet'
              href='https://fonts.googleapis.com/icon?family=Material+Icons'
            ></link>
            <table />
            {window.screen.width < 700 ? (
              createCard
            ) : (
              <MaterialTable
                columns={[
                  { title: 'Activity', field: 'activity' },
                  { title: 'Type', field: 'types' },
                  { title: 'Start Date', field: 'startDate' },
                  { title: 'End Date', field: 'endDate' },
                  { title: 'Processing', field: 'processing' },
                  { title: 'Admin', field: 'Admin' },
                  { title: 'Reason', field: 'reason' },
                ]}
                data={this.createTable()}
                title='History'
                style={{ boxshadow: 10 }}
                options={{
                  header: true,
                  search: false,

                  headerStyle: {
                    backgroundColor: 'rgb(50, 135, 168)',
                    color: 'black',
                  },
                  rowStyle: {
                    backgroundColor: '#F7F7FF',
                    color: 'black',
                  },
                }}
              />
            )}{' '}
          </div>
        </div>
      );
    } else {
      return <div className='overall'>{this.state.order[16]}</div>;
    }
  }
}

export default history;
