import React, { useState, Component } from 'react';
import Navbar, { Container } from 'react-bootstrap';
import { TopNav } from '../topnav/topnav';
import MaterialTable from 'material-table';
import './itmsdashbord.css';
import image from '../dashboard/images/img.jpg';
import Clock from 'react-clock';
import 'react-clock/dist/Clock.css';
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';
import moment from 'moment';
import { messaging } from '../../init-fcm';
import SelectInput from '@material-ui/core/Select/SelectInput';
// ...

class itmsdashboard extends React.Component {
  constructor(props) {
    super(props);
    let data = localStorage.getItem('token');
    console.log(JSON.stringify(data));
    console.log('hello');
    this.state = {
      date: new Date(),
    };
    this.getIndents(data);
  }
  //Pushing changes
  async componentDidMount() {
    document.title = 'ITRUST-RM';
  }

  async getIndents(data) {
    //console.log(data);
    //console.log(data.token);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth/user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    //console.log(res.data);
    let res1 = await axios.get(
      '/api/post/all',

      {
        params: {
          hub: res.data.user.hub,
        },
      }
    );

    //console.log(res1);
    this.setState({
      //I need to change this for multiple orders so that it is res1.data.orders instead of res1.data.orders[0].orders. This is actually going to create quite a big problem as we will have to deal with orders[x].persid and orders[x].order[i] because there will be a lot more data and each persid will have their own order. If there is more than one hub than we need to check for that too before we add it to the indents. This going to be quite the overhual.
      order: res1.data.orders[0].orders,
      orderAll: res1.data.orders, //Keep it like this for now because its simpler. Remember this isn't one order and rather the entire history of orders. orders[x].orders is one indent made.
      persid: res1.data.orders[0].persid,
      unit: res1.data.orders[0].unit,
      hub: res1.data.orders[0].hub,
    });

    console.log(this.state);
  }

  todayIndents() {
    let allOrders = this.state.order;
    for (let x = 0; x < this.state.order.length; x++) {
      let oneOrder = allOrders[x];
    }
  }

  render() {
    let orders = [{ _id: '', startDate: '' }];
    if (this.state.order != undefined) {
      orders = this.state.order.filter(function (item) {
        if (new Date(item.startDate).getDate() === new Date().getDate()) {
          console.log(item);
          return item;
        }
      });
    }
    return (
      <div
        className='overlay'
        style={{
          color: 'white',
          backgroundColor: 'rgb(50, 80, 180, 0.20)',
        }}
      >
        <TopNav />
        <Container className='formCSS1'>
          <Row></Row>

          <h1>Today's indents </h1>
          <br></br>
          {orders.map((order, index) => (
            <div>
              <br></br>
              <ol
                style={{
                  textAlign: 'center',
                  margin: 0,
                  color: 'black',
                  padding: 0,
                  marginBlockStart: 0,
                  paddingInlineStart: 0,
                  marginBlockEnd: 0,
                }}
              >
                <hr></hr>
                Order Id: {order._id}, Status:{' '}
                {moment(new Date(order.startDate).getTime()).isBefore(
                  new Date().getTime()
                ) && order.processing.localeCompare('Rejected') === 0
                  ? 'Unfulfilled'
                  : null}
                {moment(new Date(order.startDate).getTime()).isBefore(
                  new Date().getTime()
                ) && order.processing.localeCompare('Approved') === 0
                  ? 'Fulfilled'
                  : null}
                {moment(new Date(order.startDate).getTime()).isBefore(
                  new Date().getTime()
                ) &&
                order.processing.localeCompare(
                  'Processing (pending approval)'
                ) === 0
                  ? 'Unfulfilled'
                  : null}
                {moment(new Date(order.startDate).getTime()).isAfter(
                  new Date().getTime()
                )
                  ? order.processing
                  : null}
              </ol>
              <br></br>
            </div>
          ))}

          <Row>
            <Col></Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default itmsdashboard;
