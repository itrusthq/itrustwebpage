import React, { Component } from 'react';
import axios from 'axios';
import { Table, Tabs, Tab } from 'react-bootstrap';
import '../resources/resources.css';
import { TopNav } from '../topnav/topnav';
import { TextCenter, FullscreenExit } from 'react-bootstrap-icons';
import image from '../dashboard/images/img.jpg';

class resources extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      key: 'vehicle',
      resources: 10,
      vehicles: ['', ''],
      drivers: [{ name: '', skillsets: [''] }],
    };
    let drivers1 = [{ name: '', skillsets: [''] }];

    this.getVehicles();
    this.getDrivers();
  }

  getDrivers = async () => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get('/api/driver/driver', config);
    console.log(res.data);
    this.setState({ drivers: res.data.drivers });
    console.log(this.state);
  };

  getVehicles = async () => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    let res = await axios.get('/api/vehicle', config);

    console.log(res.data);
    this.setState({
      vehicles: res.data.vehicles,
    });
    console.log(this.state);
  };

  componentDidMount() {
    document.title = 'Resources';
  }

  render() {
    let drivers1 = [{ name: '', skillsets: [''] }];
    if (this.state.driverFilter != undefined) {
      //we are checking wether a driver has to be filtered... we filter
      console.log(this.state.driverFilter);
      let name = this.state.driverFilter;
      drivers1 = this.state.drivers.filter(function (item) {
        console.log(item.name);
        if (item.name.localeCompare(name) == 0) return item.name;
      });
    } else {
      drivers1 = this.state.drivers;
    }
    return (
      <div
        className='overlay'
        style={{
          overflowY: 'auto',
          backgroundColor: 'rgb(50, 80, 180, 0.20)',
          backgroundSize: 'cover',
          overflow: 'auto',
        }}
      >
        <TopNav />

        <h1 style={{ fontSize: 50 }}>Resources Page </h1>
        <br></br>
        <div
          style={{
            display: 'flex',
            marginTop: 10,
            textAlign: 'inline',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <button //These are the filter links
            value='Sembawang Camp'
            onClick={(e) => {
              this.filter(e);
            }}
            className='buttonText'
          >
            Sembawang Camp
          </button>
          <br></br>
          <button
            value='CTN'
            onClick={(e) => {
              this.filter(e);
            }}
            className='buttonText'
          >
            CTN
          </button>
          <br />
          <button
            value='CLN'
            onClick={(e) => {
              this.filter(e);
            }}
            className='buttonText'
          >
            CLN
          </button>
        </div>
        <Tabs
          defaultActiveKey={this.state.key}
          activeKey={this.state.key}
          onSelect={(k) => this.setKey(k)}
          id='uncontrolled-tab-example'
          className='myClass'
        >
          <Tab eventKey='vehicle' title='Vehicle'>
            {' '}
            <div className='contentAlignment'>
              <div></div>

              <Table
                className='table1'
                style={{ maxWidth: window.screen.width - 50 }}
              >
                <thead>
                  <tr
                    style={{
                      backgroundColor: 'slategray',
                      border: 'solid',
                      padding: 10,
                    }}
                  >
                    <th>Unit</th>
                    <th>Vehicle</th>
                    <th>OrderId</th>
                  </tr>
                </thead>
                <tbody>{this.createTable()}</tbody>
              </Table>
            </div>
          </Tab>
          <Tab eventKey='drivers' title='Drivers'>
            <div className='contentAlignment'>
              <div></div>
              <Table
                className='table1'
                style={{ maxWidth: window.screen.width - 50 }}
              >
                <thead>
                  <tr
                    style={{
                      backgroundColor: 'slategray',
                      border: 'solid',
                      padding: 10,
                    }}
                  >
                    <th>Name</th>
                    <th>OrderId</th>
                    <th>ResourceId</th>
                  </tr>
                </thead>
                <tbody>{this.createDriverTable()}</tbody>
              </Table>
            </div>
          </Tab>{' '}
          <Tab eventKey='skillsets' title='SkillSets'>
            <div
              style={{
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              {drivers1.map((
                //This is actually the correct way of rendering lists in react map, reduce and filter
                item,
                index
              ) => (
                <div class='d-flex justify-content-center'>
                  <ol
                    key={index}
                    item={item}
                    style={{
                      backgroundColor: 'lightgray',
                      border: '2px black solid',
                      borderCollapse: true,
                      borderRadius: 5,
                      alignItems: 'center',
                      display: 'flexbox',
                      justifyContent: 'center',
                      padding: 5,
                      listStylePosition: 'inside',
                      width: 300,
                    }}
                  >
                    <div style={{ textAlign: 'center' }}>{item.name}</div>
                    <hr />
                    {item.skillsets.map((item1, index) => (
                      <li
                        style={{
                          marginTop: -5,
                          marginLeft: 100,
                        }}
                      >
                        {' '}
                        {item1}
                        <br></br>
                      </li>
                    ))}
                  </ol>
                </div>
              ))}
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  }

  filter = (e) => {
    //This is the filter for the vehicles
    console.log(e.target.value);
    let unit = e.target.value;
    this.setState({ filter: unit });
  };

  setKey = (key) => {
    //This sets the tab which is supposed to be open
    this.setState({ key: key });
  };
  filterDriver = (e) => {
    //filter for the driver
    let driver = e.target.text;
    this.setState({ driverFilter: driver });
    this.setKey('skillsets');
  };
  createTable = () => {
    //This creates teh vehicle table
    let table = [];
    for (let x = 0; x < this.state.vehicles.length; x++) {
      console.log(this.state.vehicles.length);
      if (
        this.state.filter == undefined ||
        this.state.filter.localeCompare(this.state.vehicles[x].unit) == 0
      )
        table.push(
          <tr key={'row' + x}>
            <td
              style={{ border: 'solid', backgroundColor: 'white' }}
              key={'i' + x}
            >
              {this.state.vehicles[x].unit}
            </td>
            <td
              style={{ border: 'solid', backgroundColor: 'white' }}
              key={this.state.vehicles[x].name}
            >
              {this.state.vehicles[x].name}
            </td>
            <td style={{ border: 'solid', backgroundColor: 'white' }} key={x}>
              {this.state.vehicles[x].orderId}
            </td>
          </tr>
        );
    }
    return table;
  };

  createDriverTable = () => {
    //This creates the driver table
    let table = [];
    console.log(this.state.drivers);
    for (let x = 0; x < this.state.drivers.length; x++) {
      console.log(this.state.drivers[x]);
      if (this.state.filter != undefined) {
        console.log(this.state.drivers[x]);
        if (this.state.filter.localeCompare(this.state.drivers[x].unit) == 0) {
          table.push(
            <tr key={'row' + x}>
              <td
                style={{ border: 'solid', backgroundColor: 'white' }}
                key={'i' + x}
              >
                <a
                  value={this.state.drivers[x].name}
                  onClick={(e) => {
                    this.filterDriver(e); //need to show a list of skill sets
                  }}
                >
                  {this.state.drivers[x].name}
                </a>
              </td>
              <td
                style={{ border: 'solid', backgroundColor: 'white' }}
                key={this.state.drivers[x].name}
              >
                {this.state.drivers[x].orderId}
              </td>
              <td style={{ border: 'solid', backgroundColor: 'white' }} key={x}>
                {this.state.drivers[x].resourceId}
              </td>
            </tr>
          );
        }
      } else {
        table.push(
          <tr key={'row' + x}>
            <td
              style={{ border: 'solid', backgroundColor: 'white' }}
              key={'i' + x}
            >
              <a
                value={this.state.drivers[x].name}
                onClick={(e) => {
                  this.filterDriver(e); //need to show a list of skill sets
                }}
                style={{ textDecoration: 'underline', color: 'blue' }}
                className='buttonText'
              >
                {this.state.drivers[x].name}
              </a>
            </td>
            <td
              style={{ border: 'solid', backgroundColor: 'white' }}
              key={this.state.drivers[x].name}
            >
              {this.state.drivers[x].orderId}
            </td>
            <td style={{ border: 'solid', backgroundColor: 'white' }} key={x}>
              {this.state.drivers[x].resourceId}
            </td>
          </tr>
        );
      }
    }
    return table;
  };
}

export default resources;
