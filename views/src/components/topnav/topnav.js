import React from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';

export const TopNav = () => {
  return (
    <Navbar
      collapseOnSelect
      bg='primary'
      variant='dark'
      style={{ width: '100%' }}
      expand='lg'
    >
      <Navbar.Brand href='/itms'>ITRUST</Navbar.Brand>
      <Navbar.Toggle aria-controls='responsive-navbar-nav' />
      <Navbar.Collapse id='responsive-navbar-nav'>
        <Nav expand='lg' className='mr-auto'>
          <Nav.Link href='/itms/indent'>Indent</Nav.Link>
          <Nav.Link href='/itms/resources'>Resources</Nav.Link>
          <Nav.Link href='/itms/adhoc'>Adhoc</Nav.Link>
          <Nav.Link href='/itms/driver'>Requester Adhoc</Nav.Link>
          <Nav.Link href='/' onClick={(e) => onLogout(e)}>
            Logout
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
  function onLogout() {
    sessionStorage.clear();
    localStorage.clear();
  }
};

const topNav = (
  <Navbar
    collapseOnSelect
    bg='primary'
    variant='dark'
    style={{ width: '100%' }}
    expand='lg'
  >
    <Navbar.Brand href='/itms'>ITRUST</Navbar.Brand>
    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
    <Navbar.Collapse id='responsive-navbar-nav'>
      <Nav expand='lg' className='mr-auto'>
        <Nav.Link href='/itms/indent'>Indent</Nav.Link>
        <Nav.Link href='/itms/resources'>Resources</Nav.Link>
        <Nav.Link href='/itms/adhoc'>Adhoc</Nav.Link>
        <Nav.Link href='/itms/driver'>Requester Adhoc</Nav.Link>
        <Nav.Link href='/' onClick={(e) => onLogout(e)}>
          Logout
        </Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

function onLogout() {
  sessionStorage.clear();
  localStorage.clear();
}

export default topNav;
