import React, { Fragment, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import FormCheck from 'react-bootstrap/FormCheck';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Link from 'react-router-dom/Link';
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';
import moment from 'moment';
import axios from 'axios';
import '../newOrder/newOrder.css';
import 'react-datepicker/dist/react-datepicker.css';
import { enGB, eo, ru } from 'date-fns/locale';
import SideMenu from '../sidemenu/sidemenu';
import topNav from '../topnav/topnav';
import { Trash } from 'react-bootstrap-icons';
import { PlusSquare } from 'react-bootstrap-icons';
import { Table } from 'react-bootstrap';
registerLocale('enGB', enGB);
class newOrder extends React.Component {
  //Stateful Class
  //To get access to any states and props
  constructor(props) {
    super(props);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let persid = '';
    this.state = {
      order: [
        {
          activity: '',
          types: 'Admin',
          startDate: new Date(),
          endDate: new Date(),
          location: '',
          poc: '',
          check: false,
          processing: 'Processing (pending approval)',
          resources: [{ vehicle: '5TON', driver: '0', quantity: '1' }],
          extraInfo: '',
          pax: '',

          muslim: '',
          nonMuslim: '',
          vegetarianChinese: '',
          vegetarianIndian: '',
        },
      ],
      resources: [{ vehicle: '5TON', driver: '0', quantity: '1' }],
      orderId: undefined,
      unit: '',
      hub: '',
      breakfast: 0,
      lunch: 0,
      dinner: 0,
      persid: '',
      authorized: false,
      displaySummary: false,
      displayResources: false,
      submitted: false,
    };
    let orderId;
    if (this.props.location.state != undefined) {
      this.state.orderId = this.props.location.state.orderId;
      if (this.props.location.state.resubmit != undefined)
        this.state.resubmit = this.props.location.state.resubmit;
    }
    let data = localStorage.getItem('token');

    this.onChange1(data);
  }

  componentDidMount() {
    document.title = 'Order';
  }

  async onChange1(data) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth//user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    let persid = res.data.user['persid'];
    this.setState({
      persid: persid,
      unit: res.data.user['unit'],
      hub: res.data.user['hub'],
      authorized: res.data.user.authorized,
    });
    this.onChangeless(data);
  }

  onChange(e) {
    const target = e.target;
    let value = '';
    let name = '';
    if (target != undefined) {
      name = target.name;
      value = target.value;

      let order = [...this.state.order];
      order[0][name] = value;
      this.setState({ order });
    }
  }

  startDate(e) {
    this.setState({ startDate: e });
    if (moment(this.state.endDate).isBefore(moment(this.state.startDate))) {
      this.state.order[0]['endDate'] = e;
    }
    this.state.order[0]['startDate'] = e;
  }
  endDate(e) {
    if (moment(e).isBefore(moment(this.state.startDate))) {
      alert('End Date has to be after Start Date');
    } else {
      this.setState({ endDate: e });
    }
    this.state.order[0]['endDate'] = e;
  }
  nextPath(path) {
    this.props.history.push(path);
  }
  ///////////////////////////////handle change////////////////////////////////////////////////////
  handleChange = (e) => {
    e.preventDefault();
    if (e.target.className.includes('vehicle')) {
      let resources = [...this.state.resources];
      resources[e.target.dataset.id]['vehicle'] = [
        e.target.value.toUpperCase(),
      ];
      this.setState({ resources });
    } else {
      this.setState({ [e.target.name]: [e.target.value] });
    }
  };

  handleChangeDriver = (e) => {
    if (e.target.className.includes('form')) {
      let resources = [...this.state.resources];
      resources[e.target.dataset.id]['driver'] = [e.target.value];
      this.setState({ resources });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  handleChangeQuantity = (e) => {
    e.preventDefault();

    if (e.target.className.includes('quantity')) {
      let resources = [...this.state.resources];

      resources[e.target.dataset.id]['quantity'] = [e.target.value];
      this.setState({ resources });
    } else {
      this.setState({ [e.target.name]: [e.target.value] });
    }
  };
  /////////////////////////////////////////////////////////////////////////////////////////////END OF CHANGE QUANTITY//////////////////////////////////////////////////////////////////////////////////////
  onSubmit = async (e) => {
    this.setState({
      [this.state.order[0]['resources']]: this.state.resources[0],
    }); //This is because in the start the resources we set is actually outside order as a seperate array
    if (this.state.resubmit === true)
      this.setState(() => ({ orderId: undefined }));
    e.preventDefault();

    if (this.state.displayResources === false) {
      if (this.state.displaySummary === true);
      else if (this.state.order[0].check === true) this.displaySummary();
      else this.displayResources();
    }
    if (
      this.state.displayResources === true ||
      (this.state.order[0].check === true && this.state.displaySummary === true)
    ) {
      if (this.state.displaySummary === false) {
        this.displaySummary();
      } else {
        let resources = [...this.state.resources];
        let sumOfDriver = 0;
        for (let x = 0; x < resources.length; x++) {
          let arrayOfApproves = new Array(
            parseInt(this.state.resources[x].driver[0])
          );
          let arrayOfVehicleApproves = new Array(
            parseInt(this.state.resources[x].quantity[0])
          );
          let arrayOfDrivers = new Array(
            parseInt(this.state.resources[x].driver[0])
          );
          let arrayOfVehicles = new Array(
            parseInt(this.state.resources[x].quantity[0])
          );

          arrayOfApproves.fill(
            false,
            0,
            parseInt(this.state.resources[x].driver[0])
          );
          arrayOfVehicleApproves.fill(
            false,
            0,
            parseInt(this.state.resources[x].quantity[0])
          );
          arrayOfDrivers.fill(
            '',
            0,
            parseInt(this.state.resources[x].driver[0])
          );
          arrayOfVehicles.fill(
            '',
            0,
            parseInt(this.state.resources[x].quantity[0])
          );

          sumOfDriver += parseInt(this.state.resources[x].driver[0]);
          resources[x]['approve'] = arrayOfApproves;
          resources[x]['driverSum'] = sumOfDriver;
          resources[x]['driverAssigned'] = arrayOfDrivers;
          resources[x]['vehicleApprove'] = arrayOfVehicleApproves;
          resources[x]['vehicleAssigned'] = arrayOfVehicles;
        }
        if (this.state.order[0].check === true) {
          let x = 0;
          let y = 0;
          let z = 0;
          let startDate = moment(this.state.order[0].startDate);
          let endDate = moment(this.state.order[0].endDate);
          if (startDate.hour() < 6) {
            //Need to confirm timing
            x = 1;
            y = 1;
            z = 1;
          } else if (startDate.hour() < 12) {
            z = 1;
            y = 1;
          } else if (startDate.hour() < 16) {
            z = 1;
          }
          console.log(this.state.order[0].endDate);
          console.log(endDate);
          console.log(endDate.hour());
          if (endDate.hour() > 16) {
            x += 1;
            y += 1;
            z += 1;
          } else if (endDate.hour() > 12) {
            x = x + 1;
            y = y + 1;
          } else if (endDate.hour() > 6) {
            x += 1;
          }
          console.log('x: ' + x + 'Y: ' + y + 'z' + z);
          let start1 = startDate.set('hour', 8);
          let end1 = endDate.set('hour', 9);

          let numDays = end1.diff(start1, 'days');
          console.log(numDays);
          let breakfast = numDays + x;
          let lunch = numDays + y;
          let dinner = numDays + z;
          console.log(
            'breakfast: ' + breakfast + 'lunch: ' + lunch + 'dinner: ' + dinner
          );

          this.setState(
            (prevState) => {
              prevState.breakfast = breakfast;
              prevState.lunch = lunch;
              prevState.dinner = dinner;
            },
            async () => {
              try {
                const config = {
                  headers: {
                    'Content-Type': 'application/json',
                  },
                };
                const body = JSON.stringify(this.state);
                console.log(body);
                this.setState({ submitted: true });
                if (this.state.submitted == false) {
                  const res = await axios.post('/api/order', body, config);
                  if (this.state.authorized == true) {
                    this.nextPath('/itms/indent');
                  } else {
                    this.nextPath('/processing');
                  }
                }
              } catch (err) {
                console.log(err.data);
              }
            }
          );
        } else {
          this.setState({
            resources: resources,
          });

          try {
            const config = {
              headers: {
                'Content-Type': 'application/json',
              },
            };
            const body = JSON.stringify(this.state);
            console.log(body);
            this.setState({ submitted: true });
            if (this.state.submitted == false) {
              const res = await axios.post('/api/order', body, config);
              if (this.state.authorized == true) {
                this.nextPath('/itms/indent');
              } else {
                this.nextPath('/processing');
              }
            }
          } catch (err) {
            console.log(err.data);
          }
        }
      }
    }
  };

  addResources = (e) => {
    this.setState((prevState) => ({
      resources: [
        ...prevState.resources,
        { vehicle: '5TON', driver: '0', quantity: '1' },
      ],
    }));
  };

  async onChangeless(data) {
    if (this.state.orderId != undefined) {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };
      let res = await axios.get(
        '/api/post',
        {
          params: {
            ID: this.state['persid'],
          },
        },
        config
      );

      res.data.orders[0].orders.forEach((Element) => {
        if (Element._id.localeCompare(this.state.orderId) == 0) {
          Element.startDate = new Date(Element.startDate);
          Element.endDate = new Date(Element.endDate);
          Element.processing = 'Processing (pending approval)';
          this.setState({
            order: [Element],
            resources: Element.resources,
          });
        }
      });
    }
  }

  removeResources = (e) => {
    var array = [...this.state.resources]; // make a separate copy of the array
    var index = e.target.value;

    if (index !== 0 && index != undefined) {
      array.splice(index, 1);
      this.setState({ resources: array });
    }
  };

  handleCheck = (e) => {
    let order = this.state.order[0];
    order.check = !order.check;
    order = [order];
    this.setState({ order });
  };

  render() {
    let { resources } = this.state;
    let { order } = this.state;
    let authorized = this.state.authorized;
    let summary = null;
    let buttons = null;
    let navi = null;
    let margin = null;
    let testcss = null;
    let summaryStyle = {};
    if (authorized == true) {
      if (!this.state.displaySummary) navi = <div>{topNav}</div>;
      margin = (
        <h1 style={{ color: 'black', marginLeft: '0px', marginTop: '10px' }}>
          New Order
        </h1>
      );
      testcss = 'test2';
    } else {
      navi = <SideMenu></SideMenu>;
      margin = (
        <h1 style={{ color: 'black', marginLeft: '50px', marginTop: '10px' }}>
          New Order
        </h1>
      );
      testcss = 'test1';
    }
    if (this.state.displaySummary) {
      summaryStyle = {};
      buttons = (
        <div>
          <hr className='breakLine2'></hr>
          <Button
            variant='primary'
            type='submit'
            className='btn btn-primary btn-lg btn-block'
            value='Login'
            onClick={(e) => {
              this.onSubmit(e);
            }}
            style={{ background: '#3d74db', borderStyle: 'none' }}
          >
            Submit
          </Button>
          <Button
            variant='primary'
            className='btn btn-primary btn-lg btn-block'
            onClick={this.displaySummary}
            style={{ background: '#3d74db', borderStyle: 'none' }}
          >
            Edit
          </Button>
        </div>
      );
      let classNameForSummary = 'overall2';
      if (this.state.authorized == true) classNameForSummary = 'overall1';
      return (
        <div className='overall1'>
          {navi}
          <Container
            style={{
              padding: 30,
            }}
          >
            <Row
              style={{
                width: 300,
                marginTop: -10,
                clear: 'both',
              }}
            >
              <h1
                style={{
                  display: 'inline-block',
                  justifyContent: 'center',
                  float: 'center',
                }}
              >
                Summary Page
              </h1>
            </Row>
            <Row>
              <Col style={{ clear: 'both' }}>
                <h5 style={{ display: 'inline', float: 'left' }}>
                  Activity:{'\u00A0'}
                </h5>
                <h5 style={{}}> {this.state.order[0].activity}</h5>
              </Col>

              <Col style={{ clear: 'both' }}>
                <h5 style={{ display: 'inline', float: 'left' }}>
                  Type:{'\u00A0'}
                </h5>
                <h5 style={{}}> {this.state.order[0].types}</h5>
              </Col>
            </Row>
            <Row>
              <Col style={{ clear: 'both' }}>
                <h5 style={{ display: 'inline', float: 'left' }}>
                  POC:{'\u00A0'}
                </h5>{' '}
                <h5 style={{}}> {this.state.order[0].poc}</h5>
              </Col>

              <Col style={{ clear: 'both' }}>
                <h5 style={{ display: 'inline', float: 'left' }}>
                  Location:{'\u00A0'}
                </h5>
                <h5 style={{}}> {this.state.order[0].location}</h5>
              </Col>
            </Row>

            <Row>
              <Col>
                <h4 style={{ display: 'inline' }}>Start: </h4>
                <h6 style={{ display: 'inline' }}>
                  {moment(this.state.order[0].startDate).format(
                    ' Do MMMM YYYY, h:mm a'
                  )}
                </h6>
              </Col>
            </Row>
            <Row>
              <Col>
                <h4 style={{ display: 'inline' }}>End: </h4>
                <h6 style={{ display: 'inline' }}>
                  {moment(this.state.order[0].endDate).format(
                    ' Do MMMM YYYY, h:mm a'
                  )}
                </h6>
              </Col>
            </Row>
            {this.state.order[0].check === true ? null : (
              <div>
                <Row>
                  <Col>
                    <h4 style={{ display: 'inline' }}>Resources: </h4>
                    <table
                      style={{
                        border: '1px solid black',
                        width: '40%',
                      }}
                    >
                      <thead>
                        <tr style={{ border: '1px solid black' }}>
                          <th
                            style={{
                              border: '1px solid black',

                              textAlign: 'left',
                            }}
                          >
                            Vehicles{' '}
                          </th>
                          <th
                            style={{
                              border: '1px solid black',

                              textAlign: 'left',
                            }}
                          >
                            Quantity{' '}
                          </th>
                          <th
                            style={{
                              border: '1px solid black',

                              textAlign: 'left',
                            }}
                          >
                            Drivers{' '}
                          </th>
                        </tr>
                      </thead>
                      <tbody>{this.createTable()}</tbody>
                    </table>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h4 style={{}}>Remarks: </h4>
                    <h5 style={{ wordWrap: 'break-word' }}>
                      {this.state.order[0].extraInfo}
                    </h5>
                  </Col>
                </Row>
              </div>
            )}

            <br></br>

            {buttons}
          </Container>
        </div>
      );
    } else if (this.state.displayResources == true) {
      buttons = (
        <div>
          <Button
            variant='primary'
            type='submit'
            className='btn btn-primary btn-lg btn-block'
            value='Login'
            style={{ background: '#3d74db', borderStyle: 'none' }}
          >
            Proceed to Summary Page
          </Button>
          <Button
            variant='primary'
            className='btn btn-primary btn-lg btn-block'
            onClick={this.displayResources}
            style={{ background: '#3d74db', borderStyle: 'none' }}
          >
            Back to Previous Page
          </Button>
        </div>
      );
      return (
        <div>
          <div className='overall' style={{ backgroundColor: 'white' }}>
            {navi}
            <Container>
              <br></br>
              <Form className={testcss} onSubmit={(e) => this.onSubmit(e)}>
                <div style={summaryStyle}>
                  <Col xs={12}>{margin}</Col> <hr className='breakLine'></hr>{' '}
                  <h2>Resources</h2>
                  {resources.map((val, idx) => {
                    let resourceId = `resources-${idx}`,
                      vehicleId = `vehicle-${idx}`,
                      driverId = `driver-${idx}`,
                      quantityId = `quantity-${idx}`;

                    return (
                      <div key={idx}>
                        <Form.Label>{`Resource #${idx + 1}`}</Form.Label>
                        <Form.Row>
                          <Form.Group as={Col} className='test'>
                            <Form.Label>Vehicle: </Form.Label>

                            <Form.Control
                              as='select'
                              placeholder='vehicles'
                              name={vehicleId}
                              data-id={idx}
                              id={vehicleId}
                              value={resources[idx].vehicle}
                              multiple={false}
                              className='vehicle'
                              onChange={this.handleChange}
                              required
                            >
                              <option>5TON</option>
                              <option>6TON</option>
                              <option>OUV</option>
                            </Form.Control>
                          </Form.Group>
                          <Form.Group as={Col} className='test'>
                            <Form.Label>Quantity: </Form.Label>

                            <Form.Control
                              as='select'
                              placeholder='quantity'
                              name={quantityId}
                              data-id={idx}
                              id={quantityId}
                              value={resources[idx].quantity}
                              multiple={false}
                              className='quantity'
                              onChange={this.handleChangeQuantity}
                              required
                            >
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                            </Form.Control>
                          </Form.Group>
                        </Form.Row>
                        <Form.Group className='test'>
                          <Form.Label>Driver: </Form.Label>

                          <Form.Control
                            as='select'
                            placeholder='quantity'
                            name={driverId}
                            data-id={idx}
                            id={driverId}
                            value={resources[idx].driver}
                            multiple={false}
                            className='quantity'
                            onChange={this.handleChangeDriver}
                            required
                          >
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                          </Form.Control>
                        </Form.Group>
                        <Button
                          onClick={this.addResources}
                          style={{
                            align: 'center',
                            positon: 'fixed',
                            backgroundColor: '#89B6A5',
                            border: 'none',
                            width: 85,
                          }}
                        >
                          <PlusSquare color='black' size={25} />
                        </Button>{' '}
                        <Button
                          value={idx}
                          onClick={this.removeResources}
                          variant='danger'
                          style={{ width: 85 }}
                        >
                          Remove
                        </Button>
                      </div>
                    );
                  })}
                  <hr className='breakLine'></hr>
                  <Form.Label style={{ marginBottom: 0 }}>
                    Remarks/Extrainfo:{' '}
                  </Form.Label>
                  <Form.Group controlId='exampleForm.ControlInput4'>
                    <Form.Control
                      as='textarea'
                      type='text'
                      name='extraInfo'
                      value={this.state.order[0].extraInfo}
                      onChange={(e) => this.onChange(e)}
                    ></Form.Control>
                  </Form.Group>
                  <div style={{ marginBottom: 20 }}>{buttons}</div>
                </div>
              </Form>
            </Container>
          </div>
        </div>
      );
    } else {
      summary = <Col xs={12}>{margin}</Col>;
      if (this.state.order[0].check === true) {
        buttons = (
          <Button
            variant='primary'
            type='submit'
            className='btn btn-primary btn-lg btn-block'
            value='summary'
            style={{
              background: '#3d74db',
              borderStyle: 'none',
            }}
          >
            Proceed to Summary
          </Button>
        );
      } else
        buttons = (
          <Button
            variant='primary'
            type='submit'
            className='btn btn-primary btn-lg btn-block'
            value='Login'
            style={{
              background: '#3d74db',
              borderStyle: 'none',
            }}
          >
            Proceed to Resources Section
          </Button>
        );
    }
    /////////////////////////////////////////////////////RETURN AND RENDER/////////////////////////////////////////////////////////
    return (
      <div className='overall'>
        {navi}
        <Container>
          <br></br>
          <Form className={testcss} onSubmit={(e) => this.onSubmit(e)}>
            {summary}
            <div style={summaryStyle}>
              <Form.Group
                className='input-field'
                controlId='exampleForm.ControlInput3'
                style={{ marginTop: 15 }}
              >
                <Form.Control
                  type='text'
                  name='activity'
                  className='textField'
                  value={this.state.order[0].activity}
                  onChange={(e) => this.onChange(e)}
                  required
                ></Form.Control>
                <Form.Label className='label'>Activity: </Form.Label>
              </Form.Group>
              <Form.Group>
                <Form.Check
                  as='input'
                  style={{ display: 'inline', width: 10 }}
                  className='input2'
                  onChange={(e) => this.handleCheck(e)}
                  value={this.state.order[0].check}
                  checked={this.state.order[0].check}
                  label='Shared Resource'
                  name='check'
                  id='validationFormik0612'
                ></Form.Check>
              </Form.Group>
              {this.state.order[0].check === true ? (
                <Form.Group controlId='formselectType'>
                  <Form.Label style={{ display: 'inline' }}>Type: </Form.Label>

                  <Form.Control
                    as='select'
                    name='types'
                    style={{ display: 'inline', width: '80%' }}
                    value={this.state.order[0].types}
                    onChange={(e) => this.onChange(e)}
                    required
                  >
                    <option>Admin</option>
                  </Form.Control>
                </Form.Group>
              ) : (
                <Form.Group controlId='formselectType'>
                  <Form.Label style={{ display: 'inline' }}>Type: </Form.Label>

                  <Form.Control
                    as='select'
                    name='types'
                    style={{ display: 'inline', width: '80%' }}
                    value={this.state.order[0].types}
                    onChange={(e) => this.onChange(e)}
                    required
                  >
                    <option>Admin</option>
                    <option>Safety</option>
                    <option>Training</option>
                  </Form.Control>
                </Form.Group>
              )}
              {this.state.order[0].check === true ? (
                <div>
                  <Form.Group
                    className='input-field'
                    controlId='exampleForm.ControlInput31313123'
                    style={{ marginTop: 15 }}
                  >
                    <Form.Control
                      type='number'
                      name='pax'
                      className='textField'
                      value={this.state.order[0].pax}
                      onChange={(e) => this.onChange(e)}
                      required
                    ></Form.Control>
                    <Form.Label className='label'>Total Pax: </Form.Label>
                  </Form.Group>
                  <Form.Group
                    className='input-field'
                    controlId='exampleForm.412'
                    style={{ marginTop: 15 }}
                  >
                    <Form.Control
                      type='number'
                      name='muslim'
                      className='textField'
                      value={this.state.order[0].muslim}
                      onChange={(e) => this.onChange(e)}
                      required
                    ></Form.Control>
                    <Form.Label className='label'>Muslim: </Form.Label>
                  </Form.Group>
                  <Form.Group
                    className='input-field'
                    controlId='exampleForm.5345'
                    style={{ marginTop: 15 }}
                  >
                    <Form.Control
                      type='number'
                      name='nonMuslim'
                      className='textField'
                      value={this.state.order[0].nonMuslim}
                      onChange={(e) => this.onChange(e)}
                      required
                    ></Form.Control>
                    <Form.Label className='label'>Non-Muslim: </Form.Label>
                  </Form.Group>
                  <Form.Group
                    className='input-field'
                    controlId='exampleForm.ControlInput334151354'
                    style={{ marginTop: 15 }}
                  >
                    <Form.Control
                      type='number'
                      name='vegetarianIndian'
                      className='textField'
                      value={this.state.order[0].vegetarianIndian}
                      onChange={(e) => this.onChange(e)}
                      required
                    ></Form.Control>
                    <Form.Label className='label'>
                      Indian Vegetarian:{' '}
                    </Form.Label>
                  </Form.Group>
                  <Form.Group
                    className='input-field'
                    controlId='exampleForm.34513453145233222'
                    style={{ marginTop: 15 }}
                  >
                    <Form.Control
                      type='number'
                      name='vegetarianChinese'
                      className='textField'
                      value={this.state.order[0].vegetarianChinese}
                      onChange={(e) => this.onChange(e)}
                      required
                    ></Form.Control>
                    <Form.Label className='label'>
                      Chinese Vegetarian:{' '}
                    </Form.Label>
                  </Form.Group>
                  <Form.Label style={{ marginBottom: 0 }}>
                    Remarks/Extrainfo:{' '}
                  </Form.Label>
                  <Form.Group controlId='exampleForm.ControlInput4'>
                    <Form.Control
                      as='textarea'
                      type='text'
                      name='extraInfo'
                      value={this.state.order[0].extraInfo}
                      onChange={(e) => this.onChange(e)}
                    ></Form.Control>
                  </Form.Group>
                </div>
              ) : null}
              <Form.Group>
                <Form.Label
                  style={{ display: 'inline' }}
                >{`Start Date: `}</Form.Label>
                <DatePicker
                  selected={this.state.order[0].startDate}
                  name='startDate'
                  showTimeSelect
                  dateFormat='Pp'
                  minDate={new Date()}
                  locale='enGB'
                  style={{ display: 'inline' }}
                  onChange={(e) => this.startDate(e)}
                />
              </Form.Group>{' '}
              <Form.Group>
                <Form.Label style={{ display: 'inline' }}>
                  End Date: {'\u00A0'}
                </Form.Label>
                <DatePicker
                  selected={this.state.order[0].endDate}
                  name='endDate'
                  showTimeSelect
                  dateFormat='Pp'
                  locale='enGB'
                  minDate={this.state.startDate}
                  onChange={(e) => this.endDate(e)}
                />
              </Form.Group>
              {this.state.order[0].check === true ? null : (
                <Form.Label>
                  Start Point:<b> {this.state.unit}</b>
                </Form.Label>
              )}
              {this.state.order[0].check === true ? (
                <Form.Group
                  className='input-field'
                  controlId='exampleForm.ControlInput1'
                >
                  <Form.Control
                    type='text'
                    className='textField'
                    name='location'
                    value={this.state.order[0].location}
                    onChange={(e) => this.onChange(e)}
                    required
                  ></Form.Control>
                  <Form.Label className='label'>Training Location: </Form.Label>
                </Form.Group>
              ) : (
                <Form.Group
                  className='input-field'
                  controlId='exampleForm.ControlInput1'
                >
                  <Form.Control
                    type='text'
                    className='textField'
                    name='location'
                    value={this.state.order[0].location}
                    onChange={(e) => this.onChange(e)}
                    required
                  ></Form.Control>
                  <Form.Label className='label'>End Point: </Form.Label>
                </Form.Group>
              )}
              <Form.Group
                className='input-field'
                controlId='exampleForm.ControlInput2'
              >
                <Form.Control
                  type='text'
                  className='textField'
                  name='poc'
                  value={this.state.order[0].poc}
                  onChange={(e) => this.onChange(e)}
                  required
                ></Form.Control>
                <Form.Label className='label'>POC: </Form.Label>
              </Form.Group>
            </div>
            {buttons}
          </Form>
        </Container>
      </div>
    );
  }
  appendInput() {
    var newInput = `input-${this.state.inputs.length}`;
    this.setState((prevState) => ({
      inputs: prevState.inputs.concat([newInput]),
    }));
  }

  displaySummary = () => {
    let summ = !this.state.displaySummary;
    this.setState((prevState) => ({
      displaySummary: !prevState.displaySummary,
    }));
  };

  displayResources = () => {
    this.setState({ displayResources: !this.state.displayResources });
  };

  createTable = () => {
    let arr = [];

    for (let i = 0; i < this.state.resources.length; i++) {
      arr.push(
        <tr key={i} style={{ border: '1px solid black', padding: 3 }}>
          <td style={{ border: '1px solid black', padding: 3 }}>
            {this.state.resources[i].vehicle}
          </td>
          <td style={{ border: '1px solid black', padding: 3 }}>
            {this.state.resources[i].quantity}
          </td>
          <td style={{ border: '1px solid black', padding: 3 }}>
            {this.state.resources[i].driver}
          </td>
        </tr>
      );
    }
    return arr;
  };
}

class summary extends React.Component {
  render() {
    return (
      <div className='summary'>
        <Button
          variant='primary'
          type='submit'
          className='btn btn-primary btn-lg btn-block'
          value='Login'
        >
          Confirm Indent
        </Button>
        <Button
          variant='primary'
          type='submit'
          className='btn btn-primary btn-lg btn-block'
          value='Login'
          summary={this.state}
        >
          Back
        </Button>
      </div>
    );
  }
}
export default newOrder;
