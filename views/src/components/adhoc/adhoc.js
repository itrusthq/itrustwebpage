import React from 'react';
import axios from 'axios';
import InfoBox from 'react-google-maps/lib/components/addons/InfoBox';
import { Container } from '@material-ui/core';
import { Form, Button, Accordion } from 'react-bootstrap';
import { ThreeDots, X } from 'react-bootstrap-icons';
import moment from 'moment';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import '../adhoc/adhoc.css';
import { messaging } from '../../init-fcm';
import Collapse from 'react-bootstrap/Collapse';
import { PlusSquare } from 'react-bootstrap-icons';
import { TopNav } from '../topnav/topnav';

class adhoc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMarkerShown: false,
      location: '',
      destination: '',
      quantity: '',
      vehicles: '5TON',
      reason: '',
      adhoc: [],
      submitted: false,
      persid: '',
      order: {},
      typeName: 'Breakfast',
      startDate: new Date(), //I may need to change this later
      endDate: new Date(),
      resources: [
        {
          date: moment(),
          rationId: 0,
          typeName: '',
          remarks: '',
          arr: [{ driver: 0, vehicle: '' }],
        },
      ],
      drivers: [{ name: '' }],
      arrOpen: [true],
      driverSelected: '',
      vehicles: [{ name: '' }],
      vehicleSelected: '',
      vehicleSelectedDefault: '',
      open: true,
    };
    window.setInterval(this.componentDidMount.bind(this), 10000);
  }
  componentDidMount() {
    this.getIndents();
    this.getSharedResource();
    this.getDrivers();
    this.getVehicles();
  }

  //I need to add a get all indents here

  async getIndents() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get('/api/adhoc', config);
    this.setState({ adhoc: res.data.adhoc });
  }
  async getSharedResource() {
    let data = localStorage.getItem('token');
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res1 = await axios.get(
      '/api/auth/user',
      {
        params: {
          id: data,
        },
      },
      config
    );
    let res = await axios.get(
      '/api/post/all',

      {
        params: {
          hub: res1.data.user.hub,
        },
      }
    );
    let allSharedResource = [];
    let resources = [];
    for (let i = 0; i < res.data.orders.length; i++) {
      for (let x = 0; x < res.data.orders[i].orders.length; x++) {
        if (res.data.orders[i].orders[x].check === true) {
          if (
            moment(new Date()).isBefore(
              new Date(res.data.orders[i].orders[x].endDate),
              null,
              '[]'
            )
          ) {
            allSharedResource.push(res.data.orders[i].orders[x]);
          } else {
          }
        }
      }
    }
    console.log(allSharedResource);
    let type = this.state.typeName.toLowerCase();
    let filterd = [];
    let state = [];
    for (let i = 0; i < allSharedResource.length; i++) {
      //If I am doing anything with checking wether endDate has passed, I have to do it here cuz rationId depends on i, so I cannot change length. I could try changing i to rationId. Probably should lol.
      let rationRun = allSharedResource[i].rationRun[0];
      if (rationRun !== undefined)
        for (let i = 0; i < rationRun.resources.length; i++) {
          resources.push(rationRun.resources[i]);
        }
    }
    if (typeof messaging !== 'string') {
      messaging
        .requestPermission()
        .then(async function () {
          const token = await messaging.getToken();
          console.log(token);
          const config = {
            headers: {
              'Content-Type': 'application/json',
            },
          };
          let body = {
            token: token,
            resources: resources,
          };

          let res = axios.post('/api/post/notif', body, config);
        })
        .catch(function (err) {
          console.log('Unable to get permission to notify.', err);
        });
      if (document.hasFocus()) {
        navigator.serviceWorker.addEventListener('message', (message) => {
          alert(
            'Requester left a remark in: ' +
              message.data['firebase-messaging-msg-data'].data.my_another_key +
              ' Date: ' +
              message.data['firebase-messaging-msg-data'].data.my_third_key +
              ' Type: ' +
              message.data['firebase-messaging-msg-data'].data.my_last_key +
              ' Remark: ' +
              message.data['firebase-messaging-msg-data'].data.my_key
          );
          console.log(message);
        });
      }
    }
    console.log(res.data.orders[0].orders);
    this.setState({
      persid: res.data.orders[0].persid,
      order: res.data.orders,
      resources: resources,
    });
    let arrOpen = new Array(20);
    arrOpen.fill(true, 0, 20);
    this.setState({ arrOpen: arrOpen });
  }

  async getDrivers() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get('/api/driver/driver', config);
    //console.log(res);
    for (let x = 0; x < res.data.drivers.length; x++) {
      if (res.data.drivers[x].persid == this.state.persid) {
        this.setState({ driver: res.data.drivers[x] });
      }
    }
    let resources = [...this.state.resources];

    resources[0].driver = res.data.drivers[0].name;

    this.setState({
      resources,
      drivers: res.data.drivers,
      driverSelected: res.data.drivers[0].name,
      driverSelectedDefaut: res.data.drivers[0].name,
    });
  }

  async getVehicles() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get('/api/vehicle', config);
    //console.log(res);
    let resources = [...this.state.resources];
    resources[0].vehicle = res.data.vehicles[0].name;
    this.setState({
      resources,
      vehicles: res.data.vehicles,
      vehicleSelected: res.data.vehicles[0].name,
      vehicleSelectedDefault: res.data.vehicles[0].name,
    });
  }

  addResources = (e, dateSet, i, typeName) => {
    let resources = [...this.state.resources];
    let arr = [
      {
        driver: this.state.driverSelectedDefaut,
        vehicle: this.state.vehicleSelectedDefault,
      },
    ];
    let bool = false;
    for (let x = 0; x < resources.length; x++) {
      let date = resources[x].date;
      console.log(date);
      console.log(dateSet);
      if (date.date() === dateSet.date()) {
        console.log(JSON.stringify(resources) + '+' + i);

        if (
          resources[x].rationId === i &&
          resources[x].typeName.localeCompare(typeName) === 0
        ) {
          console.log('I am over here' + i);
          resources[x].arr.push({
            driver: this.state.driverSelectedDefaut,
            vehicle: this.state.vehicleSelectedDefault,
            typeName: typeName,
          });
          bool = true;
        }
      }
    }
    console.log(dateSet);
    console.log(bool);
    if (bool === false)
      resources.push({
        date: dateSet,
        rationId: i,
        typeName: typeName,
        arr: arr,
      });
    this.setState({ resources }, () => console.log(this.state));
    /*   this.setState((prevState) => ({
      resources: [
        ...prevState.resources,
        {
          vehicle: this.state.vehicleSelectedDefault,
          driver: this.state.driverSelectedDefaut,
          quantity: '1',
        },
      ],
    })); */
  };
  removeResources = (e, dateSet, i) => {
    var array = [...this.state.resources]; // make a separate copy of the array
    var index = e.target.value;
    let resources = [...this.state.resources];
    let arr = [{ driver: '', vehicle: '' }];
    let bool = false;
    for (let x = 0; x < resources.length; x++) {
      let date = resources[x].date;
      console.log(date);
      console.log(dateSet);
      if (date.date() === dateSet.date()) {
        if (resources[x].rationId === i) {
          resources[x].arr.splice(index, 1);
          this.setState({ resources: array });
        }
      }
    }
    if (index !== 0 && index != undefined) {
    }
  };
  changeOpen = (x) => {
    let arrOpen = [...this.state.arrOpen];
    arrOpen[x] = !arrOpen[x];
    console.log(arrOpen[x]);
    this.setState({ arrOpen });
  };
  handleVehicleChange = (e, date, i, typeName) => {
    e.preventDefault();

    if (e.target.className.includes('vehicle')) {
      let resources = [...this.state.resources];
      for (let x = 0; x < resources.length; x++) {
        console.log(resources[x].date);
        if (resources[x].date.date() === date.date()) {
          if (
            resources[x].rationId === i &&
            resources[x].typeName === typeName
          ) {
            console.log(resources[x].arr);
            resources[x].arr[e.target.dataset.id][
              'vehicle'
            ] = e.target.value.toUpperCase();
          }
        }
      }

      this.setState({ resources });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };
  handleDriverChange = (e, date, i, typeName) => {
    e.preventDefault();
    console.log(e.target.className);
    if (e.target.className.includes('driver')) {
      let resources = [...this.state.resources];
      for (let x = 0; x < resources.length; x++) {
        console.log(resources[x].date);
        if (resources[x].date.date() === date.date()) {
          if (
            resources[x].rationId === i &&
            resources[x].typeName === typeName
          ) {
            console.log(resources[x].arr);
            resources[x].arr[e.target.dataset.id]['driver'] = e.target.value;
          }
        }
      }
      this.setState({ resources });
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };
  onChange(e) {
    let name = e.target.name;
    let value = e.target.value;
    let prevState = this.state;
    prevState[name] = value;
    this.setState({
      prevState,
    });
  }
  onSubmit(e, date, i, order, typeName) {
    let arr = [];
    let rationRun = this.state.resources;
    for (let x = 0; x < rationRun.length; x++) {
      if (
        rationRun[x].date.date() === date.date() &&
        rationRun[x].rationId === i &&
        rationRun[x].typeName === typeName
      ) {
        arr.push(rationRun[x]);
      }
    }
    console.log(order);
    let persid = 0;
    for (let x = 0; x < this.state.order.length; x++) {
      console.log(this.state.order[x]);
      for (let y = 0; y < this.state.order[x].orders.length; y++) {
        if (this.state.order[x].orders[y].rationRun[0] !== undefined) {
          if (this.state.order[x].orders[y].rationRun[0]._id === i) {
            order = this.state.order[x].orders[y];
            console.log(this.state.order[x]);
            persid = this.state.order[x].persid;
          }
        }
      }
    }
    console.log(order);
    order.rationRun[0].resources = arr;
    order.orderId = order._id;
    console.log(arr);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let body = {};
    body.orderId = order._id;
    body.persid = persid;
    body.order = [order];
    console.log(body);
    axios.post('/api/order', body, config);
    this.getSharedResource();
    alert('Submission Successful');
  } /* rationRun: []         resources: [
    { date: moment(), rationId: 0, arr: [{ driver: 0, vehicle: '' }] },
  ], */
  render() {
    let itemNotStarted = [];
    let itemStarted = [];
    let itemCompleted = [];
    let sharedResource = '';
    let rationRun = {};
    let allSharedResource = [];
    let resources1 = [];
    let fullOrder = {};
    let cardArr = [];
    let resources = this.state.resources;
    console.log(this.state.resources);
    let bigAccordion = [];
    let bool = true; //Change to false after testing <---important
    if (this.state.order != undefined) {
      console.log(this.state.order);
      for (let y = 0; y < this.state.order.length; y++) {
        for (let i = 0; i < this.state.order[y].orders.length; i++) {
          if (this.state.order[y].orders[i].check === true) {
            if (
              moment(new Date()).isBefore(
                new Date(this.state.order[y].orders[i].endDate),
                null,
                '[]'
              )
            ) {
              console.log('you may pass');
              bool = true;
              sharedResource = this.state.order[y].orders[i].activity;
              rationRun = this.state.order[y].orders[i].rationRun[0];
              fullOrder = this.state.order[y].orders[i];
              allSharedResource.push(this.state.order[y].orders[i]);
            } else {
              console.log('you are here early');
            }
          }
        }
      }
      let type = this.state.typeName.toLowerCase();
      let state = [];
      let whichorder = [];
      for (let i = 0; i < allSharedResource.length; i++) {
        //If I am doing anything with checking wether endDate has passed, I have to do it here cuz rationId depends on i, so I cannot change length. I could try changing i to rationId. Probably should lol.
        cardArr = [];
        let rationRun = allSharedResource[i].rationRun[0];

        if (rationRun !== undefined)
          for (let x = 0; x < rationRun[type] - 1; x++) {
            if (x == 0) {
            }

            console.log(x);
            let filterd = [];

            rationRun = allSharedResource[i].rationRun[0];
            fullOrder = allSharedResource[i];
            console.log(fullOrder);
            let xa = 0;
            if (
              type === 'breakfast' &&
              moment(fullOrder.startDate).hour() > 6
            ) {
              xa = x + 1;
            } else if (
              type === 'lunch' &&
              moment(fullOrder.startDate).hour() > 12
            ) {
              xa = x + 1;
            } else {
              xa = x;
            }
            let date = moment(new Date(fullOrder.startDate))
              .add(xa, 'days')
              .format('dddd, MMMM Do YYYY');
            let open = true;
            let momentDate = moment(new Date(fullOrder.startDate)).add(
              xa,
              'days'
            );

            cardArr.push(
              <div style={{ margin: 10 }}>
                <Card>
                  <Card.Header
                    style={{
                      backgroundColor: 'grey',
                      color: 'white',
                      fontSize: 20,
                    }}
                  >
                    {date}{' '}
                    <Button
                      style={{ float: 'right' }}
                      onClick={() => this.changeOpen(x + i)}
                    >
                      {this.state.arrOpen[x + i] ? 'Close' : 'Open'}
                    </Button>{' '}
                    {open}
                  </Card.Header>
                  <Collapse in={this.state.arrOpen[x + i]}>
                    <Card.Body>
                      <Card.Title>
                        {this.state.typeName}, Pax: {rationRun.pax}, Extrainfo:{' '}
                        {fullOrder.extraInfo}
                      </Card.Title>
                      {
                        ((filterd = []),
                        (state = this.state),
                        console.log(resources),
                        resources.filter(function (item) {
                          item.date = moment(new Date(item.date));

                          if (
                            item.date.date() === momentDate.date() &&
                            item.date.month() === momentDate.month() &&
                            item.date.year() === momentDate.year() &&
                            item.rationId === rationRun._id &&
                            item.typeName === type
                          ) {
                            filterd.push(item);
                            console.log(fullOrder);
                            console.log(filterd);
                            whichorder = fullOrder;
                          }
                        }))
                      }
                      {
                        (console.log(filterd[0]),
                        filterd[0] === undefined
                          ? null
                          : filterd[0].arr.map((val, idx) => {
                              let resourceId = `resources-${idx}`,
                                vehicleId = `vehicle-${idx}`,
                                driverId = `driver-${idx}`,
                                quantityId = `quantity-${idx}`;
                              return (
                                <div key={idx}>
                                  <Form.Label>{`Resource #${
                                    idx + 1
                                  }`}</Form.Label>
                                  <Form.Row>
                                    <Form.Group as={Col} className='test'>
                                      <Form.Label>Vehicle: </Form.Label>

                                      <Form.Control
                                        as='select'
                                        placeholder='vehicles'
                                        name={vehicleId}
                                        data-id={idx}
                                        id={vehicleId}
                                        value={filterd[0].arr[idx]['vehicle']}
                                        multiple={false}
                                        className='vehicle'
                                        onChange={(e) =>
                                          this.handleVehicleChange(
                                            e,
                                            momentDate,
                                            rationRun._id,
                                            type
                                          )
                                        }
                                        required
                                      >
                                        {this.state.vehicles.map(
                                          (item, index) => (
                                            <option>{item.vehiclenum}</option>
                                          )
                                        )}
                                      </Form.Control>
                                    </Form.Group>
                                    <Form.Group as={Col} className='test'>
                                      <Form.Label>Driver: </Form.Label>
                                      <Form.Control
                                        as='select'
                                        placeholder='drivers'
                                        name={driverId}
                                        className='driver'
                                        name={driverId}
                                        data-id={idx}
                                        id={driverId}
                                        value={filterd[0].arr[idx]['driver']}
                                        onChange={(e) =>
                                          this.handleDriverChange(
                                            e,
                                            momentDate,
                                            rationRun._id,
                                            type
                                          )
                                        }
                                        required
                                      >
                                        {this.state.drivers.map(
                                          (item, index) => (
                                            <option>{item.name}</option>
                                          )
                                        )}
                                      </Form.Control>{' '}
                                    </Form.Group>
                                  </Form.Row>
                                  <Form.Group className='test'>
                                    <Button
                                      value={idx}
                                      variant='danger'
                                      style={{ width: 85 }}
                                      onClick={(e) =>
                                        this.removeResources(
                                          e,
                                          momentDate,
                                          rationRun._id,
                                          type
                                        )
                                      }
                                    >
                                      Remove
                                    </Button>
                                  </Form.Group>
                                </div>
                              );
                            }))
                      }
                      {filterd[0] === undefined ? null : filterd[0].remarks !==
                          undefined && filterd[0].remarks !== '' ? (
                        <h3>Remarks: {filterd[0].remarks}</h3>
                      ) : null}{' '}
                      <Button
                        onClick={(e) =>
                          this.addResources(e, momentDate, rationRun._id, type)
                        }
                        style={{
                          align: 'center',
                          positon: 'fixed',
                          backgroundColor: '#89B6A5',
                          border: 'none',
                          width: 120,
                        }}
                      >
                        Add Resource
                      </Button>{' '}
                    </Card.Body>
                  </Collapse>{' '}
                  <Collapse in={this.state.arrOpen[x + i]}>
                    <Card.Footer>
                      {' '}
                      {rationRun.remarks}
                      {rationRun._id.localeCompare(
                        fullOrder.rationRun[0]._id
                      ) !== 0 ? null : (
                        <Button
                          variant='success'
                          className='submit'
                          style={{ width: 85 }}
                          value={fullOrder}
                          onClick={(e) =>
                            this.onSubmit(
                              e,
                              momentDate,
                              rationRun._id,
                              fullOrder,
                              type
                            )
                          }
                        >
                          Submit
                        </Button>
                      )}
                    </Card.Footer>
                  </Collapse>
                </Card>
              </div>
            );
          }
        bigAccordion.push(
          <div style={{ margin: 15 }}>
            <Accordion defaultActiveKey='0'>
              {' '}
              <Card>
                <Card.Header>
                  {' '}
                  <Accordion.Toggle as={Button} variant='link' eventKey={i}>
                    <Row>
                      <Col>{allSharedResource[i].activity} </Col>
                      <Col>{allSharedResource[i].location} </Col>
                    </Row>{' '}
                    <Row>
                      <Col>
                        {moment(
                          new Date(allSharedResource[i].startDate)
                        ).format('Do MMMM  YYYY, hh:mm:ss a')}{' '}
                      </Col>
                      <Col>
                        {moment(new Date(allSharedResource[i].endDate)).format(
                          'Do MMMM  YYYY, hh:mm:ss a'
                        )}{' '}
                      </Col>
                    </Row>
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey={i}>
                  <Card.Body>{cardArr}</Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </div>
        );
      }
    }
    if (bool == false) {
      return (
        <div>
          <h2>No Shared Resource is active right now</h2>
        </div>
      );
    }
    {
      let drivers1 = this.state.adhoc.filter(function (item) {
        console.log(item.sharedResource);
        if (item.sharedResource === undefined) {
        } else if (item.sharedResource.localeCompare(sharedResource) === 0) {
          if (item.status === undefined) itemNotStarted.push(item);
          else if (item.status.localeCompare('Accept') == 0)
            itemStarted.push(item);
          else if (item.status.localeCompare('Completed') === 0)
            itemCompleted.push(item);
        }
      });
    }
    return (
      <div className='overall'>
        <TopNav />
        <h1>Shared Resource (Resource Manager)</h1>
        <Container>
          <Form.Label style={{ display: 'inline' }}>
            Filter Ration Run:{' '}
          </Form.Label>
          <Form.Control
            as='select'
            style={{ display: 'inline', maxWidth: '80%' }}
            placeholder='Ration'
            name='typeName'
            value={this.state.typeName}
            multiple={false}
            className='vehicle'
            onChange={(e) => this.onChange(e)}
            required
          >
            <option>Breakfast</option>
            <option>Lunch</option>
            <option>Dinner</option>
          </Form.Control>
          {bigAccordion}
        </Container>
      </div>
    );
  }
}

/*  if (
      this.state.driver != undefined &&
      this.state.driver.status != undefined &&
      this.state.driver.status.localeCompare('Accept') === 0
    ) {
      accepted = (
        <div>
          <div>
            <br></br>
            <h3
              style={{
                textAlign: 'center',
                alignItems: 'center',
                margin: 10,
                color: 'black',

                marginBlockStart: 0,
                paddingInlineStart: 0,
                marginBlockEnd: 0,
              }}
            >
              In-Progress
            </h3>
            <br />
            {itemStarted.filter(function (item) {
              if (item.status != undefined)
                if (item.status.localeCompare('Accept') === 0)
                  filtered.push(item);
            })}
            {filtered.map((item, index) => (
              <div style={{}}>
                <Card
                  style={{
                    width: 300,
                    backgroundColor: 'whitesmoke',

                    margin: '0 auto',
                    float: 'none',
                    marginBottom: 10,

                    border: '2px black',
                    padding: '5px',
                    boxShadow: '5px 5px grey',
                  }}
                >
                  <Card.Header>{item._id}</Card.Header>
                  <Card.Body>
                    <Card.Title>
                      {item.name} {'\u00A0'}
                      {item.vehicle}
                    </Card.Title>

                    <Card.Text>
                      <table>
                        <thead>
                          <tr>
                            <th style={{ width: 90, padding: 2 }}>Start:</th>
                            <th style={{ width: 90, padding: 2 }}>
                              Destination
                            </th>
                            <th style={{ width: 90, padding: 2 }}>Reason</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{item.location}</td>
                            <td>{item.destination}</td>
                            <td> {item.reason}</td>
                          </tr>
                        </tbody>
                      </table>

                      <br></br>
                    </Card.Text>
                  </Card.Body>
                  <Card.Footer>
                    {' '}
                    <Button
                      style={{ marginBottom: 10, marginTop: 10 }}
                      value={item._id}
                      onClick={(e) => {
                        this.onSubmit(e, 'Completed');
                      }}
                    >
                      Complete
                    </Button>
                  </Card.Footer>
                </Card>
              </div>
            ))}
          </div>
        </div>
      );
    } */

export default adhoc;
