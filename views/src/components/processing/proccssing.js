import React from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fade from 'react-reveal/Fade'; // Importing Zoom effect

import DialogTitle from '@material-ui/core/DialogTitle'; /* import { render } from 'react-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';import { rgbToHex } from '@material-ui/core';
import store from '../../store';
, { Fragment, useState }
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';import CreateIcon from '@material-ui/icons/Create';

import newOrder from '../newOrder/newOrder'; */
import Edit from '@material-ui/icons/Edit';
import { resubmitIndent } from '../../nextPath';
import Remove from '@material-ui/icons/Remove';
import SideMenu from '../sidemenu/sidemenu';
import '../processing/processing.css';
import nextPath1 from '../../nextPath';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { Button } from 'react-bootstrap';
import { Collapse } from '@material-ui/core';
class processing extends React.Component {
  //Stateful Class
  //To get access to any states and props
  table = [];
  constructor(props) {
    super(props);
    console.log(this.props.user);
    let arrOpen = new Array(20);
    arrOpen.fill(true, 0, 20);

    this.state = {
      allData: 'loading',
      arrOpen: arrOpen,
      order: [
        {
          _id: 0,
          activity: '',
          types: '',
          startDate: new Date(),
          endDate: new Date(),
          location: '',
          poc: '',
          data: 0,
          extraInfo: '',
        },
      ],
      resources: [{ vehicle: 'car', driver: '0', quantity: '2' }],
      remove: false,
      removed: false,
      persid: '',
      item: {},
      orderId: '',
      openResources: false,
    };
    let data = localStorage.getItem('token');
    data = JSON.parse(data);

    let allData = '';
    this.onChange(data);
  }
  async onChange(data) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    let res = await axios.get(
      '/api/auth',
      {
        params: {
          id: data,
        },
      },
      config
    );
    this.setState({
      resources: res.data.orders[0].resources,
      allData: res.data.orders[0].persid,
      persid: res.data.orders[0].persid,
      order: res.data.orders[0].orders,
      data: data,
    });
  }
  componentDidMount() {
    document.title = 'Processing Page';
  }
  createTable = () => {
    this.table = [];
    if (this.state.order.length > 0)
      for (let i = 0; i < this.state.order.length; i++) {
        if (new Date(this.state.order[i].startDate) >= new Date())
          this.table.push({
            orderId: this.state.order[i]._id,
            activity: this.state.order[i].activity,
            types: this.state.order[i].types,
            startDate: new Date(
              this.state.order[i].startDate
            ).toLocaleString('en-GB', { timeZone: 'Asia/Singapore' }),
            endDate: new Date(this.state.order[i].endDate).toLocaleString(
              'en-GB',
              {
                timeZone: 'Asia/Singapore',
              }
            ),
            processing: this.state.order[i].processing,
            remark: this.state.order[i].extraInfo,
            reason: this.state.order[i].reason,
            admin: this.state.order[i].indentApprover,
            resources: this.state.order[i].resources,
          });
      }
    return this.table;
  };
  changeOpen = (x) => {
    let arrOpen = [...this.state.arrOpen];
    arrOpen[x] = !arrOpen[x];
    console.log(arrOpen[x]);
    this.setState({ arrOpen });
  };
  render() {
    let createCard = {};
    if (this.state.order != undefined) {
      let table = this.table;
      this.createTable();
      if (window.screen.width < 700) {
        createCard = (
          <div>
            <h1>Processing</h1>
            {this.table.map((item, index) => (
              <div>
                <Fade left>
                  <Card
                    style={{
                      backgroundColor: 'whitesmoke',
                      border: '2px black',
                      padding: '5px',
                      boxShadow: '5px 5px grey',
                    }}
                  >
                    <Card.Title>
                      {item.orderId}

                      <Button
                        style={{ float: 'right' }}
                        onClick={() => this.changeOpen(index)}
                      >
                        {this.state.arrOpen[index] ? 'Close' : 'Open'}
                      </Button>
                    </Card.Title>
                    <Collapse in={this.state.arrOpen[index]}>
                      <Card.Body id='Body'>
                        {' '}
                        <div>
                          <table>
                            <thead style={{ maxWidth: 100 }}>
                              <tr>
                                <th
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  Activity
                                </th>
                                <th
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  Type
                                </th>
                                <th
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  Status
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  {item.activity}
                                </td>
                                <td
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  {item.types}
                                </td>
                                <td
                                  style={{
                                    width: 90,
                                    padding: 2,
                                  }}
                                >
                                  {item.processing}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <Card.Text>
                          <li key='23'>Start: {item.startDate}</li>
                          <li key='24'>End: {item.endDate}</li>{' '}
                          {item.extraInfo === undefined ||
                          item.extraInfo.localeCompare('') === 0 ? null : (
                            <li key='2'>Remark: {item.extraInfo}</li>
                          )}
                          {item.admin === undefined ||
                          item.admin.localeCompare('') === 0 ? null : (
                            <li key='21'>Admin: {item.admin} </li>
                          )}
                          {item.reason === undefined ||
                          item.reason.localeCompare('') === 0 ? null : (
                            <li key='26'>Rejection reason: {item.reason}</li>
                          )}{' '}
                        </Card.Text>{' '}
                        {item.resources === undefined
                          ? null
                          : item.resources.map((item1, index) => (
                              <div>
                                <table>
                                  <thead style={{ maxWidth: 100 }}>
                                    <tr>
                                      <th
                                        style={{
                                          width: 90,
                                          padding: 2,
                                        }}
                                      >
                                        Driver
                                      </th>
                                      <th
                                        style={{
                                          width: 90,
                                          padding: 2,
                                        }}
                                      >
                                        Vehicle
                                      </th>
                                      <th
                                        style={{
                                          width: 90,
                                          padding: 2,
                                        }}
                                      >
                                        Quantity
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td
                                        style={{
                                          width: 90,
                                          padding: 2,
                                        }}
                                      >
                                        {item1.vehicle}
                                      </td>
                                      <td
                                        style={{
                                          width: 90,
                                          padding: 2,
                                        }}
                                      >
                                        {item1.quantity}
                                      </td>
                                      <td
                                        style={{
                                          width: 90,
                                          padding: 2,
                                        }}
                                      >
                                        {item1.driver}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <Button
                                  style={{ marginTop: 10 }}
                                  onClick={() => this.handleOpen(item)}
                                >
                                  Resource Details
                                </Button>
                              </div>
                            ))}
                      </Card.Body>
                    </Collapse>{' '}
                    <Collapse in={this.state.arrOpen[index]}>
                      <Card.Footer>
                        <Button
                          className='btn-sm'
                          onClick={(e) => this.clickedEdit(this.props, item)}
                        >
                          Edit
                        </Button>{' '}
                        <Button
                          className='btn-sm danger'
                          variant='danger'
                          onClick={(e) => this.clickedRemove(e, item)}
                        >
                          Remove
                        </Button>{' '}
                        <Button
                          className='btn-sm'
                          onClick={(e) =>
                            resubmitIndent(
                              this.props,
                              '/newOrder',
                              item.orderId,
                              true
                            )
                          }
                        >
                          Duplicate
                        </Button>
                      </Card.Footer>
                    </Collapse>
                  </Card>
                </Fade>

                <this.OpenResources></this.OpenResources>
                <hr></hr>
              </div>
            ))}
          </div>
        );
      }
      return (
        <div
          style={{
            background: 'lightgrey',
            position: 'fixed',
            bottom: '0',
            right: '0',
            left: '0',
            top: '0',
            overflowY: 'auto',
          }}
        >
          <SideMenu></SideMenu>
          <div className='table'>
            <link
              rel='stylesheet'
              href='https://fonts.googleapis.com/icon?family=Material+Icons'
            ></link>
            <table />
            {window.screen.width < 700 ? (
              createCard
            ) : (
              <MaterialTable
                columns={[
                  { title: 'Activity', field: 'activity' },
                  { title: 'Type', field: 'types' },
                  { title: 'Start Date', field: 'startDate' },
                  { title: 'End Date', field: 'endDate' },
                  { title: 'Processing', field: 'processing' },
                  { title: 'Admin', field: 'admin' },
                  { title: 'Reason', field: 'reason' },
                ]}
                data={this.createTable()}
                title='Processing'
                actions={[
                  {
                    icon: Edit,
                    tooltip: 'Edit Data',
                    onClick: (event, rowData) =>
                      this.clickedEdit(this.props, rowData),
                  },
                  {
                    icon: Remove,
                    tooltip: 'Remove Data',
                    onClick: (event, rowData) => {
                      this.clickedRemove(event, rowData);
                    },
                  },
                  {
                    icon: FileCopyIcon,
                    tooltip: 'Resubmit Indent',
                    onClick: (event, rowData) =>
                      this.clickedResubmit(
                        this.props,

                        rowData
                      ),
                  },
                ]}
                options={{
                  header: true,
                  headerStyle: {
                    backgroundColor: 'rgb(160,200,130)',
                    color: 'black',
                  },
                  rowStyle: {
                    backgroundColor: '#F7F7FF',
                    color: 'black',
                  },
                  search: false,
                  tableLayout: 'auto',
                  actionsColumnIndex: 0,
                  addRowPosition: 'last',
                }}
              />
            )}
          </div>
        </div>
      );
    } else {
      return <div className='overall'>{this.state.order[16]}</div>;
    }
  }

  handleOpen = (item) => {
    this.setState({ openResources: !this.state.openResources, item: item });
  };
  handleClose = () => {
    this.setState({ openResources: false });
  };
  OpenResources = (item) => {
    const onChangeVehicle = (event) => {
      let prevState = this.state;
      this.setState(() => ({
        update: event,
      }));
    };
    if (typeof this.state.openResources !== 'string') {
      let rowData = this.state.item;
      return (
        <div>
          <Dialog
            open={this.state.openResources}
            onClose={this.handleClose}
            aria-labelledby='form-dialog-title'
          >
            <DialogTitle id='form-dialog-title'>Details</DialogTitle>
            <form>
              <DialogContent>
                <DialogContentText>
                  {rowData.resources === undefined
                    ? 'Nothing to Show'
                    : rowData.resources.map((item, index) => (
                        <div>
                          <h5>Driver Assigned</h5>
                          <ol>
                            {item.driverAssigned.map((item1, index1) => (
                              <div>{item1}</div>
                            ))}
                          </ol>
                          <h5>Vehicle Assigned</h5>
                          <ol>
                            {item.vehicleAssigned.map((item1, index1) => (
                              <div>{item1}</div>
                            ))}
                          </ol>
                        </div>
                      ))}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color='primary'>
                  Close
                </Button>
              </DialogActions>
            </form>
          </Dialog>
        </div>
      );
    } else {
      return <div></div>;
    }
  };
  clickedResubmit(props, rowData) {
    resubmitIndent(props, '/newOrder', rowData.orderId, true);
  }
  clickedEdit(props, rowData) {
    let bool = false;
    for (let x = 0; x < rowData.resources.length; x++) {
      for (let i = 0; i < rowData.resources[x].approve.length; i++) {
        if (rowData.resources[x].approve[i] === true) {
          alert(
            'Indent cannot be changed once a driver has been assigned, please ask the resource manager to remove the assigned driver to continue.'
          );
          bool = true;
        }
      }
    }
    for (let x = 0; x < rowData.resources.length; x++) {
      for (let i = 0; i < rowData.resources[x].vehicleApprove.length; i++) {
        if (rowData.resources[x].vehicleApprove[i] === true) {
          alert(
            'Indent cannot be changed once a vehicle has been assigned, please ask the resource manager to remove the assigned driver to continue.'
          );
          bool = true;
        }
      }
    }
    if (bool === false) nextPath1(props, '/newOrder', rowData.orderId);
  }
  async clickedRemove(event, rowData) {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };
      for (let x = 0; x < rowData.resources.length; x++) {
        for (let i = 0; i < rowData[x].approve.length; i++) {
          if (rowData[x].approve[i] === true)
            alert(
              'Indent cannot be changed once a driver/vehicle has been assigned'
            );
        }
      }
      for (let x = 0; x < rowData.resources.length; x++) {
        for (let i = 0; i < rowData[x].vehicleApprove.length; i++) {
          if (rowData[x].vehicleApprove[i] === true)
            alert(
              'Indent cannot be changed once a driver/vehicle has been assigned'
            );
        }
      }
      if (rowData.processing.localeCompare('Approved') == 0) {
        alert(
          'Indent cannot be removed once Approved. Get an admin to change its status to rejected.'
        );
        return;
      }
      let stateLocal = this.state;
      stateLocal['remove'] = true;
      stateLocal['orderId'] = rowData.orderId;
      const body = JSON.stringify(stateLocal);
      console.log(stateLocal);
      const res = await axios.post('/api/order', body, config);
      this.setState({
        removed: true,
      });
      let data = localStorage.getItem('token');
      data = JSON.parse(data);
      this.onChange(data);
    } catch (err) {
      console.log(err);
    }
  }
}

export default processing;
