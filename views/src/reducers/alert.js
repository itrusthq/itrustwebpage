import { SET_ALERT, REMOVE_ALERT, SET_DATA } from '../actions/types';

const initialState = [];

export const data = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_DATA:
      return [...state, payload];
    default:
      return state;
  }
};

export default function(state = initialState, action) {
  //initial State will be empty and action is the payload, the payload can be empty at times
  const { type, payload } = action;
  console.log(action);
  switch (type) {
    case SET_ALERT:
      return [...state, payload];
    case REMOVE_ALERT:
      return state.filter(alert => alert.id !== payload);
    case SET_DATA:
      return [...state, payload];
    default:
      return state;
  }
}
