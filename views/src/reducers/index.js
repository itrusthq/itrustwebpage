import { combineReducers } from 'redux';
import alert from './alert';
import { data } from './alert';
import auth from './auth';
export default combineReducers({
  alert,
  data,
  auth,
});
