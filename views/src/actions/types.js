export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';
export const SET_DATA = 'SET_DATA';
export const SET_AUTH = 'SET_AUTH';
