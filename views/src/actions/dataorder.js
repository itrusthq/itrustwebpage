import { SET_DATA } from './types';
import uuid from 'uuid';

export const setData = (data, timeout = 5000) => (dispatch) => {
  dispatch({
    type: SET_DATA,
    payload: { data },
  });
};

export const getData = (data) => {
  return {
    type: 'GET_DATA',
    payload: data,
  };
};
