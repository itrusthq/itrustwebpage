importScripts('https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js');
firebase.initializeApp({
  // Project Settings => Add Firebase to your web app

  apiKey: 'AIzaSyCkbfLsnJjrvQkkV0DctV86n4H52ToeWwQ',
  authDomain: 'orderapp-89c82.firebaseapp.com',
  databaseURL: 'https://orderapp-89c82.firebaseio.com',
  projectId: 'orderapp-89c82',
  storageBucket: 'orderapp-89c82.appspot.com',
  messagingSenderId: '42936114751',
  appId: '1:42936114751:web:60d6d9d5a90b45f030a28a',

  messagingSenderId: '42936114751',
});
if (firebase.messaging.isSupported()) {
  const messaging = firebase.messaging();
  /* messaging.onMessage(function (payload) {
  console.log(
    '[firebase-messaging-sw.js] Received background message ',
    payload
  );
}); */
  /*   messaging.setBackgroundMessageHandler(function (payload) {
    console.log(
      '[firebase-messaging-sw.js] Received background message ',
      payload
    );
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
      body: 'Background Message body.',
    };

    return self.registration.showNotification(
      notificationTitle,
      notificationOptions
    );
  }); */
  messaging.setBackgroundMessageHandler(function (payload) {
    console.log('message');

    const promiseChain = clients
      .matchAll({
        type: 'window',
        includeUncontrolled: true,
      })
      .then((windowClients) => {
        for (let i = 0; i < windowClients.length; i++) {
          const windowClient = windowClients[i];
          windowClient.postMessage(payload);
        }
      })

      .then(() => {
        console.log(payload);
        const notificationTitle =
          'ITRUSTS ADHOC INDENT CHANGES MADE IN: ' +
          payload.data.my_another_key;
        const notificationOptions = {
          body:
            payload.data.my_key +
            ' Date: ' +
            payload.data.my_third_key +
            ' Type: ' +
            payload.data.my_last_key,

          fcm_options: { link: 'itrusts.herokuapp.com' },
        };
        return registration.showNotification(
          notificationTitle,
          notificationOptions
        );
      });
    return promiseChain;
  });
  self.addEventListener('notificationclick', function (event) {
    // do what you want
    clients.openWindow('/itms/adhoc');
    // ...
  });
} else {
  console.log('Firebase messaging is not supported');
}
