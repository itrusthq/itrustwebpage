const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  persid: {
    type: Number,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  authorized: {
    type: Boolean,
  },
  unit: {
    type: String,
  },
  hub: {
    type: String,
  },
});

const OrderSchema = new mongoose.Schema({
  persid: Number,
  unit: String,
  hub: String,
  tokenArr: [
    {
      remarks: Array,
      token: Array,
    },
  ],

  orders: [
    {
      activity: String,
      types: String,
      startDate: Date,
      endDate: Date,
      location: String,
      poc: String,
      status: Boolean,
      token: Array,
      extraInfo: String,
      processing: String,
      indentApprover: String,
      check: Boolean,
      reason: String,
      rationRun: [
        {
          typeName: String, //[breakfast, lunch, dinner, breakfast, lunch] Created based on the start and end date... I could create in new order... probably should
          pax: Number,
          options: Array,
          token: Array,

          breakfast: Number,
          lunch: Number,
          dinner: Number,
          resources: [
            {
              date: Date,
              rationId: String,
              typeName: String,
              remarks: String,
              token: Array,
              arr: [{ driver: String, vehicle: String }],
            },
          ], //resources: []
          vehicle: Array,
          nonMuslim: Number,
          muslim: Number,
          extraInfo: String,
          vegetarianIndian: Number,
          vegetarianChinese: Number,
        },
      ],
      resources: [
        {
          vehicle: Array,
          quantity: Array,
          driver: Array,
          approve: Array,
          driverAssigned: Array,
          driverSum: Number, //Not really being used yet but I want to use it in case it is required later
          vehicleAssigned: Array,
          vehicleApprove: Array,
        },
      ],
    },
  ],
});

const VehicleSchema = new mongoose.Schema({
  vehicles: [
    {
      quantity: Number,
      name: String,
      unit: String,
      vehiclenumber: Array,
      orderId: Array,
      resourceId: Array,
    },
  ],
  quantity: String,
  name: String,
  unit: String,
  vehiclenum: String,
  orderId: Array,
  startDate: Array,
  endDate: Array,
  resourceId: Array,
});

const DriverSchema = new mongoose.Schema({
  name: String,
  orderId: Array,
  resourceId: Array,
  startDate: Array,
  endDate: Array,
  unit: String,
  hub: String,
  skillsets: Array,
  persid: Number,
  status: String,
  adhocId: String,
});

const AdhocSchema = new mongoose.Schema({
  location: String,
  startPoint: String,
  endPoint: String,
  poc: Number,
  startDate: Date,
  endDate: Date,
  indentDate: Date,
  sharedResource: String,
  destination: String,
  vehicle: String,
  quantity: Number,
  reason: String,
  name: String,
  persid: Number,
  status: String,
});

const user = (module.exports.User = mongoose.model('ITMSuser', UserSchema));
const Order = (module.exports.Order = mongoose.model('Order', OrderSchema));
const driver = (module.exports.driver = mongoose.model('Driver', DriverSchema));
const vehicle = (module.exports.vehicle = mongoose.model(
  'Vehicle',
  VehicleSchema
));
const adhoc = (module.exports.adhoc = mongoose.model('Adhoc', AdhocSchema));

module.exports.postAdhoc = function (order, callback) {
  order.save(callback);
};

module.exports.getAdhoc = function (callback) {
  adhoc.find({}, callback);
};

module.exports.getUserbyPersid = function (persid, callback) {
  const query = { persid: persid };
  user.findOne(query, callback);
};

module.exports.getVehicles = function (callback) {
  vehicle.find({}, callback);
};

module.exports.getDrivers = function (callback) {
  driver.find({}, callback);
};

module.exports.postOrder = function (order, callback) {
  order.save(callback);
};
module.exports.updateOrder = function (order, callback) {
  order[0].save(callback);
};
module.exports.postRemarksUpdate = function (remark, token, callback) {
  console.log(token);
  let order = [];
  Order.findOne({ 'orders.rationRun.resources.remarks': remark }, callback);
  console.log('Hello');

  /*   Order.findOneAndUpdate(
    { 'orders.rationRun.resources.remarks': remark },
    {
      $push: {
        'orders.$[].rationRun.$[].resources.$[].token': token,
      },
    },  { arrayFilters: [{ 'element.author': 'Bar' }] },

    callback
  );*/
};
module.exports.getUserbyId = function (id, callback) {
  user.findById(id, callback);
};

module.exports.postDriverbyId = function (
  name,
  orderId,
  resourceId,
  startDate,
  endDate,
  callback
) {
  driver.findOneAndUpdate(
    { name: name },
    {
      $push: {
        orderId: [orderId],
        resourceId: [resourceId],
        startDate: [startDate],
        endDate: [endDate],
      },
    },
    callback,
    { useFindAndModify: false }
  );
};

module.exports.postDriverbyNameAdhoc = function (
  name,
  adhocId,
  status,
  callback
) {
  if (status.localeCompare('Completed') == 0) {
    //This is for removing orderID when the indent is completed.
    driver.findOneAndUpdate(
      { adhocId: adhocId },
      {
        $set: { adhocId: '', status: '' },
      },
      callback,
      { useFindAndModify: false }
    );
  } else {
    driver.findOneAndUpdate(
      { name: name },
      {
        $set: { adhocId: adhocId, status: status },
      },
      callback,
      { useFindAndModify: false }
    );
  }
};

module.exports.patchAdhoc = function (id, status, persid, name, callback) {
  if (status.localeCompare('Completed') == 0) {
    adhoc.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          status: status,
        },
      },
      callback,
      { useFindAndModify: false }
    );
  }
  adhoc.findOneAndUpdate(
    { _id: id },
    {
      $set: {
        status: status,
        name: name,
        persid: persid,
      },
    },
    callback,
    { useFindAndModify: false }
  );
};

module.exports.postVehiclebyId = function (
  name,
  orderId,
  resourceId,
  startDate,
  endDate,
  callback
) {
  vehicle.findOneAndUpdate(
    { vehiclenum: name },
    {
      $push: {
        orderId: [orderId],
        resourceId: [resourceId],
        startDate: [startDate],
        endDate: [endDate],
      },
    },
    callback,
    { useFindAndModify: false }
  );
};

module.exports.getOrderbyPersid = function (persid, callback) {
  const query = { persid: persid };
  Order.find(query, callback);
};

//Get all orders based on the node
module.exports.getOrdersbyUnit = function (hub, callback) {
  const query = { hub: hub };
  Order.find(query, callback);
};

module.exports.removeOrder = function (persid, id, callback) {
  Order.updateOne(
    { persid: persid },
    { $pull: { orders: { _id: id } } },
    callback
  );
};

module.exports.removeDriverByOrderId = function (
  name,
  orderId,
  resourceId,
  startDate,
  endDate,
  callback
) {
  driver.update(
    { name: name },
    {
      $pull: {
        orderId: orderId,
        resourceId: resourceId,
        startDate: startDate,
        endDate: endDate,
      },
    },
    callback
  ); //I think I don't need to [orderId] but its based off of stackoverflow
};

module.exports.removeVehicleById = function (
  name,
  orderId,
  resourceId,
  startDate,
  endDate,
  callback
) {
  vehicle.update(
    { vehiclenum: name },
    {
      $pull: {
        orderId: orderId,
        resourceId: resourceId,
        startDate: startDate,
        endDate: endDate,
      },
    },
    callback
  ); //I think I don't need to [orderId] but its based off of stackoverflow
};

module.exports.comparePassword = function (candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if (err) throw err;
    callback(null, isMatch);
  });
};
